// Created by Paweł Sobótka on 04.12.17.
// Rail-Mil Computers Sp. z o.o.

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlContext>
#include <QQuickWindow>
#include <QQuickView>
#include <Qt3DQuick/QQmlAspectEngine>

#include "GelloSimulator.hpp"
#include "ParticleModel.hpp"
#include "SpringModel.hpp"
#include "CubeWalls.hpp"
#include "CuboidWireframeGeometry.hpp"
#include "BezierFrameGeometry.hpp"
#include "QmlRaycast.hpp"

int main(int ac, char **av)
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(ac, av);

    QQmlApplicationEngine engine;

    qmlRegisterType<GelloSimulator>("PSW", 1, 0, "GelloSimulator");
    qmlRegisterType<ParticleModel>("PSW", 1, 0, "ParticleModel");
    qmlRegisterType<SpringModel>("PSW", 1, 0, "SpringModel");
    qmlRegisterType<CubeWalls>("PSW", 1, 0, "CubeWalls");
    qmlRegisterType<CuboidWireframeGeometry>("PSW", 1, 0, "CuboidWireframeGeometry");
    qmlRegisterType<BezierFrameGeometry>("PSW", 1, 0, "BezierFrameGeometry");
    qmlRegisterType<QmlRaycast>("PSW", 1, 0, "QmlRaycast");

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
