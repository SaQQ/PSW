#include "SpringModel.hpp"

#include <cmath>

const float SpringModel::DefaultSpringConstant = 2.0f;

SpringModel::SpringModel(QObject *parent)
        : QAbstractListModel(parent),
          particleModel(nullptr), ks(DefaultSpringConstant) {

}

QHash<int, QByteArray> SpringModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[StartPositionRole] = "startPosition";
    roles[EndPositionRole] = "endPosition";
    return roles;
}

QVariant SpringModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto &spring = springs[index.row()];

    switch (role) {
        case StartPositionRole:
            return QVariant(spring.from->position);
        case EndPositionRole:
            return QVariant(spring.to->position);
        default:
            return QVariant();
    }
}

void SpringModel::setupSprings()
{
    beginResetModel();

    springs.clear();

    if (particleModel != nullptr)
    {
        createSprings();
    }

    endResetModel();
}

void SpringModel::createSprings()
{
    auto size = ParticleModel::size;

    auto baseLen = ParticleModel::Extent / (size - 1) * 0.98f;

    auto sqrt2 = static_cast<float>(std::sqrt(2));

    springs.reserve(360);

    for (std::size_t x = 0; x < size; ++x)
    {
        for (std::size_t y = 0; y < size; ++y)
        {
            for (std::size_t z = 0; z < size; ++z)
            {

                if (x < size - 1) {
                    springs.push_back({&particleModel->getParticle(x, y, z), &particleModel->getParticle(x + 1, y, z), baseLen, ks});
                }

                if (y < size - 1) {
                    springs.push_back({&particleModel->getParticle(x, y, z), &particleModel->getParticle(x, y + 1, z), baseLen, ks});
                }

                if (z < size - 1) {
                    springs.push_back({&particleModel->getParticle(x, y, z), &particleModel->getParticle(x, y, z + 1), baseLen, ks});
                }

                if (x < size - 1 && y < size - 1) {
                    /* Add springs on X wall */
                    springs.push_back({&particleModel->getParticle(x, y, z), &particleModel->getParticle(x + 1, y + 1, z), baseLen * sqrt2, ks});
                    springs.push_back({&particleModel->getParticle(x + 1, y, z), &particleModel->getParticle(x, y + 1, z), baseLen * sqrt2, ks});
                }

                if (x < size - 1 && z < size - 1) {
                    /* Add springs on Y wall */
                    springs.push_back({&particleModel->getParticle(x, y, z), &particleModel->getParticle(x + 1, y, z + 1), baseLen * sqrt2, ks});
                    springs.push_back({&particleModel->getParticle(x + 1, y, z), &particleModel->getParticle(x, y, z + 1), baseLen * sqrt2, ks});
                }

                if (y < size - 1 && z < size - 1) {
                    /* Add springs on Z wall */
                    springs.push_back({&particleModel->getParticle(x, y, z), &particleModel->getParticle(x, y + 1, z + 1), baseLen * sqrt2, ks});
                    springs.push_back({&particleModel->getParticle(x, y + 1, z), &particleModel->getParticle(x, y, z + 1), baseLen * sqrt2, ks});
                }

            }
        }
    }

}
