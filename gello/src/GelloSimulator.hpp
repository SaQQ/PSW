// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef PSW_GELLO_SIMULATOR_HPP
#define PSW_GELLO_SIMULATOR_HPP

#include <QObject>
#include <QTimer>
#include <QElapsedTimer>

#include "ParticleModel.hpp"
#include "SpringModel.hpp"
#include "CubeWalls.hpp"

class GelloSimulator : public QObject {
    Q_OBJECT

    Q_PROPERTY(ParticleModel* particleModel READ getParticleModel WRITE setParticleModel NOTIFY particleModelChanged)
    Q_PROPERTY(SpringModel* springModel READ getSpringModel WRITE setSpringModel NOTIFY springModelChanged)
    Q_PROPERTY(CubeWalls* cubeWalls READ getCubeWalls WRITE setCubeWalls NOTIFY cubeWallsChanged)

    Q_PROPERTY(float frameSpringConstant READ getFrameSpringConstant WRITE setFrameSpringConstant NOTIFY frameSpringConstantChanged)
    Q_PROPERTY(float viscosityConstant READ getViscosityConstant WRITE setViscosityConstant NOTIFY viscosityConstantChanged)

    Q_PROPERTY(QVector3D framePosition READ getFramePosition WRITE setFramePosition NOTIFY framePositionChanged)
    Q_PROPERTY(QVector3D frameRotation READ getFrameRotation WRITE setFrameRotation NOTIFY frameRotationChanged)
public:
    static constexpr int stepIntervalMsec = 20;
    static const float integrationStep;
    static const float defaultViscosityConstant;
    static const float defaultFrameSpringConstant;

    explicit GelloSimulator(QObject* parent = nullptr);

    ParticleModel* getParticleModel() const { return particleModel; }

    void setParticleModel(ParticleModel *value) {
        if (particleModel != value) {
            particleModel = value;
            emit particleModelChanged();
        }
    }

    SpringModel* getSpringModel() const { return springModel; }

    void setSpringModel(SpringModel *value) {
        if (springModel != value) {
            springModel = value;
            emit springModelChanged();
        }
    }

    CubeWalls* getCubeWalls() const { return cubeWalls; }

    void setCubeWalls(CubeWalls* value) {
        if (cubeWalls != value) {
            cubeWalls = value;
            emit cubeWallsChanged();
        }
    }

    float getFrameSpringConstant() const { return frameSpringConstant; }

    void setFrameSpringConstant(float value) {
        frameSpringConstant = value;
        emit frameSpringConstantChanged();
    }

    float getViscosityConstant() const { return viscosityConstant; }

    void setViscosityConstant(float value) {
        viscosityConstant = value;
        emit viscosityConstantChanged();
    }

    QVector3D getFramePosition() const {
        return framePosition;
    }

    void setFramePosition(const QVector3D &value) {
        if (framePosition != value) {
            framePosition = value;
            emit framePositionChanged();
        }
    }

    QVector3D getFrameRotation() const {
        return frameRotation;
    }

    void setFrameRotation(const QVector3D &value) {
        if (frameRotation != value) {
            frameRotation = value;
            emit frameRotationChanged();
        }
    }

signals:
    void particleModelChanged();
    void springModelChanged();
    void cubeWallsChanged();
    void framePositionChanged();
    void frameRotationChanged();
    void frameSpringConstantChanged();
    void viscosityConstantChanged();
private slots:
    void tick();

private:
    QTimer *timer;
    QElapsedTimer *elapsedTimer;
    qint64 leftover;

    ParticleModel* particleModel;
    SpringModel* springModel;
    CubeWalls* cubeWalls;

    float frameSpringConstant;
    float viscosityConstant;

    QVector3D framePosition;
    QVector3D frameRotation;

    void clearForces();

    void applyForces();

    void massDivide();

    void integrate();

    void collide();
};


#endif //PSW_SIMULATOR_HPP
