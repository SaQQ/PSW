#ifndef PSW_SPRING_MODEL_HPP
#define PSW_SPRING_MODEL_HPP

#include <QAbstractListModel>

#include "ParticleModel.hpp"

struct Spring {
    Particle *from;
    Particle *to;
    float l0;
    float ks;

    void applyForce() {
        auto diff = to->position - from->position;
        auto distance = diff.length();
        QVector3D dir;
        if (distance < 0.00001)
            dir = QVector3D(1, 0, 0);
        else
            dir = diff.normalized();
        auto force = (distance - l0) * ks;
        from->force += dir * force;
        to->force -= dir * force;
    }
};

class SpringModel : public QAbstractListModel {
    Q_OBJECT

    Q_ENUMS(Roles)

    Q_PROPERTY(ParticleModel* particleModel READ getParticleModel WRITE setParticleModel NOTIFY particleModelChanged)

    Q_PROPERTY(float springConstant READ getSpringConstant WRITE setSpringConstant NOTIFY springConstantChanged)
public:

    enum Roles {
        StartPositionRole = Qt::UserRole + 1,
        EndPositionRole
    };

    static const float DefaultSpringConstant;

    explicit SpringModel(QObject *parent = nullptr);

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent) const override { return static_cast<int>(springs.size()); }

    QVariant data(const QModelIndex &index, int role) const override;

    ParticleModel* getParticleModel() const { return particleModel; }

    void setParticleModel(ParticleModel *value) {
        if (particleModel != value) {
            particleModel = value;
            emit particleModelChanged();
            setupSprings();
        }
    }

    float getSpringConstant() const { return ks; }

    void setSpringConstant(float value) {
        ks = value;
        for (auto &spring: springs)
            spring.ks = value;
        emit springConstantChanged();
    }

    Spring &getSpring(std::size_t ind) {
        return springs[ind];
    }

signals:
    void particleModelChanged();
    void springConstantChanged();
private:
    ParticleModel* particleModel;
    float ks;

    std::vector<Spring> springs;

    void setupSprings();

    void createSprings();
};


#endif //PSW_SPRING_MODEL_HPP
