// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#include <QDebug>
#include <QQuaternion>

#include <cmath>

#include "GelloSimulator.hpp"

const float GelloSimulator::integrationStep = 0.02f;
const float GelloSimulator::defaultViscosityConstant = 0.1f;
const float GelloSimulator::defaultFrameSpringConstant = 5.0f;

GelloSimulator::GelloSimulator(QObject *parent)
        : QObject(parent),
          particleModel(nullptr),
          springModel(nullptr),
          cubeWalls(nullptr),
          frameSpringConstant(defaultFrameSpringConstant),
          viscosityConstant(defaultViscosityConstant),
          framePosition(0, 0, 0), frameRotation(0, 0, 0)
{
    timer = new QTimer{this};
    timer->setInterval(stepIntervalMsec);
    elapsedTimer = new QElapsedTimer;
    leftover = 0;
    QObject::connect(timer, &QTimer::timeout, this, &GelloSimulator::tick);

    timer->start();
}

void GelloSimulator::tick()
{
    if(particleModel == nullptr || springModel == nullptr)
        return;

    auto elapsedMsec = elapsedTimer->elapsed() + leftover;

    elapsedTimer->restart();

    leftover = elapsedMsec % static_cast<int>(integrationStep * 1000);

    for(int i = 0; i < elapsedMsec / static_cast<int>(integrationStep * 1000); ++i) {

        clearForces();

        applyForces();

        massDivide();

        integrate();

        collide();

        particleModel->signalPositionVelocityUpdate();

    }

}

void GelloSimulator::clearForces()
{
    for (std::size_t x = 0; x < ParticleModel::size; ++x)
    {
        for (std::size_t y = 0; y < ParticleModel::size; ++y)
        {
            for (std::size_t z = 0; z < ParticleModel::size; ++z)
            {
                particleModel->getParticle(x, y, z).force = QVector3D(0, 0, 0);
                particleModel->getParticle(x, y, z).previousPosition = particleModel->getParticle(x, y, z).position;
            }
        }
    }
}

void GelloSimulator::applyForces()
{
    /* Springiness forces */
    auto count = static_cast<std::size_t>(springModel->rowCount(QModelIndex()));
    for (std::size_t i = 0; i < count; i++) {
        springModel->getSpring(i).applyForce();
    }
    /* Frame springiness forces */
    for (std::size_t x = 0; x < ParticleModel::size; x += ParticleModel::size - 1) {
        for (std::size_t y = 0; y < ParticleModel::size; y += ParticleModel::size - 1) {
            for (std::size_t z = 0; z < ParticleModel::size; z += ParticleModel::size - 1) {
				Particle frameVertex;
				frameVertex.position = framePosition +
					QQuaternion::fromEulerAngles(frameRotation).rotatedVector(
						QVector3D((x != 0) * ParticleModel::Extent - ParticleModel::Extent / 2,
						(y != 0) * ParticleModel::Extent - ParticleModel::Extent / 2,
							(z != 0) * ParticleModel::Extent - ParticleModel::Extent / 2)
					);
				Spring frameSpring;
				frameSpring.from = &frameVertex;
				frameSpring.to = &particleModel->getParticle(x, y, z);
				frameSpring.l0 = 0.0;
				frameSpring.ks = frameSpringConstant;
                frameSpring.applyForce();
            }
        }
    }
    /* Viscosity forces */
    for (std::size_t x = 0; x < ParticleModel::size; ++x) {
        for (std::size_t y = 0; y < ParticleModel::size; ++y) {
            for (std::size_t z = 0; z < ParticleModel::size; ++z) {
                particleModel->getParticle(x, y, z).force -= viscosityConstant * particleModel->getParticle(x, y, z).velocity;
            }
        }
    }
}

void GelloSimulator::massDivide()
{
    for (std::size_t x = 0; x < ParticleModel::size; ++x) {
        for (std::size_t y = 0; y < ParticleModel::size; ++y) {
            for (std::size_t z = 0; z < ParticleModel::size; ++z) {
                particleModel->getParticle(x, y, z).force /= particleModel->getParticle(x, y, z).mass;
            }
        }
    }
}

void GelloSimulator::integrate() {
    for (std::size_t x = 0; x < ParticleModel::size; ++x) {
        for (std::size_t y = 0; y < ParticleModel::size; ++y) {
            for (std::size_t z = 0; z < ParticleModel::size; ++z) {
                auto &particle = particleModel->getParticle(x, y, z);
                particle.position += integrationStep * particle.velocity;
                particle.velocity += integrationStep * particle.force;
            }
        }
    }
}

void GelloSimulator::collide() {
    if (cubeWalls == nullptr)
        return;

    for (std::size_t x = 0; x < ParticleModel::size; ++x) {
        for (std::size_t y = 0; y < ParticleModel::size; ++y) {
            for (std::size_t z = 0; z < ParticleModel::size; ++z) {
                cubeWalls->processParticle(particleModel->getParticle(x, y, z));
            }
        }
    }
}
