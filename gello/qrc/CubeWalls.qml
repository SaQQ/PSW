import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0

Entity {
    id: root

    property real xMin: -30.0
    property real xMax: 30.0
    property real yMin: -30.0
    property real yMax: 30.0
    property real zMin: -30.0
    property real zMax: 30.0

    property real thickness: 1.0

    readonly property real xSize: xMax - xMin
    readonly property real ySize: yMax - yMin
    readonly property real zSize: zMax - zMin

    CubeWall {
        id: xMinWall
        xExtent: root.thickness
        yExtent: root.ySize
        zExtent: root.zSize
        position: Qt.vector3d(xMax, 0, 0)
    }

    CubeWall {
        id: xMaxWall
        xExtent: root.thickness
        yExtent: root.ySize
        zExtent: root.zSize
        position: Qt.vector3d(xMin, 0, 0)
    }

    CubeWall {
        id: yMinWall
    }

    CubeWall {
        id: yMaxWall
    }

    CubeWall {
        id: zMinWall
    }

    CubeWall {
        id: zMaxWall
    }
}