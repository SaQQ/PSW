import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.0

Entity {
    id: root

    property real xExtent: 1.0
    property real yExtent: 1.0
    property real zExtent: 1.0

    property vector3d position: Qt.vector3d(0, 0, 0)

    property Material material: PhongMaterial {
        id: material
    }

    CuboidMesh {
        id: mesh
        xExtent: root.xExtent
        yExtent: root.yExtent
        zExtent: root.zExtent
    }

    Transform {
        id: transform
        translation: root.position
    }

    components: [mesh, material, transform]
}