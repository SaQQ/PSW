// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#include <cmath>

#include <QDebug>

#include "Simulator.hpp"

Simulator::Simulator(QObject *parent) : QAbstractListModel(parent),
                                        speed{1.0},
                                        running{false}, paused{false} {

    timer = new QTimer{this};
    timer->setInterval(30);
    elapsedTimer = new QElapsedTimer;
    leftover = 0;
    QObject::connect(timer, &QTimer::timeout, this, &Simulator::tick);

    reset();
}

bool Simulator::start() {
    if(running)
        return false;

	currentState.time = 0.0;
	currentState.interpFactor = 0.0;

    emit currentStateChanged();

    timer->start();
    elapsedTimer->start();
    leftover = 0;

    running = true;
    emit runningChanged();
    return true;
}

bool Simulator::togglePause() {
    if(!running)
        return false;

    paused = !paused;

    if(paused)
        timer->stop();
    else
    {
        timer->start();
        elapsedTimer->restart();
        leftover = 0;
    }

    emit pausedChanged();
    return true;
}

bool Simulator::reset() {
    running = paused = false;
    emit runningChanged();
    emit pausedChanged();

    timer->stop();

	currentState.time = 0.0;
	currentState.interpFactor = 0.0;

    emit currentStateChanged();

    return true;
}

void Simulator::tick() {
    if(!running || paused)
        return;

    auto elapsedMsec = elapsedTimer->elapsed() + leftover;

    elapsedTimer->restart();

    leftover = elapsedMsec % timer->interval();

    for(int i = 0; i < elapsedMsec / timer->interval(); ++i)
    {
        currentState.time += (elapsedMsec / 1000.0);
        currentState.interpFactor += (elapsedMsec / 1000.0 * speed);
        if (currentState.interpFactor > 1.0)
            currentState.interpFactor -= 1.0;
    }

    emit currentStateChanged();
    emit step();
}

QVector3D Simulator::getPosition(double t) const {
    return startPosition * (1 - t) + endPosition * t;
}

QVector3D Simulator::getCurrentPosition() const {
    return getPosition(currentState.interpFactor);
}

QQuaternion Simulator::getQuaternion(double t) const {
    auto start = startQuaternion;
    auto end = endQuaternion;
    auto dot = std::clamp(QQuaternion::dotProduct(start, end), -1.0f, 1.0f);
    if (dot < 0.0f) {
        dot = -dot;
        start = -start;
    }
    if (useSlerp) {
        const auto omega = std::acos(dot);
        if (std::abs(std::sin(omega)) < 0.00001f)
            return (start + end) / 2.0;
        return std::sin((1.0 - t) * omega) / std::sin(omega) * start+ std::sin(t * omega) / std::sin(omega) * end;
    } else {
        return (start* (1.0 - t) + end * t).normalized();
    }
}

QQuaternion Simulator::getCurrentQuaternion() const {
    return getQuaternion(currentState.interpFactor);
}

float normalizeAngle(float angle) {
    return std::fmod(angle, 360.0f);
}

QVector3D normalizeAngles(const QVector3D &angles) {
    return QVector3D(normalizeAngle(angles.x()), normalizeAngle(angles.y()), normalizeAngle(angles.z()));
}

float interpolateAngle(float start, float end, float t) {
    start = normalizeAngle(start);
    end = normalizeAngle(end);

    if (std::abs(start - end) < 180.0f) {
        return start + (end - start) * t;
    } else {
        return start - (360.0f - std::abs(end - start)) * t;
    }
}

QVector3D Simulator::getEulerAngles(double t) const {
    auto x = interpolateAngle(startEulerAngles.x(), endEulerAngles.x(), t);
    auto y = interpolateAngle(startEulerAngles.y(), endEulerAngles.y(), t);
    auto z = interpolateAngle(startEulerAngles.z(), endEulerAngles.z(), t);

    return QVector3D{x, y, z};
}

QVector3D Simulator::getCurrentEulerAngles() const {
    return getEulerAngles(currentState.interpFactor);
}

QHash<int, QByteArray> Simulator::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PositionRole] = "position";
    roles[EulerAnglesRole] = "eulerAngles";
    roles[QuaternionRole] = "quaternion";
    return roles;
}

QVariant Simulator::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto row = index.row();
    auto dt = 1.0 / (framesCount - 1);
    auto t = row * dt;

    switch(role) {
        case PositionRole:
            return QVariant(getPosition(t));
        case EulerAnglesRole:
            return QVariant(getEulerAngles(t));
        case QuaternionRole:
            return QVariant(getQuaternion(t));
        default:
            return QVariant();
    }

}


