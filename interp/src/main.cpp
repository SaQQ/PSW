// Created by Paweł Sobótka on 18.08.17.
// Rail-Mil Computers Sp. z o.o.

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlContext>

#include "Simulator.hpp"
#include "QQuaternionUtils.hpp"

int main(int ac, char **av)
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(ac, av);

    QQmlApplicationEngine engine;

    qmlRegisterType<Simulator>("PSW", 1, 0, "Simulator");
    qmlRegisterSingletonType<QQuaternionUtils>("PSW", 1, 0, "QuaternionUtils", QQuaternionUtilsSingletonProvider);

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
