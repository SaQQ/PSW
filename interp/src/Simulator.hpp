// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef PSW_SIMULATOR_HPP
#define PSW_SIMULATOR_HPP

#include <QAbstractListModel>
#include <QString>
#include <QLineSeries>
#include <QQmlListProperty>

#include <QTimer>
#include <QElapsedTimer>

#include <QQuaternion>

class Simulator : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(bool running READ getRunning NOTIFY runningChanged)
    Q_PROPERTY(bool paused READ getPaused NOTIFY pausedChanged)

    Q_PROPERTY(double time READ getTime NOTIFY currentStateChanged)
    Q_PROPERTY(double interpFactor READ getInterpFactor NOTIFY currentStateChanged)

    Q_PROPERTY(double speed READ getSpeed WRITE setSpeed NOTIFY speedChanged)

    Q_PROPERTY(QVector3D startPosition READ getStartPosition WRITE setStartPosition NOTIFY startPositionChanged)
    Q_PROPERTY(QVector3D endPosition READ getEndPosition WRITE setEndPosition NOTIFY endPositionChanged)
    Q_PROPERTY(QVector3D currentPosition READ getCurrentPosition NOTIFY currentStateChanged)

    Q_PROPERTY(QQuaternion startQuaternion READ getStartQuaternion WRITE setStartQuaternion NOTIFY startQuaternionChanged)
    Q_PROPERTY(QQuaternion endQuaternion READ getEndQuaternion WRITE setEndQuaternion NOTIFY endQuaternionChanged)
    Q_PROPERTY(QQuaternion currentQuaternion READ getCurrentQuaternion NOTIFY currentStateChanged)

    Q_PROPERTY(QVector3D startEulerAngles READ getStartEulerAngles WRITE setStartEulerAngles NOTIFY startEulerAnglesChanged)
    Q_PROPERTY(QVector3D endEulerAngles READ getEndEulerAngles WRITE setEndEulerAngles NOTIFY endEulerAnglesChanged)
    Q_PROPERTY(QVector3D currentEulerAngles READ getCurrentEulerAngles NOTIFY currentStateChanged)

    Q_PROPERTY(bool useSlerp READ getUseSlerp WRITE setUseSlerp NOTIFY useSlerpChanged)

    Q_PROPERTY(int framesCount READ getFramesCount WRITE setFramesCount NOTIFY framesCountChanged)
    Q_PROPERTY(bool showFrames READ getShowFrames WRITE setShowFrames NOTIFY showFramesChanged)

public:
    explicit Simulator(QObject* parent = nullptr);

    Q_INVOKABLE bool start();

    Q_INVOKABLE bool togglePause();

    Q_INVOKABLE bool reset();

    Q_INVOKABLE QQuaternion fromToQuaternion(const QVector3D &from, const QVector3D &to) {
        return QQuaternion::rotationTo(from, to);
    }

    Q_INVOKABLE QVector3D rotateVector(const QQuaternion &quaternion, const QVector3D &vec) {
        return quaternion.rotatedVector(vec);
    }

    bool getRunning() const { return running; }

    bool getPaused() const { return paused; }

    double getTime() const { return currentState.time; }

    double getInterpFactor() const { return currentState.interpFactor; }

    double getSpeed() const { return speed; }

    void setSpeed(double value) {
        speed = value;
        emit speedChanged();
    }

    QVector3D getStartPosition() const { return startPosition; }

    void setStartPosition(const QVector3D &value) {
        if (startPosition != value) {
            startPosition = value;
            emit startPositionChanged();
            beginResetModel();
            endResetModel();
        }
    }

    QVector3D getEndPosition() const { return endPosition; }

    void setEndPosition(const QVector3D &value) {
        if (endPosition != value) {
            endPosition = value;
            emit endPositionChanged();
            beginResetModel();
            endResetModel();
        }
    }

    QVector3D getCurrentPosition() const;

    QQuaternion getStartQuaternion() const { return startQuaternion; }

    void setStartQuaternion(const QQuaternion &value) {
        if (startQuaternion != value) {
            startQuaternion = value;
            emit startQuaternionChanged();
            beginResetModel();
            endResetModel();
        }
    }

    QQuaternion getEndQuaternion() const { return endQuaternion; }

    void setEndQuaternion(const QQuaternion &value) {
        if (endQuaternion != value) {
            endQuaternion = value;
            emit endQuaternionChanged();
            beginResetModel();
            endResetModel();
        }
    }

    QQuaternion getCurrentQuaternion() const;

    QVector3D getStartEulerAngles() const { return startEulerAngles; }

    void setStartEulerAngles(const QVector3D &value) {
        if (startEulerAngles != value) {
            startEulerAngles = value;
            emit startEulerAnglesChanged();
            beginResetModel();
            endResetModel();
        }
    }

    QVector3D getEndEulerAngles() const { return endEulerAngles; }

    void setEndEulerAngles(const QVector3D &value) {
        if (endEulerAngles != value) {
            endEulerAngles = value;
            emit endEulerAnglesChanged();
            beginResetModel();
            endResetModel();
        }
    }

    QVector3D getCurrentEulerAngles() const;

    QVector3D getPosition(double t) const;

    QVector3D getEulerAngles(double t) const;

    QQuaternion getQuaternion(double t) const;

    bool getUseSlerp() const { return useSlerp; }

    void setUseSlerp(bool value) {
        if (useSlerp != value) {
            useSlerp = value;
            emit useSlerpChanged();
            beginResetModel();
            endResetModel();
        }
    }

    int getFramesCount() const {
        return framesCount;
    }

    void setFramesCount(int value) {
        if (framesCount != value) {
            framesCount = value;
            emit framesCountChanged();
            beginResetModel();
            endResetModel();
        }
    }

    bool getShowFrames() const {
        return showFrames;
    }

    void setShowFrames(bool value) {
        if (showFrames != value) {
            showFrames = value;
            emit showFramesChanged();
        }
    }

    enum SimulatorRoles {
        PositionRole = Qt::UserRole + 1,
        EulerAnglesRole,
        QuaternionRole
    };

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent) const override { return framesCount; }

    QVariant data(const QModelIndex &index, int role) const override;

signals:
    void runningChanged();
    void cubeSizeChanged();
    void pausedChanged();

    void currentStateChanged();

    void speedChanged();

    void step();

    void startPositionChanged();
    void endPositionChanged();

    void startQuaternionChanged();
    void endQuaternionChanged();

    void startEulerAnglesChanged();
    void endEulerAnglesChanged();

    void useSlerpChanged();

    void framesCountChanged();
    void showFramesChanged();

    void framesChanged();

private slots:
    void tick();

private:

    struct state {
        double time;
        double interpFactor;
    } currentState;

    double speed;

    bool running, paused;

    QTimer* timer;
    QElapsedTimer* elapsedTimer;
    qint64 leftover;

    QVector3D startPosition, endPosition;
    QVector3D startEulerAngles, endEulerAngles;
    QQuaternion startQuaternion, endQuaternion;

    bool useSlerp;

    int framesCount;
    bool showFrames;

};


#endif //PSW_SIMULATOR_HPP
