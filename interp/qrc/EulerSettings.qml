import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "controls"

GridLayout {
    id: root

    property vector3d startPos: Qt.vector3d(0, 0, 0)
    property vector3d endPos: Qt.vector3d(0, 0, 0)

    property vector3d startAngles: Qt.vector3d(0, 0, 0)
    property vector3d endAngles: Qt.vector3d(0, 0, 0)

    signal startPositionEdited()
    signal endPositionEdited()

    signal startAnglesEdited()
    signal endAnglesEdited()

    function setStartPosition(pos) {
        startXTextField.text = pos.x.toFixed(startXTextField.decimals)
        startYTextField.text = pos.y.toFixed(startYTextField.decimals)
        startZTextField.text = pos.z.toFixed(startYTextField.decimals)
        root.startPos = pos
    }

    function setEndPosition(pos) {
        endXTextField.text = pos.x.toFixed(endXTextField.decimals)
        endYTextField.text = pos.y.toFixed(endYTextField.decimals)
        endZTextField.text = pos.z.toFixed(endZTextField.decimals)
        root.endPos = pos
    }

    function setStartAngles(angles) {
        startXRotationTextField.text = angles.x.toFixed(startXRotationTextField.decimals)
        startYRotationTextField.text = angles.y.toFixed(startYRotationTextField.decimals)
        startZRotationTextField.text = angles.z.toFixed(startZRotationTextField.decimals)
        startAngles = angles
    }

    function setEndAngles(angles) {
        endXRotationTextField.text = angles.x.toFixed(endXRotationTextField.decimals)
        endYRotationTextField.text = angles.y.toFixed(endYRotationTextField.decimals)
        endZRotationTextField.text = angles.z.toFixed(endZRotationTextField.decimals)
        endAngles = angles
    }

    property real maxCoord: 10000.0
    property real minCoord: -10000.0
    property int coordDecimals: 2
    property real minAngle: -360.0
    property real maxAngle: 360.0
    property int angleDecimals: 1

    columns: 7

    Text {

    }

    Text {
        text: "X"
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
    }

    Text {
        text: "Y"
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
    }

    Text {
        text: "Z"
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
    }

    Text {
        text: qsTr("X Rotation")
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
    }

    Text {
        text: qsTr("Y Rotation")
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
    }

    Text {
        text: qsTr("Z Rotation")
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
    }

    Text {
        text: qsTr("Start")
        color: "#f5f5f5"
    }

    DoubleTextField {
        id: startXTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeStartPos()
    }

    DoubleTextField {
        id: startYTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeStartPos()
    }

    DoubleTextField {
        id: startZTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeStartPos()
    }

    DoubleTextField {
        id: startXRotationTextField
        Layout.fillWidth: true
        minValue: root.minAngle
        maxValue: root.maxAngle
        decimals: root.angleDecimals
        onEditingFinished: root.normalizeStartAngles()
    }

    DoubleTextField {
        id: startYRotationTextField
        Layout.fillWidth: true
        minValue: root.minAngle
        maxValue: root.maxAngle
        decimals: root.angleDecimals
        onEditingFinished: root.normalizeStartAngles()
    }

    DoubleTextField {
        id: startZRotationTextField
        Layout.fillWidth: true
        minValue: root.minAngle
        maxValue: root.maxAngle
        decimals: root.angleDecimals
        onEditingFinished: root.normalizeStartAngles()
    }

    Text {
        text: qsTr("End")
        color: "#f5f5f5"
    }

    DoubleTextField {
        id: endXTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeEndPos()
    }

    DoubleTextField {
        id: endYTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeEndPos()
    }

    DoubleTextField {
        id: endZTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeEndPos()
    }

    DoubleTextField {
        id: endXRotationTextField
        Layout.fillWidth: true
        minValue: root.minAngle
        maxValue: root.maxAngle
        decimals: root.angleDecimals
        onEditingFinished: root.normalizeEndAngles()
    }

    DoubleTextField {
        id: endYRotationTextField
        Layout.fillWidth: true
        minValue: root.minAngle
        maxValue: root.maxAngle
        decimals: root.angleDecimals
        onEditingFinished: root.normalizeEndAngles()
    }

    DoubleTextField {
        id: endZRotationTextField
        Layout.fillWidth: true
        minValue: root.minAngle
        maxValue: root.maxAngle
        decimals: root.angleDecimals
        onEditingFinished: root.normalizeEndAngles()
    }

    Text {
        color: "#f5f5f5"
        Layout.columnSpan: 7
        text: "Interpolation: " + "[" + vecToStr(root.startPos) + " " + vecToDegStr(root.startAngles) + "]" + " → " + "[" + vecToStr(root.endPos) + " " + vecToDegStr(root.endAngles) + "]"

        function vecToStr(vec) {
            return "[" + vec.x.toFixed(1) + ", " + vec.y.toFixed(1) + ", " + vec.z.toFixed(1) + "]"
        }

        function vecToDegStr(vec) {
            return "[" + vec.x.toFixed(1) + "°, " + vec.y.toFixed(1) + "°, " + vec.z.toFixed(1) + "°]"
        }
    }

    function normalizeStartPos() {
        var pos = Qt.vector3d(
            startXTextField.value,
            startYTextField.value,
            startZTextField.value
        )
        root.setStartPosition(pos)
        root.startPositionEdited()
    }

    function normalizeEndPos() {
        var pos = Qt.vector3d(
            endXTextField.value,
            endYTextField.value,
            endZTextField.value
        )
        root.setEndPosition(pos)
        root.endPositionEdited()
    }

    function normalizeStartAngles() {
        var angles = Qt.vector3d(
            startXRotationTextField.value,
            startYRotationTextField.value,
            startZRotationTextField.value
        )
        root.setStartAngles(angles)
        root.startAnglesEdited()
    }

    function normalizeEndAngles() {
        var angles = Qt.vector3d(
            endXRotationTextField.value,
            endYRotationTextField.value,
            endZRotationTextField.value
        )
        root.setEndAngles(angles)
        root.endAnglesEdited()
    }

}