import Qt3D.Core 2.0
import Qt3D.Render 2.0

RenderSettings {
    id: quadViewportFrameGraph

    property alias leftCamera: cameraSelectorLeftViewport.camera;
    property alias rightCamera: cameraSelectorRightViewport.camera;
    property alias window: surfaceSelector.surface

    readonly property Layer eulerAnglesLayer: Layer { id: l1 }
    readonly property Layer quaternionLayer: Layer { id: l2 }

    activeFrameGraph: RenderSurfaceSelector {
        id: surfaceSelector

        Viewport {
            id: mainViewport
            normalizedRect: Qt.rect(0, 0, 1, 1)

            ClearBuffers {
                buffers: ClearBuffers.ColorDepthBuffer
                clearColor: "gray"

                LayerFilter {
                    layers: []
                }
            }

            Viewport {
                id: topLeftViewport
                normalizedRect: Qt.rect(0, 0, 0.5, 1)

                CameraSelector {
                    id: cameraSelectorLeftViewport

                    LayerFilter {
                        layers: [ l1 ]
                    }
                }
            }

            Viewport {
                id: topRightViewport
                normalizedRect: Qt.rect(0.5, 0, 0.5, 1)

//                LayerFilter {
//                    layers: [ l2 ]

                    CameraSelector { id: cameraSelectorRightViewport }
//                }
            }

        }
    }
}
