import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import "controls"

import PSW 1.0 as PSW

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: "Rotation Interpolation"

    width: 1200
    height: 800

    PSW.Simulator {
        id: simulator

        speed: simulationControl.speed

        startPosition: eulerSettings.startPos
        endPosition: eulerSettings.endPos
        startEulerAngles: eulerSettings.startAngles
        endEulerAngles: eulerSettings.endAngles
        startQuaternion: quaternionSettings.startQuaternion
        endQuaternion: quaternionSettings.endQuaternion

        Behavior on startPosition {
            Vector3dAnimation { duration: 400; easing.type: Easing.InOutQuart }
        }

        Behavior on endPosition {
            Vector3dAnimation { duration: 400; easing.type: Easing.InOutQuart }
        }

        Behavior on startEulerAngles {
            Vector3dAnimation { duration: 400; easing.type: Easing.InOutQuart }
        }

        Behavior on endEulerAngles {
            Vector3dAnimation { duration: 400; easing.type: Easing.InOutQuart }
        }

        useSlerp: quaternionSettings.mode === "slerp"

        framesCount: simulationControl.framesCount
        showFrames: simulationControl.showFrames

        Component.onCompleted: {
            simulator.start()
        }

    }

    footer: ToolBar {
        height: 24
        Row {
            anchors.fill: parent
            anchors.margins: 4

            Label {
                text: footerText()

                function footerText() {
                    if(simulator.running) {
                        if(simulator.paused) {
                            return qsTr("Running (Paused)")
                        } else {
                            return qsTr("Running")
                        }
                    } else {
                        return qsTr("Idle")
                    }
                }

            }
        }
    }

    Rectangle {
        id: background

        color: "#424242"

        anchors.fill: parent

        Item {
            id: bottomPaneContainer

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            height: 150

            Item {
                id: eulerItem

                anchors {
                    left: parent.left
                    top: parent.top
                    bottom: parent.bottom
                }

                width: parent.width / 2

                SectionPlate {
                    anchors.fill: parent
                    anchors.margins: 5
                    anchors.topMargin: 0
                    anchors.rightMargin: 2

                    header: "Euler angles"

                    EulerSettings {
                        id: eulerSettings
                        anchors.fill: parent

                        onStartPositionEdited: quaternionSettings.setStartPosition(startPos)
                        onEndPositionEdited: quaternionSettings.setEndPosition(endPos)

                        onStartAnglesEdited: quaternionSettings.setStartQuaternion(PSW.QuaternionUtils.fromEulerAngles(startAngles))
                        onEndAnglesEdited: quaternionSettings.setEndQuaternion(PSW.QuaternionUtils.fromEulerAngles(endAngles))
                    }

                }
            }

            Item {
                anchors {
                    left: eulerItem.right
                    right: parent.right
                    top: parent.top
                    bottom: parent.bottom
                }

                SectionPlate {
                    anchors.fill: parent
                    anchors.margins: 5
                    anchors.topMargin: 0
                    anchors.leftMargin: 2

                    header: "Quaternions"

                    QuaternionSettings {
                        id: quaternionSettings
                        anchors.fill: parent

                        onStartPositionEdited: eulerSettings.setStartPosition(startPos)
                        onEndPositionEdited: eulerSettings.setEndPosition(endPos)

                        onStartQuaternionEdited: eulerSettings.setStartAngles(PSW.QuaternionUtils.toEulerAngles(startQuaternion))
                        onEndQuaternionEdited: eulerSettings.setEndAngles(PSW.QuaternionUtils.toEulerAngles(endQuaternion))
                    }
                }
            }

        }

        Item {
            id: centerPaneContainer

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                bottom: bottomPaneContainer.top
            }

            SectionPlate {
                anchors.fill: parent
                anchors.margins: 5

                header: "Visualization"

                Scene3D {
                    id: scene3d
                    anchors.fill: parent
                    anchors.margins: 0
                    focus: true
                    aspects: ["input", "logic", "render"]
                    cameraAspectRatioMode: Scene3D.UserAspectRatio

                    Entity {
                        id: rootNode
                        components: [frameGraph, inputSettings]

                        RenderSettings {
                            id: frameGraph

                            activeFrameGraph: RenderSurfaceSelector {
                                id: surfaceSelector

                                Viewport {
                                    id: mainViewport
                                    normalizedRect: Qt.rect(0, 0, 1, 1)

                                    ClearBuffers {
                                        buffers: ClearBuffers.ColorDepthBuffer
                                        clearColor: "gray"

                                        LayerFilter {
                                            layers: []
                                        }
                                    }

                                    Viewport {
                                        id: topLeftViewport
                                        normalizedRect: Qt.rect(0, 0, 0.5, 1)

                                        CameraSelector {
                                            camera: camera

                                            LayerFilter {
                                                layers: [ l1 ]
                                            }
                                        }
                                    }

                                    Viewport {
                                        id: topRightViewport
                                        normalizedRect: Qt.rect(0.5, 0, 0.5, 1)

                                        CameraSelector {
                                            camera: camera

                                            LayerFilter {
                                                layers: [ l2 ]
                                            }
                                        }
                                    }

                                }
                            }
                        }

                        Layer { id: l1 }
                        Layer { id: l2 }

                        // Event Source will be set by the Qt3DQuickWindow
                        InputSettings { id: inputSettings }

                        Entity {
                            id: cameraSet

                            Camera {
                                id: camera
                                projectionType: CameraLens.PerspectiveProjection
                                fieldOfView: 30
                                aspectRatio: scene3d.width / 2 / scene3d.height
                                nearPlane : 0.1
                                farPlane : 1000.0
                                position: Qt.vector3d( 1.0, 7.0, 7.0 )
                                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
                            }

                            OrbitCameraController {
                                camera: camera
                                linearSpeed: 10
                                lookSpeed: 1000
                            }

                        }

                        Entity {
                            id: sceneRoot

                            Tripod {
                                id: startTripod

                                Transform {
                                    id: startTransform
                                    translation: simulator.startPosition
                                    rotationX: simulator.startEulerAngles.x
                                    rotationY: simulator.startEulerAngles.y
                                    rotationZ: simulator.startEulerAngles.z
                                }

                                layers: [l1, l2]

                                components: [
                                    l1, l2,
                                    startTransform
                                ]
                            }

                            Tripod {
                                id: endTripod

                                Transform {
                                    id: endTransform
                                    translation: simulator.endPosition
                                    rotationX: simulator.endEulerAngles.x
                                    rotationY: simulator.endEulerAngles.y
                                    rotationZ: simulator.endEulerAngles.z
                                }

                                layers: [l1, l2]

                                components: [
                                    l1, l2,
                                    endTransform
                                ]
                            }

                            Tripod {
                                id: currentEulerAnglesTripod

                                Transform {
                                    id: currentEulerAnglesTransform
                                    translation: simulator.currentPosition
                                    rotationX: simulator.currentEulerAngles.x
                                    rotationY: simulator.currentEulerAngles.y
                                    rotationZ: simulator.currentEulerAngles.z
                                }

                                layers: [l1]

                                components: [
                                    l1,
                                    currentEulerAnglesTransform
                                ]
                            }

                            NodeInstantiator {
                                active: simulator.showFrames
                                model: simulator
                                delegate: Tripod {
                                    id: delegateRoot
                                    Transform {
                                        id: eulerFrameTransform
                                        translation: model.position
                                        rotationX: model.eulerAngles.x
                                        rotationY: model.eulerAngles.y
                                        rotationZ: model.eulerAngles.z
                                    }
                                    layers: [l1]
                                    components: [
                                        l1,
                                        eulerFrameTransform
                                    ]
                                }
                            }

                            Tripod {
                                id: currentQuaternionTripod

                                Transform {
                                    id: currentQuaternionTransform
                                    translation: simulator.currentPosition
                                    rotation: simulator.currentQuaternion
                                }

                                layers: [l2]

                                components: [
                                    l2,
                                    currentQuaternionTransform
                                ]
                            }

                            NodeInstantiator {
                                active: simulator.showFrames
                                model: simulator
                                delegate: Tripod {
                                    id: delegateRoot
                                    Transform {
                                        id: eulerFrameTransform
                                        translation: model.position
                                        rotation: model.quaternion
                                    }
                                    layers: [l2]
                                    components: [
                                        l2,
                                        eulerFrameTransform
                                    ]
                                }
                            }

                        } // sceneRoot
                    } // rootNode

                }

                Item {
                    id: overlay
                    anchors.fill: parent

                    Text {
                        text: qsTr("Euler Angles")
                        anchors {
                            bottom: parent.bottom
                            left: parent.left
                            margins: 10
                        }
                        color: "white"
                        font.pointSize: 16
                    }

                    Text {
                        text: qsTr("Quaternions") + " [" + quaternionSettings.mode + "]"
                        anchors {
                            bottom: parent.bottom
                            right: parent.right
                            margins: 10
                        }
                        color: "white"
                        font.pointSize: 16
                    }
                }

                Rectangle {

                    radius: 3

                    color: "#616161"

                    anchors {
                        top: parent.top
                        left: parent.left
                        margins: 5
                    }

                    width: 400
                    height: 56

                    SimulationControl {
                        id: simulationControl
                        anchors.fill: parent
                        anchors.margins: 5

                        time: simulator.time
                        interpFactor: simulator.interpFactor
                    }

                }

            }
        }

    }

}
