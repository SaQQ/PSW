import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "controls"

RowLayout {
    id: root

    property real time: 0.0
    property real interpFactor: 0.0

    property alias speed: speedSlider.value

    property int framesCount: 10
    property bool showFrames: false

    spacing: 8

    ColumnLayout {

        Layout.fillHeight: true

        Text {
            color: "#f5f5f5"
            text: "Time: " + root.time.toFixed(3) + " s"
        }

        Text {
            color: "#f5f5f5"
            text: "Factor: " + root.interpFactor.toFixed(3)
        }

    }

    ColumnLayout {

        Layout.fillHeight: true

        Text {
            color: "#f5f5f5"
            font.pointSize: 8
            text: "Speed: " + root.speed.toFixed(3)
        }

        Slider {
            id: speedSlider
            Layout.fillHeight: true
            from: 0.1
            to: 3
            value: 1
            Layout.fillWidth: true
        }

    }

    ColumnLayout {

        Layout.fillHeight: true

        Button {
            id: framesButton

            text: root.showFrames ? qsTr("Frames ON") : qsTr("Frames OFF")

            implicitHeight: 20

            Layout.fillWidth: true

            onClicked: root.showFrames = !root.showFrames
        }

        Rectangle {
            id: framesSpinBox

            color: "white"

            Layout.fillHeight: true
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent

                Button {
                    Layout.fillHeight: true

                    implicitWidth: 30

                    text: "-"

                    onClicked: if (root.framesCount > 1) root.framesCount--
                }

                Text {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter

                    text: root.framesCount
                }

                Button {
                    Layout.fillHeight: true

                    implicitWidth: 30

                    text: "+"

                    onClicked: root.framesCount++
                }

            }
        }

    }

}