import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Entity {
    id: tripod

    property var layers: []

    Vector {
        id: xVector
        coordinates: Qt.vector3d(1, 0, 0)
        color: "red"
        layers: tripod.layers
    }

    Vector {
        id: yVector
        coordinates: Qt.vector3d(0, 1, 0)
        color: "green"
        layers: tripod.layers
    }

    Vector {
        id: zVector
        coordinates: Qt.vector3d(0, 0, 1)
        color: "blue"
        layers: tripod.layers
    }

}