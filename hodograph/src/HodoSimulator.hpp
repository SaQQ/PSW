// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef PSW_HODO_SIMULATOR_HPP
#define PSW_HODO_SIMULATOR_HPP

#include <cmath>

#include <random>

#include <QObject>
#include <QString>
#include <QLineSeries>

#include <QElapsedTimer>
#include <QTimer>

inline constexpr double radiansToDegrees(double radians) noexcept {
    return radians / M_PI * 180.0;
}

inline constexpr double degreesToRadians(double degrees) noexcept {
    return degrees / 180.0 * M_PI;
}

class HodoSimulator : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool running READ getRunning NOTIFY runningChanged)
    Q_PROPERTY(bool paused READ getPaused NOTIFY pausedChanged)

    Q_PROPERTY(double time READ getTime NOTIFY currentStateChanged)

    Q_PROPERTY(double angle READ getAngle NOTIFY currentStateChanged)

    Q_PROPERTY(double position READ getPosition NOTIFY currentStateChanged)
    Q_PROPERTY(double velocity READ getVelocity NOTIFY currentStateChanged)
    Q_PROPERTY(double acceleration READ getAcceleration NOTIFY currentStateChanged)
    Q_PROPERTY(double error READ getError NOTIFY currentStateChanged)
    Q_PROPERTY(double disturbedLength READ getDisturbedLength NOTIFY currentStateChanged)

    Q_PROPERTY(double radius READ getRadius WRITE setRadius NOTIFY radiusChanged)
    Q_PROPERTY(double length READ getLength WRITE setLength NOTIFY lengthChanged)
    Q_PROPERTY(double integrationStep READ getIntegrationStep WRITE setIntegrationStep NOTIFY integrationStepChanged)
    Q_PROPERTY(double angularSpeed READ getAngularSpeed WRITE setAngularSpeed NOTIFY angularSpeedChanged)

    Q_PROPERTY(double errorDeviation READ getErrorDeviation WRITE setErrorDeviation NOTIFY errorDeviationChanged)
public:
    static const double DefaultIntegrationStep;
    static const double DefaultRadius;
    static const double DefaultLength;
    static const double DefaultAngularSpeed;
    static const double DefaultErrorDeviation;

    explicit HodoSimulator(QObject* parent = nullptr);

    Q_INVOKABLE bool start();

    Q_INVOKABLE bool togglePause();

    Q_INVOKABLE bool reset();

    bool getRunning() const { return running; }

    bool getPaused() const { return paused; }

    double getTime() const { return currentState.time; }

    double getAngle() const { return radiansToDegrees(currentState.angle); }

    double getPosition() const { return currentState.position; }

    double getVelocity() const;

    double getAcceleration() const;

    double getError() const { return currentState.error; }

    double getDisturbedLength() const { return length + currentState.error; }

    double getRadius() const { return radius; }

    void setRadius(double value);

    double getLength() const { return length; }

    void setLength(double value);

    double getIntegrationStep() const { return dt; }

    void setIntegrationStep(double value) {
        dt = value;
        timer->setInterval(static_cast<int>(dt * 1000));
        emit integrationStepChanged();
    }

    double getAngularSpeed() const { return radiansToDegrees(omega); }

    void setAngularSpeed(double value) {
        omega = degreesToRadians(value);
        emit angularSpeedChanged();
    }

    double getErrorDeviation() const { return errorDeviation; }

    void setErrorDeviation(double value) {
        errorDeviation = value;
        emit errorDeviationChanged();
    }

signals:
    void runningChanged();
    void pausedChanged();

    void currentStateChanged();

    void step();

    void radiusChanged();
    void lengthChanged();
    void integrationStepChanged();
    void angularSpeedChanged();

    void errorDeviationChanged();
private slots:
    void tick();

private:

    struct state {
        double time;
        double angle;
        double error;
        double position;
    } previousState, currentState, nextState;

    double radius, length, dt, omega, errorDeviation;

    bool running, paused;

    QTimer* timer;
    QElapsedTimer* elapsedTimer;
    qint64 leftover;

    std::random_device rd;
    std::mt19937 gen;

};


#endif //PSW_HODO_SIMULATOR_HPP
