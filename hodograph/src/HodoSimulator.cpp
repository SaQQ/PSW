// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "HodoSimulator.hpp"

#include <cmath>

#include <QtQml/QQmlListProperty>

const double HodoSimulator::DefaultIntegrationStep = 0.01;
const double HodoSimulator::DefaultRadius = 100.0;
const double HodoSimulator::DefaultLength = 280.0;
const double HodoSimulator::DefaultAngularSpeed = degreesToRadians(90.0);
const double HodoSimulator::DefaultErrorDeviation = 0.0;

namespace {

    inline double barProjection(double R, double L, double a) {
        return std::sqrt(L * L - R * R * std::sin(a) * std::sin(a));
    }

    inline double barEndPosition(double R, double L, double a) {
        return R * std::cos(a) + barProjection(R, L, a);
    }

}

HodoSimulator::HodoSimulator(QObject *parent) : QObject(parent),
    radius{DefaultRadius}, length{DefaultLength}, dt{DefaultIntegrationStep}, omega{DefaultAngularSpeed}, errorDeviation{DefaultErrorDeviation},
    running{false}, paused{false}, leftover{0}, gen{rd()} {

    timer = new QTimer{this};
    timer->setInterval(static_cast<int>(dt * 1000));
    elapsedTimer = new QElapsedTimer;

    QObject::connect(timer, &QTimer::timeout, this, &HodoSimulator::tick);

    reset();
}

bool HodoSimulator::start() {
    reset();

    leftover = 0;
    timer->start();
    elapsedTimer->start();

    running = true;
    emit runningChanged();

    return true;
}

bool HodoSimulator::togglePause() {
    if(!running)
        return false;

    paused = !paused;

    if(paused)
        timer->stop();
    else {
        timer->start();
        elapsedTimer->restart();
        leftover = 0;
    }

    emit pausedChanged();
    return true;
}

bool HodoSimulator::reset() {
    running = paused = false;

    emit runningChanged();
    emit pausedChanged();

    timer->stop();

    currentState.time = 0.0;
    currentState.angle = 0.0;
    currentState.error = 0.0;
    currentState.position = barEndPosition(radius, getDisturbedLength(), currentState.angle);
    previousState = nextState = currentState;

    emit currentStateChanged();

    return true;
}

void HodoSimulator::tick() {
    if(!running || paused)
        return;

    std::normal_distribution<double> errorDistribution{0.0, errorDeviation > 0.0 ? errorDeviation : 1.0};

    auto elapsedMsec = elapsedTimer->elapsed() + leftover;

    elapsedTimer->restart();

    leftover = elapsedMsec % timer->interval();

    for(int i = 0; i < elapsedMsec / timer->interval(); ++i) {
        state newState;

        newState.time = nextState.time + dt;
        newState.angle = nextState.angle + omega * dt;
        newState.error = errorDeviation > 0.0 ? errorDistribution(gen) : 0.0;
        newState.position = barEndPosition(radius, length + newState.error, newState.angle);
        if (newState.angle > 2 * M_PI)
            newState.angle -= 2 * M_PI;

        previousState = currentState;
        currentState = nextState;
        nextState = newState;
    }

    emit currentStateChanged();
    emit step();
}

void HodoSimulator::setRadius(double value) {
    radius = value;
    currentState.position = barEndPosition(radius, getDisturbedLength(), currentState.angle);
    emit radiusChanged();
    emit currentStateChanged();
}

void HodoSimulator::setLength(double value) {
    length = value;
    currentState.position = barEndPosition(radius, getDisturbedLength(), currentState.angle);
    emit lengthChanged();
    emit currentStateChanged();
}

double HodoSimulator::getVelocity() const {
    return (nextState.position - previousState.position) / (dt);
}

double HodoSimulator::getAcceleration() const {
    return (nextState.position - 2 * currentState.position + previousState.position) / (dt * dt);
}






