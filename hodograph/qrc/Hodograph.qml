import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import QtCharts 2.0

Item {
    id: root

    width: 400
    height: 400

    property real yMargin: 10.0

    function reset() {
        series.clear()

        positionAxis.min = -1.0
        positionAxis.max =  1.0
        velocityAxis.min = -1.0
        velocityAxis.max =  1.0

        scatterSeries.clear()
    }

    function step(t, p, v) {
        series.append(p, v)

        positionAxis.min = Math.min(positionAxis.min, p - root.yMargin)
        positionAxis.max = Math.max(positionAxis.max, p + root.yMargin)
        velocityAxis.min = Math.min(velocityAxis.min, v - root.yMargin)
        velocityAxis.max = Math.max(velocityAxis.max, v + root.yMargin)

        scatterSeries.clear()
        scatterSeries.append(p, v)
    }

    property real margins: 10

    ChartView {
        id: chart
        anchors.fill: parent
        antialiasing: true
        legend.visible: false
        margins { left: root.margins; right: root.margins; top: root.margins; bottom: root.margins }

        ValueAxis {
            id: positionAxis
            min: -1
            max: 1
        }

        ValueAxis {
            id: velocityAxis
            min: -1
            max: 1
        }

        LineSeries {
            id: series
            axisX: positionAxis
            axisY: velocityAxis
            useOpenGL: true
        }

        ScatterSeries {
            id: scatterSeries
            axisX: positionAxis
            axisY: velocityAxis
        }

    }


}