import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: root

    property real wheelRadius: 100
    property real barLength: 280
    property real barThickness: 10
    property real error: 0

    property real angle: 0

    property real padding: 20

    readonly property real maxWidth: 2 * wheelRadius + barLength
    readonly property real maxHeight: 2 * wheelRadius

    width: maxWidth + 2 * padding
    height: maxHeight +  2 * padding

    readonly property real disturbedLength: barLength + error
    readonly property vector2d barStart: barStartPosition(wheelRadius, disturbedLength, angle)
    readonly property vector2d barEnd: barEndPosition(wheelRadius, disturbedLength, angle)
    readonly property vector2d lVec: barEnd.minus(barStart)

	Rectangle {
		id: axis

		anchors.centerIn: parent

		height: 2
		width: root.maxWidth + 2 * padding

		color: "gray"
	}

	Item {
	    id: wheelCenterFrame

	    x: root.padding + root.wheelRadius
        y: root.padding + root.wheelRadius

        Image {
            id: wheel

            x: -root.wheelRadius
            y: -root.wheelRadius

            width: 2 * root.wheelRadius
            height: 2 * root.wheelRadius

            source: "qrc:/wheel.svg"

            transform: Rotation { origin.x: wheel.width / 2; origin.y: wheel.height / 2; angle: root.angle}

            mipmap: true
        }

        Rectangle {
            id: weight

            x: root.barEnd.x - width / 2
            y: root.barEnd.y - height / 2

            width: 4.5 * root.barThickness
            height: 3 * root.barThickness

            color: "lightgray"

            border {
                width: 2
                color: "black"
            }
        }

        Item {
            id: barFrame

            readonly property real angle: root.radToDeg(Math.atan2(root.lVec.y, root.lVec.x))

            transform: [
                Rotation {
                    origin.x: 0
                    origin.y: 0
                    angle: barFrame.angle
                },
                Translate {
                    x: root.barStart.x
                    y: root.barStart.y
                }
            ]

            Rectangle {
                id: bar

                x: -root.barThickness / 2
                y: -root.barThickness / 2

                width: root.disturbedLength + root.barThickness
                height: root.barThickness

                color: "lightgray"

                border {
                    width: 2
                    color: "black"
                }

                antialiasing: true
            }

        }

	}

    function barStartPosition(R, L, a) {
        a = degToRad(a)
        return Qt.vector2d(
            R * Math.cos(a),
            R * Math.sin(a)
        )
    }

    function barEndPosition(R, L, a) {
        a = degToRad(a)
        return Qt.vector2d(
            R * Math.cos(a) + barProjection(R, L, a),
            0
        )
    }

    function barProjection(R, L, a) {
        return Math.sqrt(L * L - R * R * Math.sin(a) * Math.sin(a))
    }

    function degToRad(a) {
        return Math.PI * a / 180.0
    }

    function radToDeg(a) {
        return 180.0 * a / Math.PI
    }

}