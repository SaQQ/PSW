import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GridLayout {
    id: root

    width: 220

    columns: 2

    property real time: 0
    property real angle: 0
    property real step: 0

    property real position: 0
    property real velocity: 0
    property real acceleration: 0

    property real radius: 100
    property real length: 280
    property real angularVelocity: 0
    property real errorDeviation: 0

    readonly property real valueWidth: 80

    Text {
        text: "Time"
        color: "#f5f5f5"
    }

    Text {
        text: root.time.toFixed(3) + " s"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Angle"
        color: "#f5f5f5"
    }

    Text {
        text: root.angle.toFixed(3) + "°"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Step"
        color: "#f5f5f5"
    }

    Text {
        text: root.step.toFixed(3) + " s"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Configuration"
        font.bold: true
        color: "#f5f5f5"
        Layout.columnSpan: 2
    }

    Text {
        text: "Radius"
        color: "#f5f5f5"
    }

    Text {
        text: root.radius.toFixed(3) + " m"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Length"
        color: "#f5f5f5"
    }

    Text {
        text: root.length.toFixed(3) + " m"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Angular Velocity"
        color: "#f5f5f5"
    }

    Text {
        text: root.angularVelocity.toFixed(3) + "°/s"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Error deviation"
        color: "#f5f5f5"
    }

    Text {
        text: root.errorDeviation.toFixed(3) + " m"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Kinematics"
        font.bold: true
        color: "#f5f5f5"
        Layout.columnSpan: 2
    }

    // Position

    Text {
        text: "Position"
        color: "#f5f5f5"
    }

    Text {
        text: root.position.toFixed(3) + " m"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    // Velocity

    Text {
        text: "Velocity"
        color: "#f5f5f5"
    }

    Text {
        text: root.velocity.toFixed(3) + " m/s"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    // Acceleration

    Text {
        text: "Acceleration"
        color: "#f5f5f5"
    }

    Text {
        text: root.acceleration.toFixed(3) + " m/s²"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

}

