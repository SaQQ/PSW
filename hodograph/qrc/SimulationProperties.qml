import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ColumnLayout {
    id: root

    width: 220

    property alias integrationStep: integrationStepSlider.value
    property alias angularVelocity: angularVelocitySlider.value
    property alias radius: radiusSlider.value
    property alias length: lengthSlider.value
    property alias errorDeviation: errorDeviationSlider.value

    // Time step

    property real integrationStepMin: 0.001
    property real integrationStepMax: 1.0
    property real integrationStepDefault: 0.001
    property real integrationStepStep: 0.001

    Text {
        text: qsTr("Integration step") + " [" + root.integrationStep.toFixed(3) + " s]"
        color: "#f5f5f5"
        font.bold: true
    }

    Slider {
        id: integrationStepSlider
        Layout.fillWidth: true
        from: root.integrationStepMin
        to: root.integrationStepMax
        value: root.integrationStepDefault
        stepSize: root.integrationStepStep
    }

    // Radius

    property real radiusMin: 10
    property real radiusMax: 300
    property real radiusDefault: 100
    property real radiusStep: 1

    Text {
        text: qsTr("Radius") + " [" + root.radius.toFixed(3) + " m]"
        color: "#f5f5f5"
        font.bold: true
    }

    Slider {
        id: radiusSlider
        Layout.fillWidth: true
        from: root.radiusMin
        to: root.radiusMax
        value: root.radiusDefault
        stepSize: root.radiusStep
    }

    // Length

    property real lengthMin: 2 * root.radius
    property real lengthMax: 2 * root.radiusMax
    property real lengthDefault: 280
    property real lengthStep: 1

    Text {
        text: qsTr("Length") + " [" + root.length.toFixed(3) + " m]"
        color: "#f5f5f5"
        font.bold: true
    }

    Slider {
        id: lengthSlider
        Layout.fillWidth: true
        from: root.lengthMin
        to: root.lengthMax
        value: root.lengthDefault
        stepSize: root.lengthStep
    }

    // Angular Velocity

    property real angularVelocityMin: 0
    property real angularVelocityMax: 720
    property real angularVelocityDefault: 90
    property real angularVelocityStep: 1

    Text {
        text: qsTr("Angular Velocity") + " [" + root.angularVelocity.toFixed(3) + "°/s]"
        color: "#f5f5f5"
        font.bold: true
    }

    Slider {
        id: angularVelocitySlider
        Layout.fillWidth: true
        from: root.angularVelocityMin
        to: root.angularVelocityMax
        value: root.angularVelocityDefault
        stepSize: root.angularVelocityStep
    }

    // Error deviation

    property real errorDeviationMin: 0
    property real errorDeviationMax: 0.1
    property real errorDeviationDefault: 0
    property real errorDeviationStep: 0.001

    Text {
        text: qsTr("Error Deviation") + " [" + root.errorDeviation.toFixed(3) + " m]"
        color: "#f5f5f5"
        font.bold: true
    }

    Slider {
        id: errorDeviationSlider
        Layout.fillWidth: true
        from: root.errorDeviationMin
        to: root.errorDeviationMax
        value: root.errorDeviationDefault
        stepSize: root.errorDeviationStep
    }

    Item {
        Layout.fillHeight: true
    }

}
