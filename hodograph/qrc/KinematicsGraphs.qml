import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import QtCharts 2.0

ColumnLayout {
    id: root

    width: 400
    height: 600

    spacing: 0

    property real time: 0

    property real windowWidth: windowWidthSlider.value

    property real yMargin: 10.0

    readonly property real minTime: time < 2 * windowWidth / 3 ? 0 : time - 2 * windowWidth / 3
    readonly property real maxTime: time < 2 * windowWidth / 3 ? windowWidth : time + windowWidth / 3

    function reset() {
        time = 0
        positionSeries.clear()
        velocitySeries.clear()
        accelerationSeries.clear()

        positionYAxis.min = -1.0
        positionYAxis.max =  1.0
        velocityYAxis.min = -1.0
        velocityYAxis.max =  1.0
        accelerationYAxis.min = -1.0
        accelerationYAxis.max = 1.0
    }

    function step(t, p, v, a) {
        time = t
        positionSeries.append(t, p)
        velocitySeries.append(t, v)
        accelerationSeries.append(t, a)

        positionYAxis.min = Math.min(positionYAxis.min, p - root.yMargin)
        positionYAxis.max = Math.max(positionYAxis.max, p + root.yMargin)
        velocityYAxis.min = Math.min(velocityYAxis.min, v - root.yMargin)
        velocityYAxis.max = Math.max(velocityYAxis.max, v + root.yMargin)
        accelerationYAxis.min = Math.min(accelerationYAxis.min, a - root.yMargin)
        accelerationYAxis.max = Math.max(accelerationYAxis.max, a + root.yMargin)
    }

    property real margins: 10

    Text {
        Layout.alignment: Qt.AlignHCenter
        text: qsTr("Position")
        color: "white"
        font.bold: true
    }

    ChartView {
        id: positionChart
        Layout.fillWidth: true
        Layout.fillHeight: true
        antialiasing: true
        legend.visible: false
        margins { left: root.margins; right: root.margins; top: root.margins; bottom: root.margins }

        ValueAxis {
            id: positionTimeAxis
            min: root.minTime
            max: root.maxTime
        }

        ValueAxis {
            id: positionYAxis
            min: -1
            max: 1
        }

        LineSeries {
            id: positionSeries
            axisX: positionTimeAxis
            axisY: positionYAxis
            useOpenGL: true
        }

    }

    Text {
        Layout.alignment: Qt.AlignHCenter
        text: qsTr("Velocity")
        color: "white"
        font.bold: true
    }

    ChartView {
        id: velocityChart
        Layout.fillWidth: true
        Layout.fillHeight: true
        antialiasing: true
        legend.visible: false
        margins { left: root.margins; right: root.margins; top: root.margins; bottom: root.margins }

        ValueAxis {
            id: velocityTimeAxis
            min: root.minTime
            max: root.maxTime
        }

        ValueAxis {
            id: velocityYAxis
            min: -1
            max: 1
        }

        LineSeries {
            id: velocitySeries
            axisX: velocityTimeAxis
            axisY: velocityYAxis
            useOpenGL: true
        }

    }

    Text {
        Layout.alignment: Qt.AlignHCenter
        text: qsTr("Acceleration")
        color: "white"
        font.bold: true
    }

    ChartView {
        id: accelerationChart
        Layout.fillWidth: true
        Layout.fillHeight: true
        antialiasing: true
        legend.visible: false
        margins { left: root.margins; right: root.margins; top: root.margins; bottom: root.margins }

        ValueAxis {
            id: accelerationTimeAxis
            min: root.minTime
            max: root.maxTime
        }

        ValueAxis {
            id: accelerationYAxis
            min: -1
            max: 1
        }

        LineSeries {
            id: accelerationSeries
            axisX: accelerationTimeAxis
            axisY: accelerationYAxis
            useOpenGL: true
        }

    }

    RowLayout {

        Text {
            text: qsTr("Window width")
            color: "white"
            font.bold: true
        }

        Slider {
            id: windowWidthSlider
            Layout.fillWidth: true
            from: 5
            to: 60
            value: 15
            stepSize: 0.1
        }

    }

}