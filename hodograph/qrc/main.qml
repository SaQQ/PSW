import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import QtCharts 2.0

import "controls"

import PSW 1.0 as PSW

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: qsTr("PUSN - Hodograph")

    width: 1600
    height: 800

    minimumWidth: 1000
    minimumHeight: 600

    PSW.HodoSimulator {
        id: simulator

        property bool startEnabled: !running
        property bool pauseEnabled: running
        property bool resetEnabled: running

        integrationStep: simulationProperties.integrationStep
        radius: simulationProperties.radius
        length: simulationProperties.length
        angularSpeed: simulationProperties.angularVelocity
        errorDeviation: simulationProperties.errorDeviation

        onStep: {
            kinematicsGraphs.step(simulator.time, simulator.position, simulator.velocity, simulator.acceleration)
            hodograph.step(simulator.time, simulator.position, simulator.velocity)
        }
    }

    header: ToolBar {
        Row {
            anchors.fill: parent
            spacing: 4

            ToolButton {
                text: "Simulation"
                onClicked: programMenu.open()
                Menu {
                    id: programMenu
                    y: parent.height

                    MenuItem {
                        text: "Start"
                        onTriggered: simulator.start()
                        enabled: simulator.startEnabled
                    }

                    MenuItem {
                        text: "Toggle Pause"
                        onTriggered: simulator.togglePause()
                        enabled: simulator.pauseEnabled
                    }

                    MenuItem {
                        text: "Reset"
                        onTriggered: {
                            simulator.reset()
                            kinematicsGraphs.reset()
                            hodograph.reset()
                        }
                        enabled: simulator.resetEnabled
                    }
                }
            }

        }
    }

    footer: ToolBar {
        height: 24
        Row {
            anchors.fill: parent
            anchors.margins: 4

            Label {
                text: simulator.running ? (simulator.paused ? qsTr("Running (Paused)") : qsTr("Running")) : qsTr("Idle")
            }
        }
    }

    Rectangle {
        id: background

        color: "#424242"

        anchors.fill: parent

        Item {
            id: leftPaneContainer

            width: 280

            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }

            ColumnLayout {

                anchors.fill: parent
                anchors.margins: 5

                spacing: 5

                ScrollableSectionPlate {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: 320
                    Layout.minimumHeight: 50

                    header: "Simulation information"

                    SimulationInformation {
                        id: simulationInformation

                        time: simulator.time
                        angle: simulator.angle
                        step: simulator.integrationStep

                        position: simulator.position
                        velocity: simulator.velocity
                        acceleration: simulator.acceleration

                        radius: simulator.radius
                        length: simulator.length
                        angularVelocity: simulator.angularSpeed
                        errorDeviation: simulator.errorDeviation

                    }
                }

                ScrollableSectionPlate {

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.minimumHeight: 50

                    header: "Simulation properties"

                    SimulationProperties {
                        id: simulationProperties
                    }

                }

            }
        }

        Item {
            id: rightPaneContainer

            width: 600

            anchors {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }

            ColumnLayout {

                anchors.fill: parent
                anchors.margins: 5

                spacing: 5

                SectionPlate {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    header: "Kinematics Graphs"

                    KinematicsGraphs {
                        id: kinematicsGraphs
                        anchors.fill: parent
                    }
                }

            }
        }

        Item {
            id: bottomPaneContainer

            height: 350

            anchors {
                left: leftPaneContainer.right
                right: rightPaneContainer.left
                bottom: parent.bottom
            }

            RowLayout {
                anchors.fill: parent
                anchors.topMargin: 5
                anchors.bottomMargin: 5

                spacing: 5

                SectionPlate {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    header: "Hodograph"

                    Hodograph {
                        id: hodograph
                        anchors.fill: parent
                    }

                }
            }
        }

        Item {
            id: centerPaneContainer

            anchors {
                top: parent.top
                bottom: bottomPaneContainer.top
                left: leftPaneContainer.right
                right: rightPaneContainer.left
            }

            SectionPlate {
                header: "Visualization"

                anchors.fill: parent
                anchors.topMargin: 5

                Rectangle {
                    anchors.fill: parent

                    border {
                        width: 1
                        color: simulator.running ? (simulator.paused ? "#3F51B5" : "#4CAF50") : "#212121"
                    }

                    HodoSystem {
                        anchors.centerIn: parent

                        angle: simulator.angle
                        wheelRadius: simulator.radius
                        barLength: simulator.length
                        error: simulator.error
                    }

                }
            }


        }
    }

}
