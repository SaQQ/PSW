#include "CarouselModel.hpp"

#include <cmath>

#include <QVector3D>
#include <QQuaternion>

CarouselModel::CarouselModel(QObject *parent) : QAbstractListModel(parent) {
    carousel = nullptr;
//    d.collisionShape = new btBoxShape(btVector3(getXExtent()/2, getYExtent()/2, getZExtent()/2));
//    d.rigidBody = scene->createRigidBody(1, initialTransform, d.collisionShape);
//    d.motionState = d.rigidBody->getMotionState();
//
//    const btVector3 pivot(0, 0, 0);
//    btVector3 axis( 0.0f, 1.0f, 0.0f );
//    d.hingeConstraint = new btHingeConstraint(*d.rigidBody, pivot, axis);
//    d.hingeConstraint->setLimit(-SIMD_PI * 0.25f, SIMD_PI * 0.25f);
//
//    scene->addConstraint(d.hingeConstraint);
//
//    door.push_back(d);

}


void CarouselModel::CreateCarousel(IPhysicsScene *scene) {
    btTransform carouselPosition(btQuaternion(0, 0, 0, 1), btVector3(0, getCarouselElevation(), 0));
    btCollisionShape* carouselShape = new btCylinderShape(btVector3(getCarouselRadius(), getCarouselThickness() / 2, getCarouselRadius()));

    carousel = scene->createRigidBody(1, carouselPosition, carouselShape);
    carousel->setFriction(0);
    carousel->setSpinningFriction(0);
    carousel->setRollingFriction(0);

    btVector3 carouselPivot(0, 0, 0);
    btVector3 carouselAxis(0, 1, 0);

    carouselConstraint = new btHingeConstraint(*carousel, carouselPivot, carouselAxis);
    carouselConstraint->enableAngularMotor(true, 0, 100);
    scene->addConstraint(carouselConstraint);

    beginInsertRows(QModelIndex(), 0, getChainsCount() * getChainLength() - 1);

    auto da = SIMD_2_PI / getChainsCount();

    for (int chainId = 0; chainId < getChainsCount(); ++chainId) {

        for (int blockId = 0; blockId < getChainLength(); ++blockId) {

            Block block;

            btVector3 carouselWorldPoint(
                    getCarouselRadius() * std::cos(chainId * da),
                    getCarouselElevation() - getCarouselThickness() / 2,
                    getCarouselRadius() * std::sin(chainId * da)
            );

            btVector3 carouselLocalPoint(
                    getCarouselRadius() * std::cos(chainId * da),
                    - getCarouselThickness() / 2,
                    getCarouselRadius() * std::sin(chainId * da)
            );

            btVector3 blockStart(getBlockExtent()/2, getBlockExtent()/2, getBlockExtent()/2);
            btVector3 blockEnd(-getBlockExtent()/2, -getBlockExtent()/2, -getBlockExtent()/2);

            btTransform transform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0));

            if (blockId == 0) {
                transform.setOrigin(carouselWorldPoint - blockStart);
            } else {
                transform.setOrigin(carouselWorldPoint - blockStart - blockId * btVector3(getBlockExtent(), getBlockExtent(), getBlockExtent()));
            }

            block.collisionShape = new btBoxShape(btVector3(getBlockExtent()/2, getBlockExtent()/2, getBlockExtent()/2));
            block.rigidBody = scene->createRigidBody(0.1, transform, block.collisionShape);

            if (blockId == 0) {
               scene->addConstraint(new btPoint2PointConstraint(*carousel, *block.rigidBody, carouselLocalPoint, blockStart));
            } else {
                auto last = blocks.back().rigidBody;
                scene->addConstraint(new btPoint2PointConstraint(*last, *block.rigidBody, blockEnd, blockStart));
            }

            blocks.push_back(block);

        }

    }

    endInsertRows();
}

QHash<int, QByteArray> CarouselModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[PositionRole] = "position";
    roles[OrientationRole] = "orientation";
    return roles;
}

int CarouselModel::rowCount(const QModelIndex &parent) const {
    return static_cast<int>(blocks.size());
}

QVariant CarouselModel::data(const QModelIndex &index, int role) const {

    if (!index.isValid())
        return QVariant();

    auto &block = blocks[index.row()];
    btTransform trans;
    block.rigidBody->getMotionState()->getWorldTransform(trans);

    switch (role) {
        case PositionRole:
        {
            btVector3FloatData data;
            trans.getOrigin().serializeFloat(data);
            return QVariant(QVector3D(data.m_floats[0], data.m_floats[1], data.m_floats[2]));
        }
        case OrientationRole:
        {
            btQuaternionFloatData data;
            trans.getRotation().serializeFloat(data);
            return QVariant(QQuaternion(data.m_floats[3], data.m_floats[0], data.m_floats[1], data.m_floats[2]));
        }
        default:
            return QVariant();
    }

}
//
//void DoorModel::addDoor(IPhysicsScene *scene, const btTransform &initialTransform) {
//    beginInsertRows(QModelIndex(), door.size(), door.size());
//    Door d;
//    d.collisionShape = new btBoxShape(btVector3(getXExtent()/2, getYExtent()/2, getZExtent()/2));
//    d.rigidBody = scene->createRigidBody(1, initialTransform, d.collisionShape);
//    d.motionState = d.rigidBody->getMotionState();
//
//    const btVector3 pivot(0, 0, 0);
//    btVector3 axis( 0.0f, 1.0f, 0.0f );
//    d.hingeConstraint = new btHingeConstraint(*d.rigidBody, pivot, axis);
//    d.hingeConstraint->setLimit(-SIMD_PI * 0.25f, SIMD_PI * 0.25f);
//
//    scene->addConstraint(d.hingeConstraint);
//
//    door.push_back(d);
//    endInsertRows();
//}

void CarouselModel::signalUpdate() {
    dataChanged(this->index(0, 0), this->index(blocks.size() - 1, 0), {PositionRole, OrientationRole});
    emit carouselStateChanged();
}
