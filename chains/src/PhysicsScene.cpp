#include "PhysicsScene.hpp"

#include <QDebug>

PhysicsScene::PhysicsScene(QObject *parent)
        : QObject(parent),
          leftover{0}
{
    spheresModel = new SpheresModel(this);
    doorModel = new DoorModel(this);
    carouselModel = new CarouselModel(this);

    timer = new QTimer{this};
    timer->setInterval(static_cast<int>(16));
    elapsedTimer = new QElapsedTimer;

    QObject::connect(timer, &QTimer::timeout, this, &PhysicsScene::tick);

    broadphase = new btDbvtBroadphase();

    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    solver = new btSequentialImpulseConstraintSolver;

    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver,collisionConfiguration);
    dynamicsWorld->setGravity(btVector3(0, -10, 0));

    btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1.0f);

    auto groundTransform = btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -1, 0));
    
    groundRigidBody = createRigidBody(0, groundTransform, groundShape);


    /* GENERATE DOOR */

    auto doorCount = 30;

    auto da = SIMD_2_PI / 30;

    auto radius = 5 * doorModel->getXExtent();

    for (int i = 0; i < doorCount; ++i) {
        btTransform trans(btQuaternion(0, 0, 0, 1),
                          btVector3(
                                  radius * std::cos(i * da),
                                  doorModel->getYExtent() / 2,
                                  radius * std::sin(i * da)
                          )
        );
        doorModel->addDoor(this, trans);
    }

    /* SETUP SPINNER */

    btTransform trans;
    trans.setIdentity();
    trans.setOrigin(btVector3(getSpinnerExtent() / 2, 4 * getSpinnerExtent() / 2, getSpinnerExtent() / 2));

    //trans.setRotation(btQuaternion(btVector3(1, 0, 0), SIMD_PI / 10) * btQuaternion(btVector3(1.0, 0.0, 0.0), -std::atan(1 / std::sqrt(2))) * btQuaternion(btVector3(0, 0, 1), SIMD_PI / 4));

    btCollisionShape* spinnerShape = new btBoxShape(btVector3(getSpinnerExtent() / 2, getSpinnerExtent() / 2, getSpinnerExtent() / 2));
    spinner = createRigidBody(1, trans, spinnerShape);

    //spinner->setAngularVelocity(btVector3(0,20,0));
    spinner->setAngularVelocity(btVector3(15, 15, 15));
    spinner->setFriction(0);
    spinner->setSpinningFriction(0);
    spinner->setRollingFriction(0);

    btVector3 pivotInA(-getSpinnerExtent() / 2, -getSpinnerExtent() / 2, -getSpinnerExtent() / 2);
    //btVector3 pivotInA(0, 0, 0);

    btTypedConstraint* p2p = new btPoint2PointConstraint(*spinner, pivotInA);
    this->addConstraint(p2p);

    carouselModel->CreateCarousel(this);

    timer->start();
}

void PhysicsScene::tick() {

    auto elapsedMsec = elapsedTimer->elapsed();

    btScalar  dt = elapsedMsec / 1000.0f;

//    qDebug() << dt << " " << 1/60.f;

    dynamicsWorld->stepSimulation(1/60.f, 10);

    spheresModel->signalUpdate();
    doorModel->signalUpdate();
    carouselModel->signalUpdate();

    elapsedTimer->restart();

    emit spinnerStateChanged();
}

PhysicsScene::~PhysicsScene() {
    dynamicsWorld->removeRigidBody(groundRigidBody);
    delete groundRigidBody->getMotionState();
    delete groundRigidBody;

    delete dynamicsWorld;
    delete solver;
    delete dispatcher;
    delete collisionConfiguration;
    delete broadphase;
}


btRigidBody *PhysicsScene::createRigidBody(float mass, const btTransform &startTransform, btCollisionShape *shape) {
	btAssert((!shape || shape->getShapeType() != INVALID_SHAPE_PROXYTYPE));

	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		shape->calculateLocalInertia(mass, localInertia);

	btDefaultMotionState *myMotionState = new btDefaultMotionState(startTransform);

	btRigidBody::btRigidBodyConstructionInfo cInfo(mass, myMotionState, shape, localInertia);

	btRigidBody *body = new btRigidBody(cInfo);

	dynamicsWorld->addRigidBody(body);

	return body;
}	


void PhysicsScene::addConstraint(btTypedConstraint *constraint) {
    dynamicsWorld->addConstraint(constraint);
}

void PhysicsScene::fireSphere(const QVector3D &pos, const QVector3D &v) {
    spheresModel->addSphere(this, btVector3(pos.x(), pos.y(), pos.z()), btVector3(v.x(), v.y(), v.z()));
}
