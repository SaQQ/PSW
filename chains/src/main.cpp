// Created by Paweł Sobótka on 18.08.17.
// Rail-Mil Computers Sp. z o.o.

#include <QApplication>
#include <QQmlApplicationEngine>

#include "PhysicsScene.hpp"
#include "QmlRaycast.hpp"

int main(int argc, char** argv) {

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<SpheresModel>("PSW", 1, 0, "SpheresModel");
    qmlRegisterType<DoorModel>("PSW", 1, 0, "DoorModel");
    qmlRegisterType<CarouselModel>("PSW", 1, 0, "CarouselModel");
    qmlRegisterType<PhysicsScene>("PSW", 1, 0, "PhysicsScene");
    qmlRegisterType<QmlRaycast>("PSW", 1, 0, "Raycast");

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
