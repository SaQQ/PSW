#include "SpheresModel.hpp"

#include <QVector3D>

SpheresModel::SpheresModel(QObject *parent) : QAbstractListModel(parent) {

}

QHash<int, QByteArray> SpheresModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[PositionRole] = "position";
    return roles;
}

int SpheresModel::rowCount(const QModelIndex &parent) const {
    return static_cast<int>(spheres.size());
}

QVariant SpheresModel::data(const QModelIndex &index, int role) const {

    if (!index.isValid())
        return QVariant();

    auto &sphere = spheres[index.row()];

    switch (role) {
        case PositionRole:
        {
            btTransform trans;
            sphere.rigidBody->getMotionState()->getWorldTransform(trans);
            return QVariant(QVector3D(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
        }
        default:
            return QVariant();
    }

}

void SpheresModel::addSphere(IPhysicsScene *scene) {
    addSphere(scene, btVector3(0, 30, 0), btVector3(0, 0, 0));
}


void SpheresModel::addSphere(IPhysicsScene *scene, const btVector3 &initialPosition, const btVector3 &initialVelocity) {
    beginInsertRows(QModelIndex(), spheres.size(), spheres.size());
    Sphere s;
    s.collisionShape = new btSphereShape(getRadius());
    auto startTransform = btTransform(btQuaternion(0, 0, 0, 1), initialPosition);
    s.rigidBody = scene->createRigidBody(1, startTransform, s.collisionShape); 
    s.motionState = s.rigidBody->getMotionState();
    s.rigidBody->setLinearVelocity(initialVelocity);
    spheres.push_back(s);
    endInsertRows();
}

void SpheresModel::signalUpdate() {
    dataChanged(this->index(0, 0), this->index(spheres.size() - 1, 0), {PositionRole});
}
