#ifndef CHAINS_SPHERES_MODEL_HPP
#define CHAINS_SPHERES_MODEL_HPP

#include <QAbstractListModel>

#include <bullet/btBulletDynamicsCommon.h>

#include "IPhysicsScene.hpp"

struct Sphere {
    btCollisionShape* collisionShape;
    btMotionState* motionState;
    btRigidBody* rigidBody;
};

class SpheresModel : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(float radius READ getRadius CONSTANT)

    Q_ENUMS(Roles)
public:
    enum Roles {
        PositionRole = Qt::UserRole + 1
    };

    explicit SpheresModel(QObject *parent = nullptr);

    float getRadius() {
        return 10.0f;
    };

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    void addSphere(IPhysicsScene *scene);

    void addSphere(IPhysicsScene *scene, const btVector3 &initialPosition, const btVector3 &initialVelocity);

    void signalUpdate();

private:
    std::vector<Sphere> spheres;
};

#endif
