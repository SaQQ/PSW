#ifndef CHAINS_PHYSICS_SCENE_HPP
#define CHAINS_PHYSICS_SCENE_HPP

#include <QObject>
#include <QElapsedTimer>
#include <QTimer>
#include <QVector3D>
#include <QQuaternion>

#include "IPhysicsScene.hpp"
#include "SpheresModel.hpp"
#include "DoorModel.hpp"
#include "CarouselModel.hpp"

#include <bullet/btBulletDynamicsCommon.h>

class PhysicsScene : public QObject, public IPhysicsScene {
	Q_OBJECT

    Q_PROPERTY(SpheresModel* spheresModel READ getSpheresModel CONSTANT)
    Q_PROPERTY(DoorModel* doorModel READ getDoorModel CONSTANT)
    Q_PROPERTY(CarouselModel *carouselModel READ getCarouselModel CONSTANT)

    Q_PROPERTY(float spinnerExtent READ getSpinnerExtent CONSTANT)
	Q_PROPERTY(QVector3D spinnerPosition READ getSpinnerPosition NOTIFY spinnerStateChanged)
	Q_PROPERTY(QQuaternion spinnerOrientation READ getSpinnerOrientation NOTIFY spinnerStateChanged)
public:
	explicit PhysicsScene(QObject *parent = nullptr);

	~PhysicsScene() override;

    SpheresModel *getSpheresModel() const { return spheresModel; }

    DoorModel *getDoorModel() const { return doorModel; }

    CarouselModel *getCarouselModel() const { return carouselModel; }

    Q_INVOKABLE void generateSphere() {
        spheresModel->addSphere(this);
    }

    Q_INVOKABLE void fireSphere(const QVector3D &initialPosition, const QVector3D &initialVelocity);

    btRigidBody *createRigidBody(float mass, const btTransform &startTransform, btCollisionShape *shape) override;
    
    void addConstraint(btTypedConstraint *constraint) override;

    float getSpinnerExtent() const { return 30.0f; }

	QVector3D getSpinnerPosition() const {
        btTransform transform;
        spinner->getMotionState()->getWorldTransform(transform);
        btVector3FloatData data;
        transform.getOrigin().serializeFloat(data);
        return QVector3D(data.m_floats[0], data.m_floats[1], data.m_floats[2]);
    }

    QQuaternion getSpinnerOrientation() const {
        btTransform transform;
        spinner->getMotionState()->getWorldTransform(transform);
        btQuaternionFloatData data;
        transform.getRotation().serializeFloat(data);
        return QQuaternion(data.m_floats[3], data.m_floats[0], data.m_floats[1], data.m_floats[2]);
    }

signals:
    void spinnerStateChanged();

private slots:
    void tick();
private:

    QTimer* timer;
    QElapsedTimer* elapsedTimer;
    qint64 leftover;

	btBroadphaseInterface * broadphase;
	btDefaultCollisionConfiguration * collisionConfiguration;
	btCollisionDispatcher * dispatcher;
	btSequentialImpulseConstraintSolver * solver;
	btDiscreteDynamicsWorld * dynamicsWorld;

    btRigidBody* groundRigidBody;
    btRigidBody* spinner;

    SpheresModel *spheresModel;
    DoorModel *doorModel;
    CarouselModel *carouselModel;

};

#endif
