#ifndef CHAINS_DOORS_MODEL_HPP
#define CHAINS_DOORS_MODEL_HPP

#include <QAbstractListModel>

#include "IPhysicsScene.hpp"

struct Door {
    btCollisionShape* collisionShape;
    btMotionState* motionState;
    btRigidBody* rigidBody;
    btHingeConstraint *hingeConstraint;
};

class DoorModel : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(float xExtent READ getXExtent CONSTANT)
    Q_PROPERTY(float yExtent READ getYExtent CONSTANT)
    Q_PROPERTY(float zExtent READ getZExtent CONSTANT)

    Q_ENUMS(Roles)
public:
    enum Roles {
        PositionRole = Qt::UserRole + 1,
	OrientationRole
    };

    explicit DoorModel(QObject *parent = nullptr);

    float getXExtent() { return 20.0f; }
    float getYExtent() { return 50.0f; }
    float getZExtent() { return 2.0f; }

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;

//    void addSphere(IPhysicsScene *scene);
    
    void addDoor(IPhysicsScene *scene, const btTransform &initialTransform);

    void signalUpdate();

private:
    std::vector<Door> door;
};

#endif
