#ifndef CHAINS_CAROUSEL_MODEL_HPP
#define CHAINS_CAROUSEL_MODEL_HPP

#include <QAbstractListModel>
#include <QVector3D>
#include <QQuaternion>

#include "IPhysicsScene.hpp"

struct Block {
    btCollisionShape* collisionShape;
    btRigidBody* rigidBody;
    btPoint2PointConstraint *lowerConstraint, *upperConstraint;
};

class CarouselModel : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(float blockExtent READ getBlockExtent CONSTANT)

    Q_PROPERTY(int chainsCount READ getChainsCount CONSTANT)

    Q_PROPERTY(int chainLength READ getChainLength CONSTANT)

    Q_PROPERTY(float carouselThickness READ getCarouselThickness CONSTANT)

    Q_PROPERTY(float carouselRadius READ getCarouselRadius CONSTANT)

    Q_PROPERTY(float carouselElevation READ getCarouselElevation CONSTANT)

    Q_PROPERTY(QVector3D carouselPosition READ getCarouselPosition NOTIFY carouselStateChanged)

    Q_PROPERTY(QQuaternion carouselOrientation READ getCarouselOrientation NOTIFY carouselStateChanged)

    Q_PROPERTY(float carouselVelocity READ getCarouselVelocity WRITE setCarouselVelocity NOTIFY carouselVelocityChanged)

    Q_ENUMS(Roles)
public:
    enum Roles {
        PositionRole = Qt::UserRole + 1,
	    OrientationRole
    };

    explicit CarouselModel(QObject *parent = nullptr);

    void CreateCarousel(IPhysicsScene *scene);

    float getBlockExtent() const { return 8.0f; }

    int getChainsCount() const { return 10; }

    int getChainLength() const { return 5; }

    float getCarouselThickness() const { return 5.0f; }

    float getCarouselRadius() const { return 50.0f; }

    float getCarouselElevation() const { return 150.0f; }

    QVector3D getCarouselPosition() const {
        if (carousel == nullptr)
            return QVector3D(0, 0, 0);
        btTransform transform;
        carousel->getMotionState()->getWorldTransform(transform);
        btVector3FloatData data;
        transform.getOrigin().serializeFloat(data);
        return QVector3D(data.m_floats[0], data.m_floats[1], data.m_floats[2]);
    }

    QQuaternion getCarouselOrientation() const {
        if (carousel == nullptr)
            return QQuaternion(1, 0, 0, 0);
        btTransform transform;
        carousel->getMotionState()->getWorldTransform(transform);
        btQuaternionFloatData data;
        transform.getRotation().serializeFloat(data);
        return QQuaternion(data.m_floats[3], data.m_floats[0], data.m_floats[1], data.m_floats[2]);
    }

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    void signalUpdate();

    float getCarouselVelocity() const {
        if (carouselConstraint == nullptr)
            return 0;
        return carouselConstraint->getMotorTargetVelocity();
    }

    void setCarouselVelocity(float value) {
        if (carouselConstraint == nullptr)
            return;
        carouselConstraint->setMotorTargetVelocity(value);
        emit carouselVelocityChanged();
    }

signals:
    void carouselStateChanged();
    void carouselVelocityChanged();
private:
    std::vector<Block> blocks;

    btRigidBody *carousel;
    btHingeConstraint* carouselConstraint;
};

#endif
