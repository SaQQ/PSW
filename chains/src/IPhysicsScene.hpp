#ifndef CHAINS_IPHYSICS_SCENE_HPP
#define CHAINS_IPHYSICS_SCENE_HPP

#include <bullet/btBulletDynamicsCommon.h>

class IPhysicsScene {
public:

    virtual ~IPhysicsScene() = default;

    virtual btRigidBody *createRigidBody(float mass, const btTransform &startTransform, btCollisionShape *shape) = 0;

    virtual void addConstraint(btTypedConstraint *constraint) = 0;
};

#endif
