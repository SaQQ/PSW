#include "DoorModel.hpp"

#include <QVector3D>
#include <QQuaternion>
#include <QDebug>

DoorModel::DoorModel(QObject *parent) : QAbstractListModel(parent) {

}


QHash<int, QByteArray> DoorModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[PositionRole] = "position";
    roles[OrientationRole] = "orientation";
    return roles;
}


int DoorModel::rowCount(const QModelIndex &parent) const {
    return static_cast<int>(door.size());
}

QVariant DoorModel::data(const QModelIndex &index, int role) const {

    if (!index.isValid())
        return QVariant();

    auto &d = door[index.row()];
    btTransform trans;
    d.rigidBody->getMotionState()->getWorldTransform(trans);

    switch (role) {
        case PositionRole:
        {
            btVector3FloatData data;
            trans.getOrigin().serializeFloat(data);
            return QVariant(QVector3D(data.m_floats[0], data.m_floats[1], data.m_floats[2]));
        }
	case OrientationRole:
        {
            btQuaternionFloatData data;
            trans.getRotation().serializeFloat(data);
            return QVariant(QQuaternion(data.m_floats[3], data.m_floats[0], data.m_floats[1], data.m_floats[2]));
        }
        default:
            return QVariant();
    }

}

void DoorModel::addDoor(IPhysicsScene *scene, const btTransform &initialTransform) {
    beginInsertRows(QModelIndex(), door.size(), door.size());
    Door d;
    d.collisionShape = new btBoxShape(btVector3(getXExtent()/2, getYExtent()/2, getZExtent()/2));
    d.rigidBody = scene->createRigidBody(1, initialTransform, d.collisionShape);
    d.motionState = d.rigidBody->getMotionState();

    const btVector3 pivot(0, 0, 0);
    btVector3 axis( 0.0f, 1.0f, 0.0f );
    d.hingeConstraint = new btHingeConstraint(*d.rigidBody, pivot, axis);
    d.hingeConstraint->setLimit(-SIMD_PI * 0.25f, SIMD_PI * 0.25f);
    
    scene->addConstraint(d.hingeConstraint);

    door.push_back(d);
    endInsertRows();
}

void DoorModel::signalUpdate() {
    dataChanged(this->index(0, 0), this->index(door.size() - 1, 0), {PositionRole, OrientationRole});
}
