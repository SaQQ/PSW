import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import QtCharts 2.0

import "controls"

import PSW 1.0

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: "PŚW - Spring"

    width: 1200
    height: 800

    minimumWidth: 1000
    minimumHeight: 600

    Rectangle {
        id: background

        color: "#424242"

        anchors.fill: parent

        PhysicsScene {
            id: physicsScene
        }

        Item {
            id: bottomPaneContainer

            height: 80

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            SectionPlate {
                anchors.fill: parent
                anchors.margins: 5
                anchors.topMargin: 0
                header: "Random Buttons"

                RowLayout {
                    anchors.fill: parent
                    Button {
                        id: generateSphereButton
                        Layout.fillHeight: true
                        text: qsTr("Generate Sphere")
                        onClicked: physicsScene.generateSphere()
                    }
                    Slider {
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        id: angularVelocitySlider
                        from: 0
                        to: 10
                        stepSize: 0.01
                        value: 0
                        onValueChanged: {
                            physicsScene.carouselModel.carouselVelocity = value
                        }
                    }
                }
            }

        }

        Item {
            id: centerPaneContainer

            anchors {
                top: parent.top
                bottom: bottomPaneContainer.top
                left: parent.left
                right: parent.right
            }

            SectionPlate {
                header: "Visualization"

                anchors.fill: parent
                anchors.margins: 5
                anchors.leftMargin: 0

                Rectangle {
                    anchors.fill: parent

                    border {
                        width: 1
                        color: "#212121"
                    }

                    Scene3D {
                        id: scene3d
                        anchors.fill: parent
                        anchors.margins: 1
                        focus: true
                        aspects: ["input", "logic", "render"]
                        cameraAspectRatioMode: Scene3D.UserAspectRatio

                        Entity {
                            id: sceneRoot
                            objectName: "sceneRoot"

                            Camera {
                                id: camera
                                projectionType: CameraLens.PerspectiveProjection
                                aspectRatio: scene3d.width / scene3d.height
                                fieldOfView: 30
                                nearPlane : 0.1
                                farPlane : 1000.0
                                position: Qt.vector3d( 250.0, 300.0, 250.0 )
                                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                                viewCenter: Qt.vector3d( 0.0, 50.0, 0.0 )
                            }

                            Raycast {
                                id: raycast
                                viewMatrix: camera.viewMatrix
                                projectionMatrix: camera.projectionMatrix
                                viewportSize: Qt.size(scene3d.width, scene3d.height)
                                screenPosition: Qt.vector2d(scene3d.width / 2, scene3d.height / 2)
                                distance: 20.0
                            }

                            KeyboardDevice { id: keyboardSourceDevice }

                            MouseDevice { id: mouseDevice }

                            KeyboardHandler {
                                id: keyboardHandler
                                focus: true
                                sourceDevice: keyboardSourceDevice
                                
                                onPressed: {
                                    if (event.key === Qt.Key_Space) {
                                        event.accepted = true;
                                        physicsScene.fireSphere(raycast.worldPosition, raycast.worldPosition.minus(camera.position).times(20))
                                    }
                                }
                            }


                            MouseHandler {
                                id: mouseHandler
                                sourceDevice: mouseDevice

                                onPressed: {
                                    scene3d.focus = true
                                }
                            }

                            OrbitCameraController {
                                camera: camera
                                linearSpeed: 200.0
                            }

                            components: [
                                RenderSettings {
                                    activeFrameGraph: ForwardRenderer {
                                        camera: camera
                                        clearColor: "gray"
                                    }
                                },
                                InputSettings { },
                                DirectionalLight {
                                    worldDirection: Qt.vector3d(1, 1, 1)
                                },
                                DirectionalLight {
                                    worldDirection: Qt.vector3d(-1, -1, -1)
                                }
                            ]

                            PhongMaterial {
                                id: metalMaterial
                                ambient: "#BDBDBD"
                                diffuse: "#BDBDBD"
                                specular: "#FAFAFA"
                            }

                            CuboidMesh {
                                id: groundMesh
                                xExtent: 1000.0
                                yExtent: 0.1
                                zExtent: 1000.0
                            }

                            PhongMaterial {
                                id: groundMaterial
                                ambient: "green"
                            }

                            Entity {
                                id: ground
                                components: [groundMesh, groundMaterial]
                            }

                            CuboidMesh {
                                id: spinnerMesh
                                xExtent: physicsScene.spinnerExtent
                                yExtent: physicsScene.spinnerExtent
                                zExtent: physicsScene.spinnerExtent
                            }

                            Entity {
                                id: spinner
                                Transform {
                                    id: spinnerTransform
                                    translation: physicsScene.spinnerPosition
                                    rotation: physicsScene.spinnerOrientation
                                }
                                components: [spinnerMesh, metalMaterial, spinnerTransform]
                            }

                            SphereMesh {
                                id: sphereMesh
                                radius: physicsScene.spheresModel.radius
                            }

                            NodeInstantiator {
                                model: physicsScene.spheresModel
                                delegate: Entity {
                                    Transform {
                                        id: sphereTransform
                                        translation: model.position
                                    }
                                    components: [sphereMesh, sphereTransform, metalMaterial]
                                }
                            }

                            CuboidMesh {
                                id: doorMesh
                                xExtent: physicsScene.doorModel.xExtent
                                yExtent: physicsScene.doorModel.yExtent
                                zExtent: physicsScene.doorModel.zExtent
                            }

                            PhongMaterial {
                                id: doorMaterial
                                diffuse: "lightblue"
                            }

                            NodeInstantiator {
                                model: physicsScene.doorModel
                                delegate: Entity {
                                    Transform {
                                        id: doorTransform
                                        translation: model.position
                                        rotation: model.orientation
                                    }
                                    components: [doorMesh, doorTransform, doorMaterial]
                                }
                            }

                            PhongMaterial {
                                id: blockMaterial
                                diffuse: "lightgray"
                            }

                            CuboidMesh {
                                id: blockMesh
                                xExtent: physicsScene.carouselModel.blockExtent
                                yExtent: physicsScene.carouselModel.blockExtent
                                zExtent: physicsScene.carouselModel.blockExtent
                            }

                            NodeInstantiator {
                                model: physicsScene.carouselModel
                                delegate: Entity {
                                    Transform {
                                        id: blockTransform
                                        translation: model.position
                                        rotation: model.orientation
                                    }
                                    components: [blockMesh, blockTransform, blockMaterial]
                                }
                            }

                            CylinderMesh {
                                id: carouselMesh
                                radius: physicsScene.carouselModel.carouselRadius
                                length: physicsScene.carouselModel.carouselThickness
                            }

                            Entity {
                                id: carousel
                                Transform {
                                    id: carouselTransform
                                    translation: physicsScene.carouselModel.carouselPosition
                                    rotation: physicsScene.carouselModel.carouselOrientation
                                }
                                components: [carouselMesh, metalMaterial, carouselTransform]
                            }

                        }

                    }

                }
            }


        }
    }

}
