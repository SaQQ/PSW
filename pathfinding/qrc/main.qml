import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import "controls"

import PSW 1.0 as PSW

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: "Configuration space pathfinder"

    width: 1200
    height: 800

    PSW.PathfindingSimulator {
        id: simulator
    }

    PSW.SceneModel {
        id: sceneModel
        l1: sceneSettings.l1
        l2: sceneSettings.l2
    }

    PSW.RevTaskResult {
        id: startRevTaskRes

        property real chosenA1: 0
        property real chosenA2: 0
    }

    PSW.RevTaskResult {
        id: endRevTaskRes

        property real chosenA1: 0
        property real chosenA2: 0
    }

    footer: ToolBar {
        height: 24
        Row {
            anchors.fill: parent
            anchors.margins: 4

            Label {
                text: visualization.state
            }
        }
    }

    Rectangle {
        id: background

        color: "#424242"

        anchors.fill: parent

        Item {
            id: rightPaneContainer

            anchors {
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }

            width: 380

            ColumnLayout {
                spacing: 5

                anchors.fill: parent
                anchors.margins: 5

                SectionPlate {
                    Layout.fillWidth: true
                    height: 200

                    header: "Settings"

                    SceneSettings {
                        id: sceneSettings
                        anchors.fill: parent
                        enabled: visualization.state === "unconfigured"
                        obstaclesCount: sceneModel.rectanglesCount
                        onClearRectangles: sceneModel.clearRectangles()
                        onSimulate: {
                            simulator.configure(sceneModel, cSpaceImagesProvider)
                            visualization.state = "configured"
                        }
                    }

                }

                SectionPlate {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    header: "Simulation"

                    SimulationControl {
                        id: simulationControl
                        anchors.fill: parent
                        enabled: visualization.state !== "unconfigured"
                        onClose: {
                            visualization.state = "unconfigured"
                            simulator.unconfigure(cSpaceImagesProvider)
                        }
                    }

                }

            }

        }

        Item {
            id: centerPaneContainer

            anchors {
                top: parent.top
                bottom: parent.bottom
                left: parent.left
                right: rightPaneContainer.left
            }

            SectionPlate {
                id: visualization
                anchors.fill: parent
                anchors.margins: 5

                header: "Visualization"

                state: "unconfigured"

                states: [
                    State {
                        name: "unconfigured"
                        PropertyChanges { target: mainArm; visible: true; a1: 0; a2: 0 }
                    },
                    State {
                        name: "configured"
                        PropertyChanges { target: backgroundRect; border.color: "green" }
                    },
                    State {
                        name: "resolving_start"
                        PropertyChanges { target: backgroundRect; border.color: "blue" }
                        PropertyChanges { target: optionAArm; a1: startRevTaskRes.sol1a1; a2: startRevTaskRes.sol1a2; visible: true; }
                        PropertyChanges { target: optionBArm; a1: startRevTaskRes.sol2a1; a2: startRevTaskRes.sol2a2; visible: true; }
                    },
                    State {
                        name: "start_set"
                        PropertyChanges { target: backgroundRect; border.color: "green" }
                        PropertyChanges { target: startArm; visible: true; }
                    },
                    State {
                        name: "resolving_end"
                        PropertyChanges { target: backgroundRect; border.color: "blue" }
                        PropertyChanges { target: optionAArm; a1: endRevTaskRes.sol1a1; a2: endRevTaskRes.sol1a2; visible: true; }
                        PropertyChanges { target: optionBArm; a1: endRevTaskRes.sol2a1; a2: endRevTaskRes.sol2a2; visible: true; }
                        PropertyChanges { target: startArm; visible: true; }
                    },
                    State {
                        name: "simulating"
                        PropertyChanges { target: backgroundRect; border.color: "red" }
                        PropertyChanges { target: mainArm; visible: true }
                        PropertyChanges { target: startArm; visible: true; }
                        PropertyChanges { target: endArm; visible: true; }
                        StateChangeScript { script: simulator.simulate(startRevTaskRes.chosenA1, startRevTaskRes.chosenA2, endRevTaskRes.chosenA1, endRevTaskRes.chosenA2, cSpaceImagesProvider) }
                    }
                ]

                Rectangle {
                    id: backgroundRect

                    anchors.fill: parent

                    color: sceneSettings.highlightReachable ? "lightgray" : "white"

                    border {
                        width: 2
                    }

                    Behavior on border.color {
                        ColorAnimation { duration: 100 }
                    }

                    MouseArea {
                        id: mouseArea

                        anchors.fill: parent

                        cursorShape: Qt.CrossCursor
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        hoverEnabled: true

                        onPressed: {
                            if (mouse.button === Qt.LeftButton) {
                                unreachableToast.hide()
                                unreachableToast.x = mouse.x + 5
                                unreachableToast.y = mouse.y + 5

                                var point = mouseArea.mapToItem(sceneRoot, mouse.x, mouse.y)
                                if (visualization.state === "unconfigured") {
                                    sceneModel.startNewRectangle(point.x, point.y)
                                } else if (visualization.state === "configured" || visualization.state === "simulating") {
                                    sceneModel.solveRevTask(point.x, point.y, startRevTaskRes)

                                    if (startRevTaskRes.valid === false) {
                                        unreachableToast.show()
                                        return
                                    }

                                    simulator.stop(cSpaceImagesProvider)

                                    var s1r = !simulator.isColliding(startRevTaskRes.sol1a1, startRevTaskRes.sol1a2)
                                    var s2r = !simulator.isColliding(startRevTaskRes.sol2a1, startRevTaskRes.sol2a2)

                                    if (!(s1r || s2r)) {
                                        /* Both collide */
                                        unreachableToast.show()
                                    } else if (s1r && s2r) {
                                        /* Both ok */
                                        visualization.state = "resolving_start"
                                    } else if (s2r) {
                                        /* s2 ok */
                                        startRevTaskRes.chosenA1 = startRevTaskRes.sol2a1
                                        startRevTaskRes.chosenA2 = startRevTaskRes.sol2a2
                                        visualization.state = "start_set"
                                    } else {
                                        /* s1 ok */
                                        startRevTaskRes.chosenA1 = startRevTaskRes.sol1a1
                                        startRevTaskRes.chosenA2 = startRevTaskRes.sol1a2
                                        visualization.state = "start_set"
                                    }
                                } else if (visualization.state === "resolving_start") {
                                    /* Do nothing */
                                } else if (visualization.state === "start_set") {
                                    sceneModel.solveRevTask(point.x, point.y, endRevTaskRes)

                                    if (endRevTaskRes.valid === false) {
                                        unreachableToast.show()
                                        return
                                    }

                                    var s1r = simulator.isReachable(startRevTaskRes.chosenA1, startRevTaskRes.chosenA2, endRevTaskRes.sol1a1, endRevTaskRes.sol1a2)
                                    var s2r = simulator.isReachable(startRevTaskRes.chosenA1, startRevTaskRes.chosenA2, endRevTaskRes.sol2a1, endRevTaskRes.sol2a2)

                                    if (!(s1r || s2r)) {
                                        /* Both collide */
                                        unreachableToast.show()
                                    } else if (s1r && s2r) {
                                        /* Both ok */
                                        visualization.state = "resolving_end"
                                    } else if (s2r) {
                                        /* s2 ok */
                                        endRevTaskRes.chosenA1 = endRevTaskRes.sol2a1
                                        endRevTaskRes.chosenA2 = endRevTaskRes.sol2a2
                                        visualization.state = "simulating"
                                    } else {
                                        /* s1 ok */
                                        endRevTaskRes.chosenA1 = endRevTaskRes.sol1a1
                                        endRevTaskRes.chosenA2 = endRevTaskRes.sol1a2
                                        visualization.state = "simulating"
                                    }

                                } else if (visualization.state === "resolving_end") {
                                    /* Do nothing */
                                }
                            }
                        }

                        onReleased: {
                            var point = mouseArea.mapToItem(sceneRoot, mouse.x, mouse.y)
                            if (mouse.button === Qt.LeftButton) {
                                if (visualization.state === "unconfigured") {
                                    sceneModel.finishNewRectangle(point.x, point.y);
                                }
                            }
                        }

                        onPositionChanged: {
                            var point = mouseArea.mapToItem(sceneRoot, mouse.x, mouse.y)
                            if (mouseArea.pressedButtons & Qt.LeftButton) {
                                if (visualization.state === "unconfigured") {
                                    sceneModel.newRectangleDrag(point.x, point.y);
                                }
                            }
                        }

                        Item {
                            id: sceneRoot

                            property real toolWidth: 10

                            x: parent.width / 2
                            y: parent.height / 2

                            Rectangle {
                                color: "white"
                                readonly property real r: (sceneModel.l1 + sceneModel.l2)
                                radius: r
                                x: -r
                                y: -r
                                width: 2 * r
                                height: 2 * r
                                visible: sceneSettings.highlightReachable
                            }

                            Rectangle {
                                color: "lightgray"
                                readonly property real r: Math.abs(sceneModel.l1 - sceneModel.l2)
                                radius: r
                                x: -r
                                y: -r
                                width: 2 * r
                                height: 2 * r
                                visible: sceneSettings.highlightReachable
                            }

                            Repeater {
                                model: sceneModel
                                delegate: Rectangle {
                                    id: obstacleRoot

                                    states: [
                                        State {
                                            name: "editable"
                                        },
                                        State {
                                            name: "dragged"
                                            when: obstacleArea.drag.active
                                            PropertyChanges { target: obstacleRoot; color: "red" }
                                        },
                                        State {
                                            name: "fixed"
                                            PropertyChanges { target: obstacleArea; enabled: false }
                                        }
                                    ]

                                    state: visualization.state === "unconfigured" ? "editable" : "fixed"

                                    x: model.x
                                    y: model.y
                                    width: model.width
                                    height: model.height
                                    color: obstacleArea.containsMouse ? Qt.lighter("lightgray", 1.1) : "lightgray"

                                    border {
                                        width: 1
                                        color: "#9E9E9E"
                                    }

                                    MouseArea {
                                        id: obstacleArea
                                        anchors.fill: parent
                                        hoverEnabled: true
                                        enabled: true
                                        onClicked: {
                                            sceneModel.removeRow(index)
                                        }
                                    }
                                }
                            }

                            Arm {
                                id: mainArm
                                l1: sceneModel.l1
                                l2: sceneModel.l2
                                a1: simulator.alpha1
                                a2: simulator.alpha2
                                color: "#F44336"
                                visible: false
                            }

                            Arm {
                                id: startArm
                                l1: sceneModel.l1
                                l2: sceneModel.l2
                                a1: startRevTaskRes.chosenA1
                                a2: startRevTaskRes.chosenA2
                                color: "#3F51B5"
                                opacity: 0.5
                                visible: false
                            }

                            Arm {
                                id: endArm
                                l1: sceneModel.l1
                                l2: sceneModel.l2
                                a1: endRevTaskRes.chosenA1
                                a2: endRevTaskRes.chosenA2
                                color: "#3F51B5"
                                opacity: 0.5
                                visible: false
                            }

                            Arm {
                                id: optionAArm
                                l1: sceneModel.l1
                                l2: sceneModel.l2
                                a1: 0
                                a2: 0
                                visible: false
                                opacity: 0.5
                                color: "#9C27B0"
                                midAreaVisible: true
                                onMidClicked: {
                                    if (visualization.state === "resolving_start") {
                                        startRevTaskRes.chosenA1 = startRevTaskRes.sol1a1
                                        startRevTaskRes.chosenA2 = startRevTaskRes.sol1a2
                                        visualization.state = "start_set"
                                    } else if (visualization.state === "resolving_end") {
                                        endRevTaskRes.chosenA1 = endRevTaskRes.sol1a1
                                        endRevTaskRes.chosenA2 = endRevTaskRes.sol1a2
                                        visualization.state = "simulating"
                                    }
                                }
                            }

                            Arm {
                                id: optionBArm
                                l1: sceneModel.l1
                                l2: sceneModel.l2
                                a1: 0
                                a2: 0
                                visible: false
                                opacity: 0.5
                                midAreaVisible: true
                                color: "#9C27B0"
                                onMidClicked: {
                                    if (visualization.state === "resolving_start") {
                                        startRevTaskRes.chosenA1 = startRevTaskRes.sol2a1
                                        startRevTaskRes.chosenA2 = startRevTaskRes.sol2a2
                                        visualization.state = "start_set"
                                    } else if (visualization.state === "resolving_end") {
                                        endRevTaskRes.chosenA1 = endRevTaskRes.sol2a1
                                        endRevTaskRes.chosenA2 = endRevTaskRes.sol2a2
                                        visualization.state = "simulating"
                                    }
                                }
                            }

                        }

                        Toast {
                            id: unreachableToast
                            color: "#FBE9E7"
                            border.color: "#BF360C"
                            Text {
                                anchors.fill: parent
                                text: qsTr("Unreachable")
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                color: "#BF360C"
                            }
                        }

                    }

                }

            }

        }

    }

}
