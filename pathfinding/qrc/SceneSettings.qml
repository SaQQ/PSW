import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2

import "controls"

GridLayout {
    id: root

    signal clearRectangles()
    signal simulate()

    property bool enabled: true

    property int obstaclesCount: 0

    columns: 2

    readonly property real l1: l1textField.value
    readonly property real l2: l2textField.value

    property alias highlightReachable: highlightReachableCheckBox.checked

    Text {
        text: qsTr("First stage length")
        color: "#f5f5f5"
    }

    DoubleTextField {
        id: l1textField
        minValue: 1
        maxValue: 500
        decimals: 0
        text: "100"
        Layout.fillWidth: true
        enabled: root.enabled
    }

    Text {
        text: qsTr("Second stage length")
        color: "#f5f5f5"
    }

    DoubleTextField {
        id: l2textField
        minValue: 1
        maxValue: 500
        decimals: 0
        text: "200"
        Layout.fillWidth: true
        enabled: root.enabled
    }

    Text {
        text: qsTr("Obstacles count")
        color: "#f5f5f5"
    }

    Text {
        text: root.obstaclesCount
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
    }

    Text {
        text: qsTr("Highlight reachable")
        color: "#f5f5f5"
    }

    CheckBox {
        id: highlightReachableCheckBox
        checked: true
        Layout.alignment: Qt.AlignHCenter
    }

    Button {
        id: clearRectanglesButton
        text: qsTr("Clear obstacles")
        onClicked: root.clearRectangles()
        enabled: root.enabled
        Layout.fillWidth: true
    }

    Button {
        id: simulateButton
        text: qsTr("Simulate")
        onClicked: root.simulate()
        enabled: root.enabled
        Layout.fillWidth: true
    }

}