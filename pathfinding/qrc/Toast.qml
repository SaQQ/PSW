import QtQuick 2.7
import QtQml 2.2

Rectangle {
    id: root

    default property alias content: content.children

    width: 100
    height: 30

    opacity: 0.0

    Behavior on opacity {
        NumberAnimation { duration: 100 }
    }

    function show() {
        root.opacity = 1.0
        hideTimer.restart()
    }

    function hide() {
        root.opacity = 0.0
        hideTimer.stop()
    }

    opacity: 1

    radius: 5

    color: "#616161"

    border {
        width: 1
    }

    Item {
        id: content

        clip: true

        anchors {
            fill: parent
            margins: 5
        }
    }

    Timer {
        id: hideTimer
        interval: 1000
        onTriggered: root.opacity = 0
    }

}