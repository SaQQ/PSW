#ifndef PSW_PATHFINDING_CONFIGURATIONSPACE_HPP
#define PSW_PATHFINDING_CONFIGURATIONSPACE_HPP

#include <cmath>

#include <array>

#include "SceneModel.hpp"

class ConfigurationSpace {
public:
    constexpr static int a1Division = 360;
    constexpr static int a2Division = 360;

    explicit ConfigurationSpace(const SceneModel *model);

    bool colliding(int a1i, int a2i) const {
        return arr[a1i][a2i];
    }

    bool colliding(float a1, float a2) const {
        int a1i, a2i;
        std::tie(a1i, a2i) = anglesToInd(a1, a2);
        return colliding(a1i, a2i);
    }

    bool colliding(SceneModel *sceneModel, float a1, float a2) const {
        return isConfigurationColliding(sceneModel, 2 * M_PI * normalizeAngle(a1) / 360.0f, 2 * M_PI * normalizeAngle(a2) / 360.0f);
    }

    std::pair<std::vector<std::pair<int, int>>, std::array<std::array<int, a2Division>, a1Division>> findPath(float a1Start, float a2Start, float a1End, float a2End);

    std::pair<std::vector<std::pair<int, int>>, std::array<std::array<int, a2Division>, a1Division>> findPath(int a1iStart, int a2iStart, int a1iEnd, int a2iEnd);

private:
    std::array<std::array<bool, a2Division>, a1Division> arr;

    inline std::pair<int, int> anglesToInd(float a1, float a2) const {
        a1 = normalizeAngle(a1);
        a2 = normalizeAngle(a2);
        return { static_cast<int>(a1 / (360.0 / a1Division)), static_cast<int>(a2 / (360.0 / a2Division))};
    }

    inline float normalizeAngle(float a) const {
        while(a > 360.0f)
            a -= 360.0f;
        while(a < 0.0f)
            a += 360.0f;
        return a;
    }

    static bool isConfigurationColliding(const SceneModel *sceneModel, float a1, float a2);
};

#endif //PSW_PATHFINDING_CONFIGURATIONSPACE_HPP
