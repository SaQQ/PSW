#ifndef PSW_PATHFINDING_GEOMETRYALGORITHMS_HPP
#define PSW_PATHFINDING_GEOMETRYALGORITHMS_HPP

#include <cmath>

namespace GeometryAlgorithms
{

    constexpr double eps = 0.00001;

    template<typename Point2D>
    double SideOfPoint(const Point2D &start, const Point2D &end, const Point2D &point)
    {
        return start.x * end.y + end.x * point.y + point.x * start.y - point.x * end.y - start.x * point.y -
               end.x * start.y;
    }

    template<typename Point2D>
    bool Collinear(const Point2D &p1, const Point2D &p2, const Point2D &p3)
    {
        return std::abs(SideOfPoint(p1, p2, p3)) < eps;
    }

    template<typename Point2D>
    bool PointOnSegment(const Point2D &start, const Point2D &end, const Point2D &point)
    {
        return std::min(start.x, end.x) - eps <= point.x
               && point.x <= std::max(start.x, end.x) + eps
               && std::min(start.y, end.y) - eps <= point.y
               && point.y <= std::max(start.y, end.y) + eps
               && Collinear(start, end, point);
    }

    template<typename Point2D>
    bool DoSegmentsIntersect(const Point2D &s1, const Point2D &e1, const Point2D &s2, const Point2D &e2)
    {
        if (PointOnSegment(s1, e1, s2) || PointOnSegment(s1, e1, e2) || PointOnSegment(s2, e2, s1) ||
            PointOnSegment(s2, e2, e1))
            return true;

        if (SideOfPoint(s1, e1, s2) * SideOfPoint(s1, e1, e2) >= 0)
            return false;

        return SideOfPoint(s2, e2, s1) * SideOfPoint(s2, e2, e1) < 0;
    }

    template<typename Coord>
    bool DoesSegmentIntersectWithRect(Coord xMin, Coord yMin, Coord xMax, Coord yMax,
                                      Coord x0, Coord y0, Coord x1, Coord y1)
    {

        constexpr int left = 1 << 0;
        constexpr int right = 1 << 1;
        constexpr int bottom = 1 << 2;
        constexpr int top = 1 << 3;

        auto outCode = [=](Coord x, Coord y)
        {
            int code = 0;

            if (x < xMin)
                code |= left;
            else if (x > xMax)
                code |= right;

            if (y < yMin)
                code |= bottom;
            else if (y > yMax)
                code |= top;

            return code;
        };

        auto outCode0 = outCode(x0, y0);
        auto outCode1 = outCode(x1, y1);

        while (true)
        {
            if (!(outCode0 | outCode1))
            {
                /* Inside */
                return true;
            } else if (outCode0 & outCode1)
            {
                /* On the same side */
                return false;
            } else
            {
                /* Pick endpoint outside clip rect */
                auto outCodeOut = outCode0 ? outCode0 : outCode1;

                Coord x, y;

                if (outCodeOut & top)
                {
                    x = x0 + (x1 - x0) * (yMax - y0) / (y1 - y0);
                    y = yMax;
                } else if (outCodeOut & bottom)
                {
                    x = x0 + (x1 - x0) * (yMin - y0) / (y1 - y0);
                    y = yMin;
                } else if (outCodeOut & right)
                {
                    y = y0 + (y1 - y0) * (xMax - x0) / (x1 - x0);
                    x = xMax;
                } else if (outCodeOut & left)
                {
                    y = y0 + (y1 - y0) * (xMin - x0) / (x1 - x0);
                    x = xMin;
                }

                if (outCodeOut == outCode0)
                {
                    x0 = x;
                    y0 = y;
                    outCode0 = outCode(x0, y0);
                } else
                {
                    x1 = x;
                    y1 = y;
                    outCode1 = outCode(x1, y1);
                }
            }
        }

    }

}

#endif //PSW_PATHFINDING_GEOMETRYALGORITHMS_HPP
