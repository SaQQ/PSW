// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef PSW_SIMULATOR_HPP
#define PSW_SIMULATOR_HPP

#include <memory>

#include <QObject>
#include <QString>

#include <QTimer>
#include <QElapsedTimer>

#include "ConfigurationSpace.hpp"
#include "CSpaceImageProvider.hpp"

class PathfindingSimulator : public QObject {
    Q_OBJECT

    Q_PROPERTY(QString state READ getState NOTIFY stateChanged)

    Q_PROPERTY(float alpha1 READ getAlpha1 WRITE setAlpha1 NOTIFY alpha1Changed)
    Q_PROPERTY(float alpha2 READ getAlpha2 WRITE setAlpha2 NOTIFY alpha2Changed)

public:
    constexpr static int timerMsecInterval = 30;

    explicit PathfindingSimulator(QObject* parent = nullptr);

    Q_INVOKABLE bool configure(SceneModel *model, CSpaceImageProvider* imageProvider);

    Q_INVOKABLE bool unconfigure(CSpaceImageProvider* imageProvider);

    Q_INVOKABLE bool isColliding(float a1, float a2);

    Q_INVOKABLE bool isReachable(float a1s, float a2s, float a1e, float a2e);

    Q_INVOKABLE void simulate(float a1s, float a2s, float a1e, float a2e, CSpaceImageProvider* imageProvider);

    Q_INVOKABLE void stop(CSpaceImageProvider* imageProvider);

    QString getState() const {
        return cSpace == nullptr ? "unconfigured" : "configured";
    }

    float getAlpha1() const { return alpha1; }

    void setAlpha1(float value) {
        alpha1 = value;
        emit alpha1Changed();
    }

    float getAlpha2() const { return alpha2; }

    void setAlpha2(float value) {
        alpha2 = value;
        emit alpha2Changed();
    }

signals:
    void stateChanged();

    void error(const QString &msg);

    void alpha1Changed();
    void alpha2Changed();

private slots:
    void tick();

private:
    std::unique_ptr<ConfigurationSpace> cSpace;
    SceneModel *sceneModel;
    float alpha1, alpha2;

    QTimer* timer;
    QElapsedTimer* elapsedTimer;
    qint64 leftover;

    std::vector<std::pair<int, int>> path;
    std::vector<std::pair<int, int>>::const_iterator path_iterator;
};


#endif //PSW_SIMULATOR_HPP
