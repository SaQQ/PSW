#ifndef PSW_PATHFINDING_SCENEMODEL_HPP
#define PSW_PATHFINDING_SCENEMODEL_HPP

#include <cmath>

#include <vector>

#include <QAbstractListModel>

#include "RevTaskResult.hpp"

struct RectandleDesc {
    float x1;
    float y1;
    float x2;
    float y2;

    float width() const { return std::abs(x1 - x2); }
    float height() const { return std::abs(y1 - y2); }
    float x() const { return std::min(x1, x2); }
    float y() const { return std::min(y1, y2); }
};

class SceneModel : public QAbstractListModel {
    Q_OBJECT

    Q_PROPERTY(int rectanglesCount READ getRectanglesCount NOTIFY rectanglesCountChanged)

    Q_PROPERTY(float l1 READ getL1 WRITE setL1 NOTIFY l1Changed)
    Q_PROPERTY(float l2 READ getL2 WRITE setL2 NOTIFY l2Changed)

public:
    explicit SceneModel(QObject *parent = nullptr);

    enum SceneModelRoles {
        XPosRole = Qt::UserRole + 1,
        YPosRole,
        WidthRole,
        HeightRole
    };

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &parent) const override {
        return static_cast<int>(rectangles.size());
    }

    QVariant data(const QModelIndex &index, int role) const override;

    bool removeRows(int row, int count, const QModelIndex &parent) override;

    Q_INVOKABLE bool removeRow(int row);

    Q_INVOKABLE void startNewRectangle(float x, float y);

    Q_INVOKABLE void newRectangleDrag(float x, float y);

    Q_INVOKABLE void finishNewRectangle(float x, float y);

    Q_INVOKABLE void clearRectangles();

    Q_INVOKABLE bool solveRevTask(float x, float y, RevTaskResult *result) const {
        try {
            auto solution = revTask(x, y);
            result->setValid(solution.first, solution.second);
            return true;
        } catch (const std::exception&) {
            result->setInvalid();
            return false;
        }
    }

    float getL1() const { return l1; }

    void setL1(float value) {
        l1 = value;
        emit l1Changed();
    }

    float getL2() const { return l2; }

    void setL2(float value) {
        l2 = value;
        emit l2Changed();
    }

    int getRectanglesCount() const {
        return rowCount(QModelIndex());
    }

signals:
    void l1Changed();
    void l2Changed();
    void alpha1Changed();
    void alpha2Changed();
    void rectanglesCountChanged();
private:
    std::vector<RectandleDesc> rectangles;

    float l1, l2;

    std::pair<std::pair<float, float>, std::pair<float, float>> revTask(float x, float y) const;
};

#endif //PSW_PATHFINDING_SCENEMODEL_HPP
