// Created by Paweł Sobótka on 07.12.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef PSW_PATHFINDING_REVTASKRESULT_HPP
#define PSW_PATHFINDING_REVTASKRESULT_HPP

#include <QObject>

class RevTaskResult : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool valid READ getValid NOTIFY changed)

    Q_PROPERTY(float sol1a1 READ getSol1a1 NOTIFY changed)
    Q_PROPERTY(float sol1a2 READ getSol1a2 NOTIFY changed)
    Q_PROPERTY(float sol2a1 READ getSol2a1 NOTIFY changed)
    Q_PROPERTY(float sol2a2 READ getSol2a2 NOTIFY changed)
public:
    explicit RevTaskResult(QObject *parent = nullptr);

    bool getValid() const { return valid; }

    float getSol1a1() const { return sol1.first; }

    float getSol1a2() const { return sol1.second; }

    float getSol2a1() const { return sol2.first; }

    float getSol2a2() const { return sol2.second; }

    void setValid(std::pair<float, float> s1, std::pair<float, float> s2) {
        valid = true;
        sol1.first = s1.first;
        sol1.second = s1.second;
        sol2.first = s2.first;
        sol2.second = s2.second;
        emit changed();
    }

    void setInvalid() {
        valid = false;
        sol1.first = 0.0f;
        sol1.second = 0.0f;
        sol2.first = 0.0f;
        sol2.second = 0.0f;
        emit changed();
    }

signals:
    void changed();

private:
    bool valid;
    std::pair<float, float> sol1, sol2;

};

#endif //PSW_PATHFINDING_REVTASKRESULT_HPP
