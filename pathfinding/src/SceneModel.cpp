#include "SceneModel.hpp"

#include <cmath>

SceneModel::SceneModel(QObject *parent) : QAbstractListModel(parent), l1(180.0), l2(150.0)
{
}

QHash<int, QByteArray> SceneModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[XPosRole] = "x";
    roles[YPosRole] = "y";
    roles[WidthRole] = "width";
    roles[HeightRole] = "height";
    return roles;
}

QVariant SceneModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const auto &rect = rectangles[index.row()];

    switch(role) {
        case XPosRole:
            return QVariant(rect.x());
        case YPosRole:
            return QVariant(rect.y());
        case WidthRole:
            return QVariant(rect.width());
        case HeightRole:
            return QVariant(rect.height());
        default:
            return QVariant();
    }
}

void SceneModel::startNewRectangle(float x, float y)
{
    auto index = static_cast<int>(rectangles.size());
    beginInsertRows(QModelIndex(), index, index);
    rectangles.push_back({x, y, x, y});
    endInsertRows();
    emit rectanglesCountChanged();
}

void SceneModel::newRectangleDrag(float x, float y)
{
    auto &rectDesc = rectangles.back();
    auto index = static_cast<int>(rectangles.size()) - 1;
    rectDesc.x2 = x;
    rectDesc.y2 = y;
    dataChanged(this->index(index), this->index(index), {XPosRole, YPosRole, WidthRole, HeightRole});
}

void SceneModel::finishNewRectangle(float x, float y)
{

}

void SceneModel::clearRectangles()
{
    beginResetModel();
    rectangles.clear();
    endResetModel();
    emit rectanglesCountChanged();
}

std::pair<std::pair<float, float>, std::pair<float, float>> SceneModel::revTask(float x, float y) const {
    float d = std::sqrt(x * x + y * y);

    if (d > l1 + l2)
        throw std::invalid_argument("point beyond reach");

    if (d < std::abs(l1 - l2))
        throw std::invalid_argument("point beyond reach");

    if (d < 0.00001)
        throw std::invalid_argument("point beyond reach");

    float nx = x / d;
    float ny = y / d;

    float xr = (d * d - l2 * l2 + l1 * l1) / (2 * d);
    float yr = std::sqrt(l1 * l1 - xr * xr);

    float px1 = xr * nx + yr * -ny;
    float py1 = xr * ny + yr * nx;

    float px2 = xr * nx - yr * -ny;
    float py2 = xr * ny - yr * nx;

    float a11 = std::atan2(py1, px1);
    float a21 = std::atan2(y - py1, x - px1) - a11;

    float a12 = std::atan2(py2, px2);
    float a22 = std::atan2(y - py2, x - px2) - a12;

    return {{static_cast<float>(180.0 * a11 / M_PI), static_cast<float>(180.0 * a21 / M_PI)},
            {static_cast<float>(180.0 * a12 / M_PI), static_cast<float>(180.0 * a22 / M_PI)}};
}

bool SceneModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    rectangles.erase(rectangles.begin() + row, rectangles.begin() + (row + count));
    endRemoveRows();
    emit rectanglesCountChanged();
    return true;
}

bool SceneModel::removeRow(int row)
{
    return removeRows(row, 1, QModelIndex());
}




