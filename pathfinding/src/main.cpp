// Created by Paweł Sobótka on 04.12.17.
// Rail-Mil Computers Sp. z o.o.

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlContext>

#include "SceneModel.hpp"
#include "PathfindingSimulator.hpp"

int main(int ac, char **av)
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(ac, av);

    QQmlApplicationEngine engine;

    qmlRegisterType<RevTaskResult>("PSW", 1, 0, "RevTaskResult");
    qmlRegisterType<SceneModel>("PSW", 1, 0, "SceneModel");
    qmlRegisterType<PathfindingSimulator>("PSW", 1, 0, "PathfindingSimulator");

    CSpaceImageProvider imageProvider;

    engine.addImageProvider("cSpaceImagesProvider", &imageProvider);
    engine.rootContext()->setContextProperty("cSpaceImagesProvider", &imageProvider);

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
