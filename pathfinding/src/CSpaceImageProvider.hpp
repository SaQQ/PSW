#ifndef PSW_PATHFINDING_CSPACEIMAGEPROVIDER_HPP
#define PSW_PATHFINDING_CSPACEIMAGEPROVIDER_HPP

#include <QObject>
#include <QQuickImageProvider>

#include "ConfigurationSpace.hpp"

class CSpaceImageProvider : public QObject, public QQuickImageProvider {
    Q_OBJECT
public:
    constexpr static int width = 360;
    constexpr static int height = 360;

    explicit CSpaceImageProvider(QObject *parent = nullptr);

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;

    void setCSpaceImage(const ConfigurationSpace* cSpace);

    void setCSpaceImage(const ConfigurationSpace* cSpace, const std::vector<std::pair<int, int>> &path);

    void setCSpaceImage(const ConfigurationSpace *cSpace, const std::vector<std::pair<int, int>> &path, const std::array<std::array<int, ConfigurationSpace::a2Division>, ConfigurationSpace::a1Division> &distances_from_start);

    void resetCSpaceImage();

signals:
    void imageChanged(const QString &id);

private:
    QImage cSpaceImage;
};

#endif //PSW_PATHFINDING_CSPACEIMAGEPROVIDER_HPP
