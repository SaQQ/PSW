// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#include <cmath>

#include <QDebug>

#include "PathfindingSimulator.hpp"

PathfindingSimulator::PathfindingSimulator(QObject *parent) : sceneModel(nullptr), alpha1(0.0), alpha2(0.0) {
    timer = new QTimer{this};
    timer->setInterval(timerMsecInterval);
    elapsedTimer = new QElapsedTimer;
    leftover = 0;
    QObject::connect(timer, &QTimer::timeout, this, &PathfindingSimulator::tick);
}

bool PathfindingSimulator::configure(SceneModel *model, CSpaceImageProvider* imageProvider) {
    sceneModel = model;
    cSpace = std::make_unique<ConfigurationSpace>(model);
    imageProvider->setCSpaceImage(cSpace.get());
	return true;
}

bool PathfindingSimulator::unconfigure(CSpaceImageProvider* imageProvider) {
    sceneModel = nullptr;
    cSpace = nullptr;
    imageProvider->resetCSpaceImage();
	return true;
}

bool PathfindingSimulator::isColliding(float a1, float a2) {

    if (cSpace == nullptr) {
        emit error("Simulator not configured");
        return false;
    }

    return cSpace->colliding(a1, a2);

}

bool PathfindingSimulator::isReachable(float a1s, float a2s, float a1e, float a2e) {
    if (cSpace == nullptr) {
        emit error("Simulator not configured");
        return false;
    }

    try {
        cSpace->findPath(a1s, a2s, a1e, a2e);
        return true;
    } catch (const std::invalid_argument&) {
        return false;
    }
}

void PathfindingSimulator::simulate(float a1s, float a2s, float a1e, float a2e, CSpaceImageProvider* imageProvider) {
    if (cSpace == nullptr) {
        emit error("Simulator not configured");
        return;
    }

    try {
        auto result = cSpace->findPath(a1s, a2s, a1e, a2e);
        path = result.first;
        path_iterator = path.cbegin();
        // imageProvider->setCSpaceImage(cSpace.get(), path);
        imageProvider->setCSpaceImage(cSpace.get(), path, result.second);

        setAlpha1(a1s);
        setAlpha2(a2s);

        leftover = 0;
        elapsedTimer->start();
        timer->start();

    } catch (const std::invalid_argument &ex) {
        emit error(ex.what());
    }

}

void PathfindingSimulator::stop(CSpaceImageProvider *imageProvider) {
    imageProvider->setCSpaceImage(cSpace.get());
}

void PathfindingSimulator::tick() {
    auto elapsedMsec = elapsedTimer->elapsed() + leftover;

    elapsedTimer->restart();

    leftover = elapsedMsec % timer->interval();

    for(int i = 0; i < elapsedMsec / timerMsecInterval; ++i)
    {
        auto inds = *path_iterator;
        auto a1 = static_cast<float>(inds.first * 360.0 / ConfigurationSpace::a1Division);
        auto a2 = static_cast<float>(inds.second * 360.0 / ConfigurationSpace::a2Division);
        setAlpha1(a1);
        setAlpha2(a2);
        path_iterator++;
        if (path_iterator == path.cend())
            path_iterator = path.cbegin();
    }

}


