#include "ConfigurationSpace.hpp"

#include <set>
#include <utility>

#include "GeometryAlgorithms.hpp"

#define OCTAL_CONNECTIVITY

ConfigurationSpace::ConfigurationSpace(const SceneModel *sceneModel)
{
    for (int a1i = 0; a1i < a1Division; ++a1i)
    {
        for (int a2i = 0; a2i < a2Division; ++a2i)
        {
            auto a1 = static_cast<float>(a1i * 2.0 * M_PI / a1Division);
            auto a2 = static_cast<float>(a2i * 2.0 * M_PI / a2Division);

            arr[a1i][a2i] = isConfigurationColliding(sceneModel, a1, a2);
        }
    }
}

bool ConfigurationSpace::isConfigurationColliding(const SceneModel *sceneModel, float a1, float a2)
{
    auto l1 = sceneModel->getL1();
    auto l2 = sceneModel->getL2();

    auto ca1 = std::cos(a1);
    auto sa1 = std::sin(a1);

    struct {
        float x;
        float y;
    } start = {0, 0}, mid = {l1 * ca1, l1 * sa1}, end = {l1 * ca1 + l2 * std::cos(a1 + a2), l1 * sa1 + l2 * std::sin(a1 + a2)};

    for (int i = 0; i < sceneModel->rowCount(QModelIndex()); ++i) {
        auto xMin = sceneModel->data(sceneModel->index(i, 0), SceneModel::XPosRole).toFloat();
        auto yMin = sceneModel->data(sceneModel->index(i, 0), SceneModel::YPosRole).toFloat();
        auto w = sceneModel->data(sceneModel->index(i, 0), SceneModel::WidthRole).toFloat();
        auto h = sceneModel->data(sceneModel->index(i, 0), SceneModel::HeightRole).toFloat();
        auto xMax = xMin + w;
        auto yMax = yMin + h;

        if (GeometryAlgorithms::DoesSegmentIntersectWithRect(xMin, yMin, xMax, yMax, start.x, start.y, mid.x, mid.y))
            return true;
        if (GeometryAlgorithms::DoesSegmentIntersectWithRect(xMin, yMin, xMax, yMax, mid.x, mid.y, end.x, end.y))
            return true;
    }

    return false;
}

std::pair<std::vector<std::pair<int, int>>, std::array<std::array<int, ConfigurationSpace::a2Division>, ConfigurationSpace::a1Division>>
ConfigurationSpace::findPath(float a1Start, float a2Start, float a1End, float a2End)
{
    int a1iStart, a2iStart, a1iEnd, a2iEnd;
    std::tie(a1iStart, a2iStart) = anglesToInd(a1Start, a2Start);
    std::tie(a1iEnd, a2iEnd) = anglesToInd(a1End, a2End);
    return findPath(a1iStart, a2iStart, a1iEnd, a2iEnd);
}

std::pair<std::vector<std::pair<int, int>>, std::array<std::array<int, ConfigurationSpace::a2Division>, ConfigurationSpace::a1Division>>
ConfigurationSpace::findPath(int a1iStart, int a2iStart, int a1iEnd, int a2iEnd)
{
    if (a1iStart < 0 || a2iStart < 0 || a1iEnd < 0 || a2iEnd < 0)
        throw std::invalid_argument("index lower than 0");
    if (a1iStart >= a1Division || a2iStart >= a2Division || a1iEnd >= a1Division || a2iEnd >= a2Division)
        throw std::invalid_argument("index greater than division");

    auto neighbours = [this] (std::pair<int, int> point) {
        std::vector<std::pair<int, int>> n;
#ifdef OCTAL_CONNECTIVITY
        n.reserve(8);
#else
        n.reserve(4);
#endif
        if(!arr[(point.first - 1 + a1Division) % a1Division][point.second])
            n.emplace_back((point.first - 1 + a1Division) % a1Division, point.second);
        if(!arr[(point.first + 1) % a1Division][point.second])
            n.emplace_back((point.first + 1) % a1Division, point.second);
        if(!arr[point.first][(point.second - 1 + a2Division) % a2Division])
            n.emplace_back(point.first, (point.second - 1 + a2Division) % a2Division);
        if(!arr[point.first][(point.second + 1) % a2Division])
            n.emplace_back(point.first, (point.second + 1) % a2Division);
#ifdef OCTAL_CONNECTIVITY
        if(!arr[(point.first - 1 + a1Division) % a1Division][(point.second - 1 + a2Division) % a2Division])
            n.emplace_back((point.first - 1 + a1Division) % a1Division, (point.second - 1 + a2Division) % a2Division);
        if(!arr[(point.first + 1) % a1Division][(point.second - 1 + a2Division) % a2Division])
            n.emplace_back((point.first + 1) % a1Division, (point.second - 1 + a2Division) % a2Division);
        if(!arr[(point.first - 1 + a1Division) % a1Division][(point.second + 1) % a2Division])
            n.emplace_back((point.first - 1 + a1Division) % a1Division, (point.second + 1) % a2Division);
        if(!arr[(point.first + 1) % a1Division][(point.second + 1) % a2Division])
            n.emplace_back((point.first + 1) % a1Division, (point.second + 1) % a2Division);
#endif
        return n;
    };

    auto distance_to_end = [=](std::pair<int, int> point) {
        int dist = 0;

        auto a1 = point.first;
        auto a2 = point.second;

        auto a1e = a1iEnd;
        auto a2e = a2iEnd;

        if (a1 > a1e)
            std::swap(a1, a1e);

        if (a2 > a2e)
            std::swap(a2, a2e);

        auto da1_f = a1e - a1;
        auto da2_f = a2e - a2;
        auto da1_t = a1 + (a1Division - 1 - a1e);
        auto da2_t = a2 + (a2Division - 1 - a2e);

        da1_f *= da1_f;
        da2_f *= da2_f;
        da1_t *= da1_t;
        da2_t *= da2_t;

        return std::sqrt(std::min(std::min(da1_f + da2_f, da1_f + da2_t), std::min(da1_t + da2_f, da1_t + da2_t)));
    };

    using open_node = std::pair<double, std::pair<int, int>>;

    std::array<std::array<int, a2Division>, a1Division> distance_from_start;
    std::array<std::array<std::pair<int, int>, a2Division>, a1Division> preceding;
    std::array<std::array<bool, a2Division>, a1Division> in_open, in_closed;

    for(std::size_t row = 0; row < a1Division; ++row) {
        distance_from_start[row].fill(a2Division * a1Division + 1);
        preceding[row].fill({-1, -1});
        in_open[row].fill(false);
        in_closed[row].fill(false);
    }

    std::set<open_node> open;

    auto construct_path = [&preceding, a1iStart, a2iStart, a1iEnd, a2iEnd](){
        std::vector<std::pair<int, int>> path;
        std::pair<int, int> current{a1iEnd, a2iEnd};
        while (current != std::make_pair(a1iStart, a2iStart)) {
            path.emplace_back(current.first, current.second);
            int nextA1 = preceding[current.first][current.second].first;
            int nextA2 = preceding[current.first][current.second].second;
            current.first = nextA1;
            current.second = nextA2;
        }
        std::reverse(path.begin(), path.end());
        return path;
    };

    auto start = std::make_pair(a1iStart, a2iStart);

    in_open[start.first][start.second] = true;
    open.insert({0, start});
    distance_from_start[a1iStart][a2iStart] = 0;

    std::size_t visited_vertices = 1;

    while (!open.empty()) {
        auto current = (*open.begin()).second;

        if (current == std::make_pair(a1iEnd, a2iEnd)) {
            return std::make_pair(construct_path(), distance_from_start);
        }

        visited_vertices++;

        open.erase(open.begin());
        in_open[current.first][current.second] = false;
        in_closed[current.first][current.second] = true;

        for (auto neighbour : neighbours(current)) {
            if (in_closed[neighbour.first][neighbour.second])
                continue;

            if (distance_from_start[current.first][current.second] + 1 < distance_from_start[neighbour.first][neighbour.second]) {
                distance_from_start[neighbour.first][neighbour.second] = distance_from_start[current.first][current.second] + 1;
                preceding[neighbour.first][neighbour.second].first = current.first;
                preceding[neighbour.first][neighbour.second].second = current.second;

                if (!in_open[neighbour.first][neighbour.second])
                    open.insert(std::make_pair(distance_from_start[neighbour.first][neighbour.second], neighbour));

            }

        }
    }

    throw std::invalid_argument("path not found");
}


