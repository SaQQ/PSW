// Created by Paweł Sobótka on 18.08.17.
// Rail-Mil Computers Sp. z o.o.

#include <QApplication>
#include <QQmlApplicationEngine>

#include "Simulator.hpp"
#include "Formula.hpp"

int main(int argc, char** argv) {

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<PathfindingSimulator>("PSW", 1, 0, "Simulator");
    qmlRegisterType<Formula>("PSW", 1, 0, "Formula");

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}