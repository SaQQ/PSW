// Created by Paweł Sobótka on 06.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "Formula.hpp"

Formula::Formula(QObject *parent) : QObject(parent),
    text{"0.0"}, valid{true}, t{0.0} {
    parser.DefineVar("t", &t);
    parser.SetExpr("0.0");
}

void Formula::setText(const QString &text) {
    this->text = text;
    emit textChanged();

    buildParser();
}

double Formula::evaluate(double x) {
    t = x;
    return parser.Eval();
}

void Formula::setValid(bool valid) {
    if(this->valid != valid) {
        this->valid = valid;
        emit validChanged();
    }
}

void Formula::buildParser() {
    using namespace mu;
    try {
        parser.SetExpr(text.toStdString());
        parser.Eval();
        setValid(true);
    } catch(Parser::exception_type &e) {
        setValid(false);
    }
}



