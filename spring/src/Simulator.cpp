// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#include <QDebug>
#include <QtQml/QQmlListProperty>
#include <QtCore/QAbstractItemModel>

#include "Simulator.hpp"
#include "../../gyration/src/Quaternion.hpp"

namespace {
    inline double integrate_forward(double currentValue, double currentDerivative, double dt) {
        return currentValue + currentDerivative * dt;
    }

    inline double integrate_central(double previousValue, double currentDerivative, double dt) {
        return previousValue + 2.0 *  currentDerivative * dt;
    }
}

PathfindingSimulator::PathfindingSimulator(QObject *parent) : QObject(parent),
    dt{0.01}, x0{7.0}, v0{0.0}, mass{1.0}, elasticity{5.0}, drag{0.1},
    externalForce{nullptr}, baseOffset{nullptr},
    running{false}, paused{false} {
    timer = new QTimer{this};
    elapsedTimer = new QElapsedTimer;
    leftover = 0;
    QObject::connect(timer, &QTimer::timeout, this, &PathfindingSimulator::tick);

    reset();
}

bool PathfindingSimulator::start(double dt, double x0, double v0, double m, double k, double c, Formula *externalForce, Formula *baseOffset) {
    if(running)
        return false;

    this->dt = dt;
    this->x0 = x0;
    this->v0 = v0;
    this->mass = m;
    this->elasticity = k;
    this->drag = c;
    this->externalForce = externalForce;
    this->baseOffset = baseOffset;

    currentState = { x0, v0, 0.0 };
    previousState = { x0, v0, -dt };

    emit currentStateChanged();

    timer->setInterval(static_cast<int>(dt * 1000));
    timer->start();
    elapsedTimer->start();
    leftover = 0;

    running = true;
    emit runningChanged();

    this->maxError = 0;

    return true;
}

bool PathfindingSimulator::togglePause() {
    if(!running)
        return false;
    paused = !paused;

    if(paused)
        timer->stop();
    else {
        timer->start();
        elapsedTimer->restart();
        leftover = 0;
    }

    emit pausedChanged();
    return true;
}

bool PathfindingSimulator::reset() {
    running = paused = false;
    maxError = 0.0f;
    emit runningChanged();
    emit pausedChanged();
    emit errorChanged();

    timer->stop();

    currentState = { x0, v0, 0.0 };
    previousState = { x0, v0, -dt };

    emit currentStateChanged();

    return true;
}

void PathfindingSimulator::tick() {
    if(!running || paused)
        return;

    state nextState;

    auto elapsedMsec = elapsedTimer->elapsed() + leftover;

    elapsedTimer->restart();

    leftover = elapsedMsec % timer->interval();

    for(int i = 0; i < elapsedMsec / timer->interval(); ++i) {

        nextState.position = dt * dt * getAcceleration() + 2 * currentState.position - previousState.position;

        // nextState.position = integrate_central(previousState.position, currentState.velocity, dt);
        nextState.velocity = (nextState.position - currentState.position) / dt;
        nextState.time = currentState.time + dt;

        auto good_solution = x0 * cos(nextState.time);
        auto error = std::abs(good_solution - nextState.position);
        if (nextState.time > 1.0 && error > maxError)
        {
            maxError = error;
            emit errorChanged();
        }

        previousState = currentState;
        currentState = nextState;
    }

    emit currentStateChanged();
    emit step();
}






