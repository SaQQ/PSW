// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef PSW_SIMULATOR_HPP
#define PSW_SIMULATOR_HPP

#include <QObject>
#include <QString>
#include <QLineSeries>

#include <QElapsedTimer>
#include <QTimer>

#include "Formula.hpp"

class PathfindingSimulator : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool running READ getRunning NOTIFY runningChanged)
    Q_PROPERTY(bool paused READ getPaused NOTIFY pausedChanged)

    Q_PROPERTY(double time READ getTime NOTIFY currentStateChanged)

    Q_PROPERTY(double position READ getPosition NOTIFY currentStateChanged)
    Q_PROPERTY(double velocity READ getVelocity NOTIFY currentStateChanged)
    Q_PROPERTY(double acceleration READ getAcceleration NOTIFY currentStateChanged)

    Q_PROPERTY(double elasticityForce READ getElasticityForce NOTIFY currentStateChanged)
    Q_PROPERTY(double dragForce READ getDragForce NOTIFY currentStateChanged)
    Q_PROPERTY(double externalForce READ getExternalForce NOTIFY currentStateChanged)
    Q_PROPERTY(double baseOffset READ getBaseOffset NOTIFY currentStateChanged)

    Q_PROPERTY(double error READ getError NOTIFY errorChanged)
public:
    explicit PathfindingSimulator(QObject* parent = nullptr);

    Q_INVOKABLE bool start(double dt, double x0, double v0, double m, double k, double c, Formula *externalForce, Formula *baseOffset);

    Q_INVOKABLE bool togglePause();

    Q_INVOKABLE bool reset();

    bool getRunning() const { return running; }

    bool getPaused() const { return paused; }

    double getTime() const { return currentState.time; }

    double getPosition() const { return currentState.position; }

    double getVelocity() const { return currentState.velocity; }

    double getAcceleration() const { return 1.0 / mass * (getElasticityForce() + getDragForce() + getExternalForce()); }

    double getElasticityForce() const { return -elasticity * (currentState.position - getBaseOffset()); }

    double getDragForce() const { return -drag * currentState.velocity; }

    double getExternalForce() const { return externalForce ? externalForce->evaluate(currentState.time) : 0.0; }

    double getBaseOffset() const { return baseOffset ? baseOffset->evaluate(currentState.time) : 0.0; }

    double getError() const { return maxError; }

signals:
    void runningChanged();
    void pausedChanged();

    void currentStateChanged();

    void step();

    void errorChanged();

private slots:
    void tick();

private:

    struct state {
        double position;
        double velocity;
        double time;
    } previousState, currentState;

    double dt, x0, v0, mass, elasticity, drag;

    Formula *externalForce, *baseOffset;

    bool running, paused;

    QTimer* timer;
    QElapsedTimer* elapsedTimer;
    qint64 leftover;

    double maxError;
};


#endif //PSW_SIMULATOR_HPP
