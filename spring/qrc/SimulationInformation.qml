import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GridLayout {
    id: root

    width: 220

    columns: 2

    property real time: 0
    property real baseOffset: 0

    property real position: 0
    property real velocity: 0
    property real acceleration: 0

    property real elasticityForce: 0
    property real dragForce: 0
    property real externalForce: 0

    property real maxError: 0

    property real valueWidth: 80

    Text {
        text: "Time"
        color: "#f5f5f5"
    }

    Text {
        text: root.time.toFixed(3) + " s"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Base offset"
        color: "#f5f5f5"
    }

    Text {
        text: root.baseOffset.toFixed(3) + " m"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Kinematics"
        font.bold: true
        color: "#f5f5f5"
        Layout.columnSpan: 2
    }

    // Position

    Text {
        text: "Position"
        color: "#f5f5f5"
    }

    Text {
        text: root.position.toFixed(3) + " m"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    // Velocity

    Text {
        text: "Velocity"
        color: "#f5f5f5"
    }

    Text {
        text: root.velocity.toFixed(3) + " m/s"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    // Acceleration

    Text {
        text: "Acceleration"
        color: "#f5f5f5"
    }

    Text {
        text: root.acceleration.toFixed(3) + " m/s²"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Dynamics"
        font.bold: true
        color: "#f5f5f5"
        Layout.columnSpan: 2
    }

    // Spring force

    Text {
        text: "Elasticity force"
        color: "#f5f5f5"
    }

    Text {
        text: root.elasticityForce.toFixed(3) + " N"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    // Drag force

    Text {
        text: "Drag force"
        color: "#f5f5f5"
    }

    Text {
        text: root.dragForce.toFixed(3) + " N"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    // External force

    Text {
        text: "External force"
        color: "#f5f5f5"
    }

    Text {
        text: root.externalForce.toFixed(3) + " N"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

    Text {
        text: "Miscellaneous"
        font.bold: true
        color: "#f5f5f5"
        Layout.columnSpan: 2
    }

    Text {
        text: "Maximal error"
        color: "#f5f5f5"
    }

    Text {
        text: root.maxError.toFixed(5) + " m"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
        Layout.maximumWidth: root.valueWidth
    }

}

