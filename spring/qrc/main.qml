import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import QtCharts 2.0

import "controls"

import PSW 1.0 as PSW

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: "PŚW - Spring"

    width: 1200
    height: 800

    minimumWidth: 1000
    minimumHeight: 600

    PSW.Formula {
        id: externalForceFormula
    }

    PSW.Formula {
        id: baseOffsetFormula
    }

    PSW.Simulator {
        id: simulator

        property bool startEnabled: !running && externalForceFormula.valid && baseOffsetFormula.valid
        property bool pauseEnabled: running
        property bool resetEnabled: running
    }

    header: ToolBar {
        Row {
            anchors.fill: parent
            spacing: 4

            ToolButton {
                text: "Simulation"
                onClicked: programMenu.open()
                Menu {
                    id: programMenu
                    y: parent.height

                    MenuItem {
                        text: "Start"
                        onTriggered: {
                            simulator.start(simulationProperties.integrationStep,
                                            simulationProperties.initialPosition,
                                            simulationProperties.initialVelocity,
                                            simulationProperties.mass,
                                            simulationProperties.elasticity,
                                            simulationProperties.drag,
                                            externalForceFormula, baseOffsetFormula)

                        }
                        enabled: simulator.startEnabled

                    }

                    MenuItem {
                        text: "Toggle Pause"
                        onTriggered: simulator.togglePause()
                        enabled: simulator.pauseEnabled
                    }

                    MenuItem {
                        text: "Reset"
                        onTriggered: {
                            simulator.reset()
                            positionSeries.clear()
                            velocitySeries.clear()
                            accelerationSeries.clear()
                            elasticitySeries.clear()
                            dragSeries.clear()
                            externalSeries.clear()
                            cspaceSeries.clear()

                            kinematicsYAxis.min = -1
                            kinematicsYAxis.max = 1

                            forceYAxis.min = -5
                            forceYAxis.max = 5

                            velocityAxis.min = -1
                            velocityAxis.max = 1
                            positionAxis.min = -1
                            positionAxis.max = 1
                        }
                        enabled: simulator.resetEnabled
                    }
                }
            }

        }
    }

    footer: ToolBar {
        height: 24
        Row {
            anchors.fill: parent
            anchors.margins: 4

            Label {
                text: simulator.running ? (simulator.paused ? qsTr("Running (Paused)") : qsTr("Running")) : qsTr("Idle")
            }
        }
    }

    Rectangle {
        id: background

        color: "#424242"

        anchors.fill: parent

        Item {
            id: leftPaneContainer

            width: 250

            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }

            ColumnLayout {

                anchors.fill: parent
                anchors.margins: 5

                spacing: 5

                ScrollableSectionPlate {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumHeight: 280
                    Layout.minimumHeight: 50

                    header: "Simulation information"

                    SimulationInformation {
                        id: simulationInformation

                        time: simulator.time
                        baseOffset: simulator.baseOffset

                        position: simulator.position
                        velocity: simulator.velocity
                        acceleration: simulator.acceleration

                        elasticityForce: simulator.elasticityForce
                        dragForce: simulator.dragForce
                        externalForce: simulator.externalForce

                        maxError: simulator.error
                    }
                }

                ScrollableSectionPlate {

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.minimumHeight: 50

                    header: "Simulation properties"

                    SimulationProperties {
                        id: simulationProperties

                        enabled: !simulator.running

                        externalForceFormulaValid: externalForceFormula.valid
                        baseOffsetFormulaValid: baseOffsetFormula.valid

                        externalForceTextField.onTextChanged: externalForceFormula.text = externalForceTextField.text
                        baseOffsetTextField.onTextChanged: baseOffsetFormula.text = baseOffsetTextField.text
                    }


                }

            }
        }

        Item {
            id: rightPaneContainer

            width: 350

            anchors {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }

            ColumnLayout {

                anchors.fill: parent
                anchors.margins: 5

                spacing: 5

                SectionPlate {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    header: "Graphs of forces"

                    ChartView {
                        id: forcesChart
                        anchors.fill: parent
                        antialiasing: true

                        ValueAxis {
                            id: forceTimeAxis
                            property real windowWidth: 15
                            min: simulator.time < 2 * windowWidth / 3 ? 0 : simulator.time - 2 * windowWidth / 3
                            max: simulator.time < 2 * windowWidth / 3 ? windowWidth : simulator.time + windowWidth / 3
                        }

                        ValueAxis {
                            id: forceYAxis
                            min: -5.0
                            max: 5.0
                        }

                        LineSeries {
                            id: elasticitySeries
                            name: "Elasticity"
                            axisX: forceTimeAxis
                            axisY: forceYAxis
                            useOpenGL: true
                        }

                        LineSeries {
                            id: dragSeries
                            name: "Drag"
                            axisX: forceTimeAxis
                            axisY: forceYAxis
                            useOpenGL: true
                        }

                        LineSeries {
                            id: externalSeries
                            name: "External"
                            axisX: forceTimeAxis
                            axisY: forceYAxis
                            useOpenGL: true
                        }

                    }

                }

                SectionPlate {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    header: "Configuration space graph"

                    ChartView {
                        id: cspaceChart
                        anchors.fill: parent
                        antialiasing: true

                        legend {
                            visible: false
                        }

                        ValueAxis {
                            id: positionAxis
                            min: -1
                            max: 1
                        }

                        ValueAxis {
                            id: velocityAxis
                            min: -1
                            max: 1
                        }

                        LineSeries {
                            id: cspaceSeries
                            name: "State"
                            axisX: positionAxis
                            axisY: velocityAxis
                            useOpenGL: true
                        }

                    }
                }

            }
        }

        Item {
            id: bottomPaneContainer

            height: 250

            anchors {
                left: leftPaneContainer.right
                right: rightPaneContainer.left
                bottom: parent.bottom
            }

            RowLayout {
                anchors.fill: parent
                anchors.topMargin: 5
                anchors.bottomMargin: 5

                spacing: 5

                SectionPlate {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    header: "Graphs of position, velocity and acceleration"

                    ChartView {
                        id: kinematicsChart
                        anchors.fill: parent
                        antialiasing: true

                        ValueAxis {
                            id: kinematicsTimeAxis
                            property real windowWidth: 15
                            min: simulator.time < 2 * windowWidth / 3 ? 0 : simulator.time - 2 * windowWidth / 3
                            max: simulator.time < 2 * windowWidth / 3 ? windowWidth : simulator.time + windowWidth / 3
                        }

                        ValueAxis {
                            id: kinematicsYAxis
                            min: -1
                            max: 1
                        }


                        LineSeries {
                            id: positionSeries
                            name: "Position"
                            axisX: kinematicsTimeAxis
                            axisY: kinematicsYAxis
                            useOpenGL: true
                        }

                        LineSeries {
                            id: velocitySeries
                            name: "Velocity"
                            axisX: kinematicsTimeAxis
                            axisY: kinematicsYAxis
                            useOpenGL: true
                        }

                        LineSeries {
                            id: accelerationSeries
                            name: "Acceleration"
                            axisX: kinematicsTimeAxis
                            axisY: kinematicsYAxis
                            useOpenGL: true
                        }

                    }
                }
            }
        }

        Item {
            id: centerPaneContainer

            anchors {
                top: parent.top
                bottom: bottomPaneContainer.top
                left: leftPaneContainer.right
                right: rightPaneContainer.left
            }

            SectionPlate {
                header: "Visualization"

                anchors.fill: parent
                anchors.topMargin: 5

                Rectangle {
                    anchors.fill: parent

                    border {
                        width: 1
                        color: simulator.running ? (simulator.paused ? "#3F51B5" : "#4CAF50") : "#212121"
                    }

                    Scene3D {
                        id: scene3d
                        anchors.fill: parent
                        anchors.margins: 1
                        focus: true
                        aspects: ["input", "logic", "render"]
                        cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

                        Entity {
                            id: sceneRoot
                            objectName: "sceneRoot"

                            property real l0: 6.0
                            property real dl: simulator.position
                            property real baseOffset: simulator.baseOffset

                            Camera {
                                id: camera
                                projectionType: CameraLens.PerspectiveProjection
                                fieldOfView: 45
                                nearPlane : 0.1
                                farPlane : 1000.0
                                position: Qt.vector3d( 0.0, 10.0, 30.0 )
                                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
                            }

                            OrbitCameraController {
                                camera: camera
                                linearSpeed: 0
                            }

                            components: [
                                RenderSettings {
                                    activeFrameGraph: ForwardRenderer {
                                        camera: camera
                                        clearColor: "gray"
                                    }
                                },
                                InputSettings { },
                                DirectionalLight {
                                    worldDirection: Qt.vector3d(1, -1, 1)
                                },
                                DirectionalLight {
                                    worldDirection: Qt.vector3d(-1, -1, -1)
                                }

                            ]

                            PhongMaterial {
                                id: metalMaterial
                                ambient: "#BDBDBD"
                                diffuse: "#BDBDBD"
                                specular: "#FAFAFA"
                            }

                            Mesh {
                                id: springMesh

                                property real radius: 2.9528
                                property real length: 13.7795

                                source: "qrc:/spring.obj"
                            }

                            CylinderMesh {
                                id: weightMesh
                                length: 5.0
                                radius: springMesh.radius + simulationProperties.mass
                            }

                            CuboidMesh {
                                id: baseMesh
                                xExtent: 8.0
                                yExtent: 1.0
                                zExtent: 8.0
                            }

                            Transform {
                                id: weightTransform
                                matrix: {
                                    var m = Qt.matrix4x4();
                                    m.translate(Qt.vector3d(0.0, -weightMesh.length / 2.0 - sceneRoot.dl, 0.0));
                                    return m;
                                }
                            }

                            Transform {
                                id: springTransform
                                matrix: {
                                    var m = Qt.matrix4x4();
                                    m.translate(Qt.vector3d(0.0, sceneRoot.l0 - sceneRoot.baseOffset, 0.0))
                                    m.scale(0.5, -(sceneRoot.l0 + sceneRoot.dl - sceneRoot.baseOffset) / springMesh.length, 0.5)
                                    return m;
                                }
                            }

                            Transform {
                                id: baseTransform
                                matrix: {
                                    var m = Qt.matrix4x4();
                                    m.translate(0, baseMesh.yExtent / 2 + sceneRoot.l0 - sceneRoot.baseOffset, 0)
                                    return m;
                                }
                            }

                            Entity {
                                id: spring
                                components: [ springMesh, metalMaterial, springTransform ]
                            }

                            Entity {
                                id: weight
                                components: [ weightMesh, metalMaterial, weightTransform ]
                            }

                            Entity {
                                id: base
                                components: [ baseMesh, metalMaterial, baseTransform ]
                            }

                        }

                    }

                }
            }


        }
    }


    Connections {
        target: simulator
        onStep: {
            positionSeries.append(simulator.time, simulator.position)
            velocitySeries.append(simulator.time, simulator.velocity)
            accelerationSeries.append(simulator.time, simulator.acceleration)

            kinematicsYAxis.min = Math.min(kinematicsYAxis.min, simulator.position, simulator.velocity, simulator.acceleration)
            kinematicsYAxis.max = Math.max(kinematicsYAxis.max, simulator.position, simulator.velocity, simulator.acceleration)

            elasticitySeries.append(simulator.time, simulator.elasticityForce)
            dragSeries.append(simulator.time, simulator.dragForce)
            externalSeries.append(simulator.time, simulator.externalForce)

            forceYAxis.min = Math.min(forceYAxis.min, simulator.elasticityForce, simulator.dragForce, simulator.externalForce)
            forceYAxis.max = Math.max(forceYAxis.max, simulator.elasticityForce, simulator.dragForce, simulator.externalForce)

            cspaceSeries.append(simulator.position, simulator.velocity)

            positionAxis.min = Math.min(positionAxis.min, simulator.position)
            positionAxis.max = Math.max(positionAxis.max, simulator.position)
            velocityAxis.min = Math.min(velocityAxis.min, simulator.velocity)
            velocityAxis.max = Math.max(velocityAxis.max, simulator.velocity)
        }
    }


}
