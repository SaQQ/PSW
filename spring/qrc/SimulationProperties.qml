import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GridLayout {
    id: root

    width: 220

    property real integrationStep: Number(integrationStepTextField.text)
    property real initialPosition: Number(initialPositionTextField.text)
    property real initialVelocity: Number(initialVelocityTextField.text)
    property real mass: Number(massTextField.text)
    property real elasticity: Number(elasticityTextField.text)
    property real drag: Number(dragTextField.text)

    property alias externalForceTextField: externalForceTextField
    property alias baseOffsetTextField: baseOffsetTextField

    property bool enabled: true
    property bool externalForceFormulaValid: true
    property bool baseOffsetFormulaValid: true

    columns: 1

    property real descWidth: 70

    // Time step

    property real integrationStepDecimals: 4
    property real integrationStepMin: 0.0001
    property real integrationStepMax: 1.0
    property real integrationStepDefault: 0.001

    Text {
        text: "Integration step"
        color: "#f5f5f5"
        font.bold: true
    }

    TextField {
        id: integrationStepTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.integrationStepDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.integrationStepMin
            top: root.integrationStepMax
            decimals: root.integrationStepDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    // Initial position

    property real initialPositionDecimals: 3
    property real initialPositionMin: -10
    property real initialPositionMax: 10
    property real initialPositionDefault: 1

    Text {
        text: "Initial position"
        color: "#f5f5f5"
        font.bold: true
        
    }

    TextField {
        id: initialPositionTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.initialPositionDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.initialPositionMin
            top: root.initialPositionMax
            decimals: root.initialPositionDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    // Initial velocity

    property real initialVelocityDecimals: 3
    property real initialVelocityMin: -10
    property real initialVelocityMax: 10
    property real initialVelocityDefault: 0

    Text {
        text: "Initial velocity"
        color: "#f5f5f5"
        font.bold: true
        
    }

    TextField {
        id: initialVelocityTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.initialVelocityDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.initialVelocityMin
            top: root.initialVelocityMax
            decimals: root.initialVelocityDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    // Mass

    property real massDecimals: 3
    property real massMin: 0.001
    property real massMax: 10
    property real massDefault: 1

    Text {
        text: "Mass"
        color: "#f5f5f5"
        font.bold: true
        
    }

    TextField {
        id: massTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.massDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.massMin
            top: root.massMax
            decimals: root.massDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    // Elasticity

    property real elasticityDecimals: 3
    property real elasticityMin: 0
    property real elasticityMax: 10
    property real elasticityDefault: 1

    Text {
        text: "Elasticity"
        color: "#f5f5f5"
        font.bold: true
        
    }

    TextField {
        id: elasticityTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.elasticityDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.elasticityMin
            top: root.elasticityMax
            decimals: root.elasticityDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    // Drag

    property real dragDecimals: 4
    property real dragMin: 0
    property real dragMax: 10
    property real dragDefault: 1

    Text {
        text: "Drag"
        color: "#f5f5f5"
        font.bold: true
        
    }

    TextField {
        id: dragTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.dragDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.dragMin
            top: root.dragMax
            decimals: root.dragDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    // External force

    Text {
        text: "External force formula"
        color: "#f5f5f5"
        font.bold: true
        
    }

    TextField {
        id: externalForceTextField
        
        Layout.fillWidth: true
        selectByMouse: true
        text: "0.0"
        placeholderText: qsTr("Enter mathematics")
        enabled: root.enabled
        background: Rectangle {
            border {
                width: 1
                color: root.externalForceFormulaValid ? "#212121" : "#F44336"
            }
        }
    }

    // Base offset

    Text {
        text: "Base offset formula"
        color: "#f5f5f5"
        font.bold: true
        
    }

    TextField {
        id: baseOffsetTextField
        
        Layout.fillWidth: true
        selectByMouse: true
        text: "0.0"
        placeholderText: qsTr("Enter mathematics")
        enabled: root.enabled
        background: Rectangle {
            border {
                width: 1
                color: root.baseOffsetFormulaValid ? "#212121" : "#F44336"
            }
        }
    }

}
