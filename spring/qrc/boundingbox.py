with open("spring.obj") as f:
    content = f.readlines()

content = [[word for word in x.strip().split(' ') if word] for x in content]

vertices = [(float(vertex_line[1]), float(vertex_line[2]), float(vertex_line[3])) for vertex_line in [line for line in content if len(line) == 4 and line[0] == "v"]]

print("Max X:", max(vertices, key=lambda v: v[0])[0])
print("Max Y:", max(vertices, key=lambda v: v[1])[1])
print("Max Z:", max(vertices, key=lambda v: v[2])[2])

print("Min X:", min(vertices, key=lambda v: v[0])[0])
print("Min Y:", min(vertices, key=lambda v: v[1])[1])
print("Min Z:", min(vertices, key=lambda v: v[2])[2])