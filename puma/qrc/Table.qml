import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Entity {
    id: table

    property real thickness: 1.0
    property real size: 100.0

    Entity {
        components: [ tableMesh, tableMaterial ]
    }

    CuboidMesh {
        id: tableMesh
        xExtent: table.size
        yExtent: table.thickness
        zExtent: table.size
    }

    PhongMaterial {
        id: tableMaterial
    }

}