import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Entity {
    id: puma

    property real a1: 0.0
    property real l1: 2.0
    property real a2: 0.0
    property real q2: 2.0
    property real a3: 0.0
    property real l3: 1.0
    property real a4: 0.0
    property real l4: 1.0
    property real a5: 0.0

    readonly property real armRadii: l1 / 15.0
    readonly property real jointRadii: 1.5 * armRadii
    readonly property real jointLength: 2 * jointRadii

    property bool framesVisible: true

    Entity {
        id: base
        components: [ baseMesh, baseMaterial ]

        Entity {
            id: firstFrame

            Tripod { enabled: puma.framesVisible }

            Entity {
                id: firstArm
                components: [ armMesh, armMaterial, firstArmTransform ]
            }

            Entity {
                id: firstJoint
                components: [jointMesh, jointMaterial, firstJointTransform ]
            }

            Entity {
                id: secondFrame

                Tripod { enabled: puma.framesVisible }

                Entity {
                    id: secondArm
                    components: [ armMesh, armMaterial, secondArmTransform ]
                }

                Entity {
                    id: secondJoint
                    components: [jointMesh, jointMaterial, secondJointTransform ]
                }

                Entity {
                    id: thirdFrame

                    Tripod { enabled: puma.framesVisible }

                    Entity {
                        id: thirdArm
                        components: [ armMesh, armMaterial, thirdArmTransform ]
                    }

                    Entity {
                        id: thirdJoint
                        components: [ jointMesh, jointMaterial, thirdJointTransform ]
                    }

                    Entity {
                        id: fourthFrame

                        Tripod { enabled: puma.framesVisible }

                        Entity {
                            id: fourthArm
                            components: [ armMesh, armMaterial, fourthArmTransform ]
                        }

                        Entity {
                            id: fifthFrame

                            Tripod { }

                            Entity {
                                id: effector
                                components: [ effectorMesh, effectorMaterial ]
                            }

                            components: [fifthFrameTransform]
                        }

                        components: [fourthFrameTransform]
                    }

                    components: [thridFrameTransform]
                }

                components: [secondFrameTransform]
            }

            components: [firstFrameTransform]

        }

    }



    Transform {
        id: firstArmTransform
        scale3D: Qt.vector3d(1, puma.l1, 1)
        translation: Qt.vector3d(0, puma.l1 / 2, 0)
    }

    Transform {
        id: firstJointTransform
        translation: Qt.vector3d(0, puma.l1, 0)
        rotationX: 90.0
    }

    Transform {
        id: firstFrameTransform
        rotationY: puma.a1
    }

    Transform {
        id: secondArmTransform
        scale3D: Qt.vector3d(1, puma.q2, 1)
        translation: Qt.vector3d(puma.q2 / 2, 0, 0)
        rotationZ: 90.0
    }

    Transform {
        id: secondJointTransform
        translation: Qt.vector3d(puma.q2, 0, 0)
        rotationX: 90.0
    }

    Transform {
        id: secondFrameTransform
        translation: Qt.vector3d(0, puma.l1, 0)
        rotationZ: puma.a2
    }

    Transform {
        id: thirdArmTransform
        scale3D: Qt.vector3d(1, puma.l3, 1)
        translation: Qt.vector3d(0, -puma.l3 / 2, 0)
    }

    Transform {
        id: thirdJointTransform
        translation: Qt.vector3d(0, -puma.l3, 0)
    }

    Transform {
        id: thridFrameTransform
        translation: Qt.vector3d(puma.q2, 0, 0)
        rotationZ: puma.a3
    }

    Transform {
        id: fourthArmTransform
        scale3D: Qt.vector3d(1, puma.l4, 1)
        translation: Qt.vector3d(puma.l4 / 2, 0, 0)
        rotationZ: 90.0
    }

    Transform {
        id: fourthFrameTransform
        translation: Qt.vector3d(0, -puma.l3, 0)
        rotationY: puma.a4
    }

    Transform {
        id: fifthFrameTransform
        translation: Qt.vector3d(puma.l4, 0, 0)
        rotationX: puma.a5
    }

    CylinderMesh {
        id: jointMesh
        radius: puma.jointRadii
        length: puma.jointLength
    }

    PhongMaterial {
        id: jointMaterial
        ambient: "red"
    }

    CylinderMesh {
        id: baseMesh
        length: 0.3
    }

    PhongMaterial {
        id: baseMaterial
        ambient: "lightgray"
    }

    CylinderMesh {
        id: armMesh
        radius: puma.armRadii
        length: 1
    }

    PhongMaterial {
        id: armMaterial
        ambient: "white"
    }

    CuboidMesh {
        id: effectorMesh
        xExtent: puma.jointLength
        yExtent: puma.jointLength
        zExtent: puma.jointLength
    }

    PhongMaterial {
        id: effectorMaterial
        ambient: "red"
    }



}