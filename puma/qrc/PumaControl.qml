import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "controls"

GridLayout {
    id: root

    columns: 2

    property alias a1: a1slider.value
    property alias a2: a2slider.value
    property alias q2: q2slider.value
    property alias a3: a3slider.value
    property alias a4: a4slider.value
    property alias a5: a5slider.value

    Text {
        text: qsTr("a1")
        color: "white"
    }

    Slider {
        id: a1slider
        Layout.fillWidth: true
        from: -180.0
        to: 180.0
    }

    Text {
        text: qsTr("a2")
        color: "white"
    }

    Slider {
        id: a2slider
        Layout.fillWidth: true
        from: -180.0
        to: 180.0
    }

    Text {
        text: qsTr("q2")
        color: "white"
    }

    Slider {
        id: q2slider
        Layout.fillWidth: true
        from: 0.0
        to: 10.0
        value: 2.0
    }

    Text {
        text: qsTr("a3")
        color: "white"
    }

    Slider {
        id: a3slider
        Layout.fillWidth: true
        from: -180.0
        to: 180.0
    }

    Text {
        text: qsTr("a4")
        color: "white"
    }

    Slider {
        id: a4slider
        Layout.fillWidth: true
        from: -180.0
        to: 180.0
    }

    Text {
        text: qsTr("a5")
        color: "white"
    }

    Slider {
        id: a5slider
        Layout.fillWidth: true
        from: -180.0
        to: 180.0
    }

}