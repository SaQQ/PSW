import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import "controls"

import PSW 1.0 as PSW

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: "PUMA"

    width: 1200
    height: 800

    PSW.Simulator {
        id: simulator

        speed: simulationControl.speed

        startPosition: eulerSettings.startPos
        endPosition: eulerSettings.endPos
        startEulerAngles: eulerSettings.startAngles
        endEulerAngles: eulerSettings.endAngles
        startQuaternion: quaternionSettings.startQuaternion
        endQuaternion: quaternionSettings.endQuaternion

        useSlerp: quaternionSettings.mode === "slerp"

        Component.onCompleted: {
            simulator.start()
        }

        onStep: {
            pumaQuaternionSolver.solve(currentPosition, currentQuaternion, !firstFrame)
            pumaStartSolver.solve(startPosition, startQuaternion, false)
            pumaEndSolver.solve(endPosition, endQuaternion, false)
        }
    }

    PSW.PumaSolver {
        id: pumaStartSolver
    }

    PSW.PumaSolver {
        id: pumaEndSolver
    }

    PSW.PumaSolver {
        id: pumaQuaternionSolver
    }

    footer: ToolBar {
        height: 24
        Row {
            anchors.fill: parent
            anchors.margins: 4

            Label {
                text: footerText()

                function footerText() {
                    if(simulator.running) {
                        if(simulator.paused) {
                            return qsTr("Running (Paused)")
                        } else {
                            return qsTr("Running")
                        }
                    } else {
                        return qsTr("Idle")
                    }
                }

            }
        }
    }

    Rectangle {
        id: background

        color: "#424242"

        anchors.fill: parent

        Item {
            id: bottomPaneContainer

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            height: 150

            Item {
                id: eulerItem

                anchors {
                    left: parent.left
                    top: parent.top
                    bottom: parent.bottom
                }

                width: parent.width / 2

                SectionPlate {
                    anchors.fill: parent
                    anchors.margins: 5
                    anchors.topMargin: 0
                    anchors.rightMargin: 2

                    header: "Euler angles"

                    EulerSettings {
                        id: eulerSettings
                        anchors.fill: parent

                        onStartPositionEdited: quaternionSettings.setStartPosition(startPos)
                        onEndPositionEdited: quaternionSettings.setEndPosition(endPos)

                        onStartAnglesEdited: quaternionSettings.setStartQuaternion(PSW.QuaternionUtils.fromEulerAngles(startAngles))
                        onEndAnglesEdited: quaternionSettings.setEndQuaternion(PSW.QuaternionUtils.fromEulerAngles(endAngles))
                    }

                }
            }

            Item {
                anchors {
                    left: eulerItem.right
                    right: parent.right
                    top: parent.top
                    bottom: parent.bottom
                }

                SectionPlate {
                    anchors.fill: parent
                    anchors.margins: 5
                    anchors.topMargin: 0
                    anchors.leftMargin: 2

                    header: "Quaternions"

                    QuaternionSettings {
                        id: quaternionSettings
                        anchors.fill: parent

                        onStartPositionEdited: eulerSettings.setStartPosition(startPos)
                        onEndPositionEdited: eulerSettings.setEndPosition(endPos)

                        onStartQuaternionEdited: eulerSettings.setStartAngles(PSW.QuaternionUtils.toEulerAngles(startQuaternion))
                        onEndQuaternionEdited: eulerSettings.setEndAngles(PSW.QuaternionUtils.toEulerAngles(endQuaternion))
                    }
                }
            }

        }

        Item {
            id: centerPaneContainer

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                bottom: bottomPaneContainer.top
            }

            SectionPlate {
                anchors.fill: parent
                anchors.margins: 5

                header: "Visualization"

                Scene3D {
                    id: scene3d
                    anchors.fill: parent
                    anchors.margins: 0
                    focus: true
                    aspects: ["input", "logic", "render"]
                    cameraAspectRatioMode: Scene3D.UserAspectRatio

                    Entity {
                        id: rootNode
                        components: [frameGraph, inputSettings]

                        RenderSettings {
                            id: frameGraph

                            activeFrameGraph: RenderSurfaceSelector {
                                id: surfaceSelector

                                Viewport {
                                    id: mainViewport
                                    normalizedRect: Qt.rect(0, 0, 1, 1)

                                    ClearBuffers {
                                        buffers: ClearBuffers.ColorDepthBuffer
                                        clearColor: "gray"

                                        LayerFilter {
                                            layers: []
                                        }
                                    }

                                    Viewport {
                                        id: topLeftViewport
                                        normalizedRect: Qt.rect(0, 0, 0.5, 1)

                                        CameraSelector {
                                            camera: camera

                                            LayerFilter {
                                                layers: [ l1 ]
                                            }
                                        }
                                    }

                                    Viewport {
                                        id: topRightViewport
                                        normalizedRect: Qt.rect(0.5, 0, 0.5, 1)

                                        CameraSelector {
                                            camera: camera

                                            LayerFilter {
                                                layers: [ l2 ]
                                            }
                                        }
                                    }

                                }
                            }
                        }

                        Layer { id: l1; recursive: true }
                        Layer { id: l2; recursive: true }

                        // Event Source will be set by the Qt3DQuickWindow
                        InputSettings { id: inputSettings }

                        Entity {
                            id: cameraSet

                            Camera {
                                id: camera
                                projectionType: CameraLens.PerspectiveProjection
                                fieldOfView: 45
                                aspectRatio: scene3d.width / 2 / scene3d.height
                                nearPlane : 0.1
                                farPlane : 1000.0
                                position: Qt.vector3d( 1.0, 11.0, 11.0 )
                                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
                            }

                            OrbitCameraController {
                                camera: camera
                                linearSpeed: 10
                            }

                        }

                        Entity {
                            id: sceneRoot

                            Tripod {
                                id: startTripod

                                Transform {
                                    id: startTransform
                                    translation: simulator.startPosition
                                    rotationX: simulator.startEulerAngles.x
                                    rotationY: simulator.startEulerAngles.y
                                    rotationZ: simulator.startEulerAngles.z
                                }

                                components: [
                                    l1, l2,
                                    startTransform
                                ]
                            }

                            Tripod {
                                id: endTripod

                                Transform {
                                    id: endTransform
                                    translation: simulator.endPosition
                                    rotationX: simulator.endEulerAngles.x
                                    rotationY: simulator.endEulerAngles.y
                                    rotationZ: simulator.endEulerAngles.z
                                }

                                components: [
                                    l1, l2,
                                    endTransform
                                ]
                            }

                            Tripod {
                                id: currentEulerAnglesTripod
                                enabled: false

                                Transform {
                                    id: currentEulerAnglesTransform
                                    translation: simulator.currentPosition
                                    rotationX: simulator.currentEulerAngles.x
                                    rotationY: simulator.currentEulerAngles.y
                                    rotationZ: simulator.currentEulerAngles.z
                                }

                                components: [ l1, currentEulerAnglesTransform ]
                            }

                            Tripod {
                                id: currentQuaternionTripod
                                enabled: false

                                Transform {
                                    id: currentQuaternionTransform
                                    translation: simulator.currentPosition
                                    rotation: simulator.currentQuaternion
                                }

                                components: [ l2, currentQuaternionTransform ]
                            }

                            Puma {
                                id: eulerPuma
                                components: [l1]

                                framesVisible: false

                                a1: PSW.RotationUtils.interpolateDegreesAngle(pumaStartSolver.a1, pumaEndSolver.a1, simulator.interpFactor)
                                a2: PSW.RotationUtils.interpolateDegreesAngle(pumaStartSolver.a2, pumaEndSolver.a2, simulator.interpFactor)
                                q2: pumaStartSolver.q2 * (1.0 - simulator.interpFactor) + pumaEndSolver.q2 * simulator.interpFactor
                                a3: PSW.RotationUtils.interpolateDegreesAngle(pumaStartSolver.a3, pumaEndSolver.a3, simulator.interpFactor)
                                a4: PSW.RotationUtils.interpolateDegreesAngle(pumaStartSolver.a4, pumaEndSolver.a4, simulator.interpFactor)
                                a5: PSW.RotationUtils.interpolateDegreesAngle(pumaStartSolver.a5, pumaEndSolver.a5, simulator.interpFactor)

                                l1: pumaStartSolver.l1
                                l3: pumaStartSolver.l3
                                l4: pumaStartSolver.l4
                            } // eulerPuma

                            Puma {
                                id: quaternionPuma
                                components: [l2]

                                framesVisible: false

                                a1: pumaQuaternionSolver.a1
                                a2: pumaQuaternionSolver.a2
                                q2: pumaQuaternionSolver.q2
                                a3: pumaQuaternionSolver.a3
                                a4: pumaQuaternionSolver.a4
                                a5: pumaQuaternionSolver.a5

                                l1: pumaQuaternionSolver.l1
                                l3: pumaQuaternionSolver.l3
                                l4: pumaQuaternionSolver.l4
                            } // quaternionPuma

                            Table {
                                id: table
                                components: [l1, l2]
                                size: 5
                                thickness: 0.1
                            }

                        } // sceneRoot
                    } // rootNode

                }

                Rectangle {

                    radius: 3

                    color: "#616161"

                    anchors {
                        top: parent.top
                        left: parent.left
                        margins: 5
                    }

                    width: 300
                    height: 56

                    SimulationControl {
                        id: simulationControl
                        anchors.fill: parent
                        anchors.margins: 5

                        time: simulator.time
                        interpFactor: simulator.interpFactor
                    }

                }

            }
        }

    }

}
