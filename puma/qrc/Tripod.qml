import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

Entity {
    id: tripod

    property real length: 2

    Vector {
        id: xVector
        coordinates: Qt.vector3d(tripod.length, 0, 0)
        color: "red"
    }

    Vector {
        id: yVector
        coordinates: Qt.vector3d(0, tripod.length, 0)
        color: "green"
    }

    Vector {
        id: zVector
        coordinates: Qt.vector3d(0, 0, tripod.length)
        color: "blue"
    }

}