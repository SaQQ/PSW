import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "controls"

import PSW 1.0

GridLayout {
    id: root

    property vector3d startPos: Qt.vector3d(0, 0, 0)
    property vector3d endPos: Qt.vector3d(0, 0, 0)

    property quaternion startQuaternion: Qt.quaternion(1, 0, 0, 0)
    property quaternion endQuaternion: Qt.quaternion(1, 0, 0, 0)

    readonly property string mode: modeComboBox.currentText

    signal startPositionEdited()
    signal endPositionEdited()

    signal startQuaternionEdited()
    signal endQuaternionEdited()

    function setStartPosition(pos) {
        startXTextField.text = pos.x.toFixed(startXTextField.decimals)
        startYTextField.text = pos.y.toFixed(startYTextField.decimals)
        startZTextField.text = pos.z.toFixed(startZTextField.decimals)
        root.startPos = pos
    }

    function setEndPosition(pos) {
        endXTextField.text = pos.x.toFixed(endXTextField.decimals)
        endYTextField.text = pos.y.toFixed(endYTextField.decimals)
        endZTextField.text = pos.z.toFixed(endZTextField.decimals)
        root.endPos = pos
    }

    function setStartQuaternion(q) {
        startQuaternionWTextField.text = q.scalar.toFixed(startQuaternionWTextField.decimals)
        startQuaternionXTextField.text = q.x.toFixed(startQuaternionXTextField.decimals)
        startQuaternionYTextField.text = q.y.toFixed(startQuaternionYTextField.decimals)
        startQuaternionZTextField.text = q.z.toFixed(startQuaternionZTextField.decimals)
        startQuaternion = q
    }

    function setEndQuaternion(q) {
        endQuaternionWTextField.text = q.scalar.toFixed(endQuaternionWTextField.decimals)
        endQuaternionXTextField.text = q.x.toFixed(endQuaternionXTextField.decimals)
        endQuaternionYTextField.text = q.y.toFixed(endQuaternionYTextField.decimals)
        endQuaternionZTextField.text = q.z.toFixed(endQuaternionZTextField.decimals)
        endQuaternion = q
    }

    property real maxCoord: 10000.0
    property real minCoord: -10000.0
    property int coordDecimals: 2

    property real minQuaternionCoord: -1.0
    property real maxQuaternionCoord: 1.0
    property int quaternionCoordDecimals: 3

    columns: 8

    Text {

    }

    Text {
        text: "X"
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
        font.pointSize: 8
    }

    Text {
        text: "Y"
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
        font.pointSize: 8
    }

    Text {
        text: "Z"
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
        font.pointSize: 8
    }

    Text {
        text: qsTr("Quat. W")
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
        font.pointSize: 8
    }

    Text {
        text: qsTr("Quat. X")
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
        font.pointSize: 8
    }

    Text {
        text: qsTr("Quat. Y")
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
        font.pointSize: 8
    }

    Text {
        text: qsTr("Quat. Z")
        color: "#f5f5f5"
        Layout.alignment: Qt.AlignHCenter
        font.pointSize: 8
    }

    Text {
        text: qsTr("Start")
        color: "#f5f5f5"
        font.pointSize: 8
    }

    DoubleTextField {
        id: startXTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeStartPos()
    }

    DoubleTextField {
        id: startYTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeStartPos()
    }

    DoubleTextField {
        id: startZTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeStartPos()
    }

    DoubleTextField {
        id: startQuaternionWTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeStartQuaternion()
    }

    DoubleTextField {
        id: startQuaternionXTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeStartQuaternion()
    }

    DoubleTextField {
        id: startQuaternionYTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeStartQuaternion()
    }

    DoubleTextField {
        id: startQuaternionZTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeStartQuaternion()
    }

    Text {
        text: qsTr("End")
        color: "#f5f5f5"
        font.pointSize: 8
    }

    DoubleTextField {
        id: endXTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeEndPos()
    }

    DoubleTextField {
        id: endYTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeEndPos()
    }

    DoubleTextField {
        id: endZTextField
        Layout.fillWidth: true
        minValue: root.minCoord
        maxValue: root.maxCoord
        decimals: root.coordDecimals
        onEditingFinished: root.normalizeEndPos()
    }

    DoubleTextField {
        id: endQuaternionWTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeEndQuaternion()
    }

    DoubleTextField {
        id: endQuaternionXTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeEndQuaternion()
    }

    DoubleTextField {
        id: endQuaternionYTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeEndQuaternion()
    }

    DoubleTextField {
        id: endQuaternionZTextField
        Layout.fillWidth: true
        minValue: root.minQuaternionCoord
        maxValue: root.maxQuaternionCoord
        decimals: root.quaternionCoordDecimals
        onEditingFinished: root.normalizeEndQuaternion()
    }

    Text {
        color: "#f5f5f5"
        Layout.columnSpan: 6
//        text: "Interpolation: " + "[" + vecToStr(root.startPos) + " " + quatToStr(root.startQuaternion) + "]" + " → " + "[" + vecToStr(root.endPos) + " " + quatToStr(root.endQuaternion) + "]"
        font.pointSize: 8

        function vecToStr(vec) {
            return "[" + vec.x.toFixed(1) + ", " + vec.y.toFixed(1) + ", " + vec.z.toFixed(1) + "]"
        }

        function quatToStr(q) {
            return "[" + q.scalar.toFixed(1) + ", (" + q.x.toFixed(1) + ", " + q.y.toFixed(1) + ", " + q.z.toFixed(1) + ")]"
        }
    }

    ComboBox {
        id: modeComboBox
        model: ["lerp", "slerp"]
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.columnSpan: 2
        font.pointSize: 8
    }

    function normalizeStartPos() {
        var pos = Qt.vector3d(
            startXTextField.value,
            startYTextField.value,
            startZTextField.value
        )
        root.setStartPosition(pos)
        root.startPositionEdited()
    }

    function normalizeEndPos() {
        var pos = Qt.vector3d(
            endXTextField.value,
            endYTextField.value,
            endZTextField.value
        )
        root.setEndPosition(pos)
        root.endPositionEdited()
    }

    function normalizeStartQuaternion() {
        var q = QuaternionUtils.normalizedQuaternion(Qt.quaternion(
            startQuaternionWTextField.value,
            startQuaternionXTextField.value,
            startQuaternionYTextField.value,
            startQuaternionZTextField.value
        ))
        root.setStartQuaternion(q)
        root.startQuaternionEdited()
    }

    function normalizeEndQuaternion() {
        var q = QuaternionUtils.normalizedQuaternion(Qt.quaternion(
            endQuaternionWTextField.value,
            endQuaternionXTextField.value,
            endQuaternionYTextField.value,
            endQuaternionZTextField.value
        ))
        root.setEndQuaternion(q)
        root.endQuaternionEdited()
    }

}