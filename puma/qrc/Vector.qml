
import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import PSW 1.0

Entity {
    id: vector

    property color color: "#32BC25"

    property vector3d coordinates: Qt.vector3d(0, 10, 0)
    readonly property real length: coordinates.length()

    Transform {
        id: rotationTransform
        rotation: QuaternionUtils.fromToQuaternion(Qt.vector3d(0, 1, 0), vector.coordinates)
    }

    PhongMaterial {
        id: material
        ambient: vector.color
    }

    CylinderMesh {
        id: shaftMesh
        length: 10
        radius: 10
    }

    ConeMesh {
        id: tipMesh
        length: 0.2 * vector.length
        topRadius: 0.0
        bottomRadius: 0.05 * vector.length
    }

    readonly property real shaftLength: 0.8 * length
    readonly property real shaftRadius: 0.02 * length
    readonly property real tipLength: 0.2 * length
    readonly property real tipRadius: 0.05 * length

    Transform {
        id: shaftTransform
        scale3D: Qt.vector3d(vector.shaftRadius / shaftMesh.radius, vector.shaftLength / shaftMesh.length, vector.shaftRadius / shaftMesh.radius)
        translation: Qt.vector3d(0, vector.shaftLength / 2.0, 0)
    }

    Transform {
        id: tipTransform
        scale3D: Qt.vector3d(1, 1, 1)
        translation: Qt.vector3d(0, vector.shaftLength + tipMesh.length / 2.0, 0)
    }

    Entity {
        id: arrow

        Entity {
            id: shaft
            components: [ shaftMesh, material, shaftTransform ].concat(vector.layers)
        }

        Entity {
            id: tip
            components: [ tipMesh, material, tipTransform ].concat(vector.layers)
        }

        components: [ rotationTransform ]
    }

}