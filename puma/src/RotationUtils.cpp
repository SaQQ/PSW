#include "RotationUtils.hpp"

#include <cmath>

RotationUtils::RotationUtils(QObject *parent) : QObject(parent)
{

}

qreal RotationUtils::radiansDistance(qreal from, qreal to) const {
//    return std::fmod(to - from + M_PI, 2 * M_PI) - M_PI;
    return std::fmod((std::fmod((to - from), 2 * M_PI) + 3 * M_PI), 2 * M_PI) - M_PI;
}

qreal RotationUtils::degreesDistance(qreal from, qreal to) const {
    return radiansToDegrees(radiansDistance(degreesToRadians(from), degreesToRadians(to)));
}

qreal RotationUtils::interpolateRadiansAngle(qreal startAngle, qreal endAngle, float interpFactor) const {
    return startAngle + interpFactor * radiansDistance(startAngle, endAngle);
}

qreal RotationUtils::interpolateDegreesAngle(qreal startAngle, qreal endAngle, float interpFactor) const {
    return radiansToDegrees(interpolateRadiansAngle(degreesToRadians(startAngle), degreesToRadians(endAngle), interpFactor));
}

QObject *RotationUtilsSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return new RotationUtils();
}

