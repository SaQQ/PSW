// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#include <QDebug>

#include "Simulator.hpp"
#include "RotationUtils.hpp"

Simulator::Simulator(QObject *parent) : QAbstractListModel(parent),
                                        speed{1.0},
                                        running{false}, paused{false} {

    timer = new QTimer{this};
    timer->setInterval(30);
    elapsedTimer = new QElapsedTimer;
    leftover = 0;
    QObject::connect(timer, &QTimer::timeout, this, &Simulator::tick);

    reset();
}

bool Simulator::start() {
    if(running)
        return false;

	currentState.time = 0.0;
	currentState.interpFactor = 0.0;

    emit currentStateChanged();

    timer->start();
    elapsedTimer->start();
    leftover = 0;

    running = true;
    emit runningChanged();
    return true;
}

bool Simulator::togglePause() {
    if(!running)
        return false;

    paused = !paused;

    if(paused)
        timer->stop();
    else
    {
        timer->start();
        elapsedTimer->restart();
        leftover = 0;
    }

    emit pausedChanged();
    return true;
}

bool Simulator::reset() {
    running = paused = false;
    emit runningChanged();
    emit pausedChanged();

    timer->stop();

	currentState.time = 0.0;
	currentState.interpFactor = 0.0;

    emit currentStateChanged();

    return true;
}

void Simulator::tick() {
    if(!running || paused)
        return;

    auto elapsedMsec = elapsedTimer->elapsed() + leftover;

    elapsedTimer->restart();

    leftover = elapsedMsec % timer->interval();

    bool firstFrame = false;

    for(int i = 0; i < elapsedMsec / timer->interval(); ++i)
    {
        currentState.time += (timer->interval() / 1000.0);
        currentState.interpFactor += (timer->interval() / 1000.0 * speed);
        if (currentState.interpFactor > 1.0) {
            currentState.interpFactor -= 1.0;
            firstFrame = true;
        }
    }

    emit currentStateChanged();
    emit step(firstFrame);
}

QVector3D Simulator::getPosition(double t) const {
    return startPosition * (1 - t) + endPosition * t;
}

QVector3D Simulator::getCurrentPosition() const {
    return getPosition(currentState.interpFactor);
}

QQuaternion Simulator::getQuaternion(double t) const {
    if (useSlerp) {
        const auto omega = std::acos(std::clamp(QQuaternion::dotProduct(startQuaternion, endQuaternion), -1.0f, 1.0f));
        if (std::abs(std::sin(omega)) < 0.00001f)
            return (startQuaternion + endQuaternion) / 2.0;
        return std::sin((1.0 - t) * omega) / std::sin(omega) * startQuaternion + std::sin(t * omega) / std::sin(omega) * endQuaternion;
    } else {
        return (startQuaternion * (1.0 - t) + endQuaternion * t).normalized();
    }
}

QQuaternion Simulator::getCurrentQuaternion() const {
    return getQuaternion(currentState.interpFactor);
}

QVector3D Simulator::getEulerAngles(double t) const {
    RotationUtils utils;
    return QVector3D(
            utils.interpolateDegreesAngle(startEulerAngles.x(), endEulerAngles.x(), t),
            utils.interpolateDegreesAngle(startEulerAngles.y(), endEulerAngles.y(), t),
            utils.interpolateDegreesAngle(startEulerAngles.z(), endEulerAngles.z(), t)
    );
}

QVector3D Simulator::getCurrentEulerAngles() const {
    return getEulerAngles(currentState.interpFactor);
}

QHash<int, QByteArray> Simulator::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[PositionRole] = "position";
    roles[EulerAnglesRole] = "eulerAngles";
    roles[QuaternionRole] = "quaternion";
    return roles;
}

QVariant Simulator::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto row = index.row();
    auto dt = 1.0 / (framesCount - 1);
    auto t = row * dt;

    switch(role) {
        case PositionRole:
            return QVariant(getPosition(t));
        case EulerAnglesRole:
            return QVariant(getEulerAngles(t));
        case QuaternionRole:
            return QVariant(getQuaternion(t));
        default:
            return QVariant();
    }

}


