#include "PumaSolver.hpp"

#include <cmath>

const qreal PumaSolver::DefaultL1 = 2.0;
const qreal PumaSolver::DefaultL3 = 1.0;
const qreal PumaSolver::DefaultL4 = 1.0;
const qreal PumaSolver::DefaultQ2 = 2.0;

PumaSolver::PumaSolver(QObject *parent)
        : QObject(parent),
          a1(0.0), a2(0.0), q2(DefaultQ2), a3(0.0), a4(0.0), a5(0.0),
          l1(DefaultL1), l3(DefaultL3), l4(DefaultL4)
{

}

float angle(const QVector3D &v, const QVector3D &w) {
//    auto nv = v.length();
//    auto nw = w.length();
//    auto ca =  / (nv * nw);
//    auto sa =  / (nv * nw);
    auto cross = QVector3D::crossProduct(v, w);
//    auto cross2 = QVector3D::crossProduct(v, cross);
    auto angle = std::atan2(cross.length(), QVector3D::dotProduct(v, w));
//    if (QVector3D::dotProduct(w, cross2) < 0)
//        angle = -angle;
    return angle;
//    auto a1 = std::asin(sa);
//    auto a2 = M_PI - a1;
//    auto dca1 = std::abs(std::cos(a1) - ca);
//    auto dca2 = std::abs(std::cos(a2) - ca);
//    return dca1 < dca2 ? a1 : a2;
}

float angle(const QVector3D &v, const QVector3D &w, const QVector3D &n024) {
//    auto nv = v.length();
//    auto nw = w.length();
//    auto ca =  / (nv * nw);
//    auto sa =  / (nv * nw);
    auto cross = QVector3D::crossProduct(v, w);
//    auto cross2 = QVector3D::crossProduct(v, cross);
    auto angle = std::atan2(cross.length(), QVector3D::dotProduct(v, w));
    if (QVector3D::dotProduct(cross, n024) < 0)
        angle = -angle;
    return angle;
//    auto a1 = std::asin(sa);
//    auto a2 = M_PI - a1;
//    auto dca1 = std::abs(std::cos(a1) - ca);
//    auto dca2 = std::abs(std::cos(a2) - ca);
//    return dca1 < dca2 ? a1 : a2;
}


inline float radiansToDegrees(float radians) {
    return static_cast<float>(radians / M_PI) * 180.0f;
}

bool PumaSolver::solve(const QVector3D &position, const QQuaternion &rotation, bool preserve) {
    QVector3D p0(0.0, 0.0, 0.0);
    QVector3D p1(p0);
    QVector3D p2 = p1 + l1 * QVector3D(0, 1, 0);
    QVector3D p5(position);
    QVector3D p4 = p5 - l4 * rotation.rotatedVector(QVector3D(1, 0, 0));
    QVector3D n024 = QVector3D::crossProduct(p4 - p0, p2 - p0);
    if (n024.length() < 0.0001f) {
        n024 = QVector3D(0, 0, 1);
    }
    n024 = n024.normalized();
    QVector3D z4 = QVector3D::crossProduct(rotation.rotatedVector(QVector3D(1, 0, 0)), n024).normalized();
    if (z4.length() < 0.0001f) {
        z4 = (p2 - p4).normalized();
    }
    QVector3D p3 = p4 + l3 * z4;
    QVector3D p3alt = p4 - l3 * z4;

    if (preserve) {
        if((p3alt - lastP3).lengthSquared() < (p3 - lastP3).lengthSquared())
            p3 = p3alt;
    } else {
        if ((p2 - p3alt).lengthSquared() < (p2 - p3).lengthSquared())
            p3 = p3alt;
    }

    lastP3 = p3;

    a1 = -radiansToDegrees(std::atan2(p4.z(), p4.x()));
    a2 = radiansToDegrees(angle(p2 - p0, p3 - p2, n024) + M_PI_2);
    q2 = (p3 - p2).length();
    a3 = radiansToDegrees(angle(p3 - p2, p4 - p3, n024) + M_PI_2);
    a4 = radiansToDegrees(angle(n024, rotation.rotatedVector(QVector3D(1, 0, 0)), p3 - p4) - M_PI_2);
    a5 = radiansToDegrees(angle(p3 - p4, rotation.rotatedVector(QVector3D(0, 1, 0)), rotation.rotatedVector(QVector3D(1, 0, 0))));
    valid = true;
    emit changed();

	return true;
}
