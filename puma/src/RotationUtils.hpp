#ifndef PSW_ROTATION_UTILS_HPP
#define PSW_ROTATION_UTILS_HPP

#include <cmath>

#include <QObject>

#include <QQmlEngine>
#include <QJSEngine>

class RotationUtils : public QObject {
    Q_OBJECT
public:
    explicit RotationUtils(QObject *parent = nullptr);

    Q_INVOKABLE qreal radiansToDegrees(qreal radians) const {
        return radians / M_PI * 180.0;
    }

    Q_INVOKABLE qreal degreesToRadians(qreal degrees) const {
        return degrees / 180.0 * M_PI;
    }

    Q_INVOKABLE qreal radiansDistance(qreal from, qreal to) const;

    Q_INVOKABLE qreal degreesDistance(qreal from, qreal to) const;

    Q_INVOKABLE qreal interpolateRadiansAngle(qreal startAngle, qreal endAngle, float interpFactor) const;

    Q_INVOKABLE qreal interpolateDegreesAngle(qreal startAngle, qreal endAngle, float interpFactor) const;

};

QObject *RotationUtilsSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine);

#endif //PSW_ROTATION_UTILS_HPP
