#ifndef PSW_PUMA_SOLVER_HPP
#define PSW_PUMA_SOLVER_HPP

#include <QVector3D>
#include <QQuaternion>
#include <QObject>

class PumaSolver : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool valid READ getValid NOTIFY changed)
    Q_PROPERTY(qreal a1 READ getA1 NOTIFY changed)
    Q_PROPERTY(qreal a2 READ getA2 NOTIFY changed)
    Q_PROPERTY(qreal q2 READ getQ2 NOTIFY changed)
    Q_PROPERTY(qreal a3 READ getA3 NOTIFY changed)
    Q_PROPERTY(qreal a4 READ getA4 NOTIFY changed)
    Q_PROPERTY(qreal a5 READ getA5 NOTIFY changed)

    Q_PROPERTY(qreal l1 READ getL1 WRITE setL1 NOTIFY constsChanged)
    Q_PROPERTY(qreal l3 READ getL3 WRITE setL3 NOTIFY constsChanged)
    Q_PROPERTY(qreal l4 READ getL4 WRITE setL4 NOTIFY constsChanged)
public:
    static const qreal DefaultL1, DefaultL3, DefaultL4;
    static const qreal DefaultQ2;

    explicit PumaSolver(QObject *parent = nullptr);

    Q_INVOKABLE bool solve(const QVector3D &position, const QQuaternion &rotation, bool preserve = true);

    bool getValid() const { return valid; }

    qreal getA1() const { return a1; }

    qreal getA2() const { return a2; }

    qreal getQ2() const { return q2; }

    qreal getA3() const { return a3; }

    qreal getA4() const { return a4; }

    qreal getA5() const { return a5; }

    qreal getL1() const { return l1; }

    void setL1(qreal value) {
        l1 = value;
        emit constsChanged();
    }

    qreal getL3() const { return l3; }

    void setL3(qreal value) {
        l3 = value;
        emit constsChanged();
    }

    qreal getL4() const { return l4; }

    void setL4(qreal value) {
        l4 = value;
        emit constsChanged();
    }

signals:
    void changed();
    void constsChanged();
    void unreachable();
private:
    bool valid;
    qreal a1, a2, q2, a3, a4, a5;
    qreal l1, l3, l4;
    QVector3D lastP3;
};

#endif //PSW_PUMA_SOLVER_HPP
