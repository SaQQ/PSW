// Created by Paweł Sobótka on 18.08.17.
// Rail-Mil Computers Sp. z o.o.

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QQmlContext>

#include "Simulator.hpp"
#include "PumaSolver.hpp"
#include "QQuaternionUtils.hpp"
#include "RotationUtils.hpp"

int main(int ac, char **av)
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(ac, av);

    QQmlApplicationEngine engine;

    qmlRegisterType<Simulator>("PSW", 1, 0, "Simulator");
    qmlRegisterType<PumaSolver>("PSW", 1, 0, "PumaSolver");
    qmlRegisterSingletonType<QQuaternionUtils>("PSW", 1, 0, "QuaternionUtils", QQuaternionUtilsSingletonProvider);
    qmlRegisterSingletonType<RotationUtils>("PSW", 1, 0, "RotationUtils", RotationUtilsSingletonProvider);

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
