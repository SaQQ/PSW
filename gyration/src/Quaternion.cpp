#include "Quaternion.hpp"

#include "Constants.hpp"
#include "Utilities.hpp"

namespace Math {

	Quaternion::Quaternion()
		: W{ 0.0f }, X{ 0.0f }, Y{ 0.0f }, Z{ 0.0f } {
	}

	Quaternion::Quaternion(double w, double x, double y, double z)
		: W{ w }, X{ x }, Y{ y }, Z{ z } {
	}

	Vector4d Quaternion::GetAxisAngle() const {
		double angle = 2.0f * std::acos(W);
		double len = std::sqrt(1.0f - W * W);
		if (len < Eps) {
			return Vector4d{ 1.0f, 0.0f, 0.0f, 0.0f };
		}
		else {
			len = 1.0f / len;
			return Vector4d{ X * len, Y * len, Z * len, angle };
		}
	}

	Vector3d Quaternion::GetEulerAngles() const {
		double q0 = W;
		double q1 = Z;
		double q2 = X;
		double q3 = Y;

		double sinVal = static_cast<double>(-2) * (q1 * q3 - q0 * q2);

		if (std::abs(sinVal - 1.0f) < Eps) {
			double pi = PI;
			return Vector3d{ -pi * 0.5f, std::atan2(q1 * q2 - q0 * q3, q1 * q3 + q0 * q2), 0.0f };
		}
		if (std::abs(sinVal - 1.0f) < Eps) {
			double pi = PI;
			return Vector3d{ pi * 0.5f, -std::atan2(q1 * q2 - q0 * q3, q1 * q3 + q0 * q2), 0.0f };
		}

		return Vector3d{
				std::asin(sinVal),
				std::atan2(q1 * q2 + q0 * q3, 0.5f - (q2 * q2 + q3 * q3)),
				std::atan2(q2 * q3 + q0 * q1, 0.5f - (q1 * q1 + q2 * q2))
		};
	}

	double Quaternion::GetLengthSquared() const {
		return this->W * this->W + this->X * this->X + this->Y * this->Y + this->Z * this->Z;
	}

	double Quaternion::GetLength() const {
		return std::sqrt(static_cast<double>(this->W * this->W + this->X * this->X + this->Y * this->Y + this->Z * this->Z));
	}

	Quaternion Quaternion::GetNormalized() const {
		Quaternion ret = *this;
		ret.Normalize();

		return ret;
	}

	void Quaternion::Normalize() {
		double length = GetLengthSquared();

		if (length < Eps) {
			this->W = static_cast<double>(1);
			this->X = static_cast<double>(0);
			this->Y = static_cast<double>(0);
			this->Z = static_cast<double>(0);
		}
		else {
			length = static_cast<double>(1) / std::sqrt(length);

			this->X *= length;
			this->Y *= length;
			this->Z *= length;
			this->W *= length;
		}
	}

	Quaternion Quaternion::CreateFromMatrix(const Matrix4d &matrix) {
		Quaternion q;

		double trace = matrix[0][0] + matrix[1][1] + matrix[2][2];
		if (trace > 0) {
			double s = 0.5f / std::sqrt(trace + 1.0f);
			q.W = 0.25f / s;
			q.X = (matrix[2][1] - matrix[1][2]) * s;
			q.Y = (matrix[0][2] - matrix[2][0]) * s;
			q.Z = (matrix[1][0] - matrix[0][1]) * s;
		}
		else {
			if (matrix[0][0] > matrix[1][1] && matrix[0][0] > matrix[2][2]) {
				double s = 2.0f * std::sqrt(1.0f + matrix[0][0] - matrix[1][1] - matrix[2][2]);
				q.W = (matrix[2][1] - matrix[1][2]) / s;
				q.X = 0.25f * s;
				q.Y = (matrix[0][1] + matrix[1][0]) / s;
				q.Z = (matrix[0][2] + matrix[2][0]) / s;
			}
			else if (matrix[1][1] > matrix[2][2]) {
				double s = 2.0f * std::sqrt(1.0f + matrix[1][1] - matrix[0][0] - matrix[2][2]);
				q.W = (matrix[0][2] - matrix[2][0]) / s;
				q.X = (matrix[0][1] + matrix[1][0]) / s;
				q.Y = 0.25f * s;
				q.Z = (matrix[1][2] + matrix[2][1]) / s;
			}
			else {
				double s = 2.0f * std::sqrt(1.0f + matrix[2][2] - matrix[0][0] - matrix[1][1]);
				q.W = (matrix[1][0] - matrix[0][1]) / s;
				q.X = (matrix[0][2] + matrix[2][0]) / s;
				q.Y = (matrix[1][2] + matrix[2][1]) / s;
				q.Z = 0.25f * s;
			}
		}

		return q;
	}

	Quaternion Quaternion::CreateFromAxisAngle(const Vector3d &axis, double angle) {
		return CreateFromAxisAngle(axis.X(), axis.Y(), axis.Z(), angle);
	}

	Quaternion Quaternion::CreateFromAxisAngle(double x, double y, double z, double angle) {
		angle *= static_cast<double>(0.5);

		double lenSqrt = x * x + y * y + z * z;
		if (lenSqrt != static_cast<double>(1)) {
			if (lenSqrt < Eps) {
				x = static_cast<double>(1);
				y = static_cast<double>(0);
				z = static_cast<double>(0);
			}
			else {
				double len = static_cast<double>(1) / std::sqrt(lenSqrt);
				x *= len;
				y *= len;
				z *= len;
			}
		}

		double s = std::sin(angle);

		return Quaternion{ std::cos(angle), s * x, s * y, s * z };
	}

	Quaternion Quaternion::CreateRotationZ(double angle) {
		angle *= static_cast<double>(0.5);

		return Quaternion{ std::cos(angle), static_cast<double>(0), static_cast<double>(0), std::sin(angle) };
	}

	Quaternion Quaternion::CreateRotationY(double angle) {
		angle *= static_cast<double>(0.5);

		return Quaternion{ std::cos(angle), static_cast<double>(0), std::sin(angle), static_cast<double>(0) };
	}

	Quaternion Quaternion::CreateRotationX(double angle) {
		angle *= static_cast<double>(0.5);

		return Quaternion{ std::cos(angle), std::sin(angle), static_cast<double>(0), static_cast<double>(0) };
	}

	Quaternion Quaternion::CreateFromEuler(double x, double y, double z) {
		x *= static_cast<double>(0.5);
		y *= static_cast<double>(0.5);
		z *= static_cast<double>(0.5);

		double cosX = std::cos(x);
		double sinX = std::sin(x);

		double cosY = std::cos(y);
		double sinY = std::sin(y);

		double cosZ = std::cos(z);
		double sinZ = std::sin(z);

		// Z -> X -> Y
		return Quaternion{
				cosY * cosX * cosZ + sinY * sinX * sinZ,
				cosY * sinX * cosZ + sinY * cosX * sinZ,
				sinY * cosX * cosZ - cosY * sinX * sinZ,
				cosY * cosX * sinZ - sinY * sinX * cosZ
		};
	}

	Quaternion Quaternion::CreateFromEuler(const Vector3d &angles) {
		return CreateFromEuler(angles.X(), angles.Y(), angles.Z());
	}

	const Quaternion Quaternion::Identity = Quaternion{ 1, 0, 0, 0 };

	Quaternion operator+(const Quaternion &q) {
		return q;
	}

	Quaternion operator-(const Quaternion &q) {
		Quaternion q2 = q.GetNormalized();
		return Quaternion{ q2.W, -q2.X, -q2.Y, -q2.Z };
	}

	Quaternion operator+(const Quaternion &q1, const Quaternion &q2) {
		return Quaternion{ q1.W + q2.W, q1.X + q2.X, q1.Y + q2.Y, q1.Z + q2.Z };
	}

	Quaternion operator-(const Quaternion &q1, const Quaternion &q2) {
		return Quaternion{ q1.W - q2.W, q1.X - q2.X, q1.Y - q2.Y, q1.Z - q2.Z };
	}

	Quaternion operator*(const Quaternion &q1, const Quaternion &q2) {
		return Quaternion{
				q1.W * q2.W - q1.X * q2.X - q1.Y * q2.Y - q1.Z * q2.Z,
				q1.W * q2.X + q1.X * q2.W + q1.Y * q2.Z - q1.Z * q2.Y,
				q1.W * q2.Y - q1.X * q2.Z + q1.Y * q2.W + q1.Z * q2.X,
				q1.W * q2.Z + q1.X * q2.Y - q1.Y * q2.X + q1.Z * q2.W
		};
	}

	Vector4d operator*(const Quaternion &q, const Vector4d &v) {
		double x2 = q.X * 2.0f;
		double y2 = q.Y * 2.0f;
		double z2 = q.Z * 2.0f;
		double xx = q.X * x2;
		double yy = q.Y * y2;
		double zz = q.Z * z2;
		double xy = q.X * y2;
		double xz = q.X * z2;
		double yz = q.Y * z2;
		double wx = q.W * x2;
		double wy = q.W * y2;
		double wz = q.W * z2;

		return Vector4d{
				(1.0f - (yy + zz)) * v.X() + (xy - wz) * v.Y() + (xz + wy) * v.Z(),
				(xy + wz) * v.X() + (1.0f - (xx + zz)) * v.Y() + (yz - wx) * v.Z(),
				(xz - wy) * v.X() + (yz + wx) * v.Y() + (1.0f - (xx + yy)) * v.Z(),
				v.W()
		};
	}

	Quaternion operator* (const Quaternion &q, double value) {
		return Quaternion(q.W * value, q.X * value, q.Y * value, q.Z * value);
	}

    Quaternion operator/(const Quaternion &q, double value) {
        return Quaternion(q.W / value, q.X / value, q.Y / value, q.Z / value);
    }

	Vector3d operator*(const Quaternion &q, const Vector3d &v) {
		double num = q.X * static_cast<double>(2);
		double num2 = q.Y * static_cast<double>(2);
		double num3 = q.Z * static_cast<double>(2);
		double num4 = q.X * num;
		double num5 = q.Y * num2;
		double num6 = q.Z * num3;
		double num7 = q.X * num2;
		double num8 = q.X * num3;
		double num9 = q.Y * num3;
		double num10 = q.W * num;
		double num11 = q.W * num2;
		double num12 = q.W * num3;

		return Vector3d{
				(1.0f - (num5 + num6)) * v.X() + (num7 - num12) * v.Y() + (num8 + num11) * v.Z(),
				(num7 + num12) * v.X() + (1.0f - (num4 + num6)) * v.Y() + (num9 - num10) * v.Z(),
				(num8 - num11) * v.X() + (num9 + num10) * v.Y() + (1.0f - (num4 + num5)) * v.Z()
		};
	}

	bool operator==(const Quaternion &lhs, const Quaternion &rhs) {
		//	return (q1.W == q2.W && q1.X == q2.X && q1.Y == q2.Y && q1.Z == q2.Z);
		return
			(EpsEqual(lhs.W, rhs.W) && EpsEqual(lhs.X, rhs.X) && EpsEqual(lhs.Y, rhs.Y) &&
				EpsEqual(lhs.Z, rhs.Z)) ||
				(EpsEqual(lhs.W, -rhs.W) && EpsEqual(lhs.X, -rhs.X) && EpsEqual(lhs.Y, -rhs.Y) &&
					EpsEqual(lhs.Z, -rhs.Z));
	}

	bool operator!=(const Quaternion &lhs, const Quaternion &rhs) {
		//	return (q1.W != q2.W || q1.X != q2.X || q1.Y != q2.Y || q1.Z != q2.Z);
		return
			(!EpsEqual(lhs.W, rhs.W) || !EpsEqual(lhs.X, rhs.X) ||
				!EpsEqual(lhs.Y, rhs.Y) || !EpsEqual(lhs.Z, rhs.Z)) &&
				(!EpsEqual(lhs.W, -rhs.W) || !EpsEqual(lhs.X, -rhs.X) ||
					!EpsEqual(lhs.Y, -rhs.Y) || !EpsEqual(lhs.Z, -rhs.Z));
	}

}