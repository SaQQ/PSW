#ifndef MATH_UTILITIES_HPP
#define MATH_UTILITIES_HPP

#include <limits>
#include <cmath>
#include <type_traits>
#include "Constants.hpp"

namespace Math 
{
    template<class T>
    typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type EpsEqual(T x, T y, int ulp = 6) 
	{
        // the machine epsilon has to be scaled to the magnitude of the values used
        // and multiplied by the desired precision in ULPs (units in the last place)
        return std::abs(x - y) < std::numeric_limits<T>::epsilon() * std::abs(x + y) * ulp
               // unless the result is subnormal
               || std::abs(x - y) < std::numeric_limits<T>::min();
    }

	template<class T>
	typename std::enable_if<std::numeric_limits<T>::is_integer, bool>::type EpsEqual(T x, T y, int ulp = 6)
	{
		return x == y;
	}

	constexpr float DegToRad(float degrees)
	{
		return degrees / 180.0f * PI;
	}

	constexpr float RadToDeg(float radians)
	{
		return radians * 180.0f / PI;
	}

	constexpr double DegToRad(double degrees)
	{
		return degrees / 180.0f * PI;
	}

	constexpr double RadToDeg(double radians)
	{
		return radians * 180.0f / PI;
	}
}

#endif //MATH_UTILITIES_HPP
