// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef PSW_SIMULATOR_HPP
#define PSW_SIMULATOR_HPP

#include <QObject>
#include <QString>
#include <QLineSeries>

#include <QTimer>
#include <QElapsedTimer>

#include <QQuaternion>

#include "Matrix.hpp"
#include "Vector.hpp"
#include "Quaternion.hpp"

class PathfindingSimulator : public QObject {
    Q_OBJECT

    Q_PROPERTY(bool running READ getRunning NOTIFY runningChanged)
    Q_PROPERTY(bool paused READ getPaused NOTIFY pausedChanged)

    Q_PROPERTY(double time READ getTime NOTIFY currentStateChanged)
    Q_PROPERTY(double cubeSize READ getCubeSize NOTIFY cubeSizeChanged)

    Q_PROPERTY(QQuaternion rotation READ getRotation NOTIFY currentStateChanged)

    Q_PROPERTY(bool gravity READ getGravity WRITE setGravity NOTIFY gravityChanged)

    Q_PROPERTY(QVector3D gravityInCubeFrame READ getGravityInCubeFrame NOTIFY currentStateChanged)
    Q_PROPERTY(QVector3D torqueInCubeFrame READ getTorqueInCubeFrame NOTIFY currentStateChanged)
    Q_PROPERTY(QVector3D cubeCenterInCuberFrame READ getCubeCenterInCubeFrame NOTIFY currentStateChanged)
    Q_PROPERTY(QVector3D angularVelocityInCubeFrame READ getAngularVelocityInCubeFrame NOTIFY currentStateChanged)

public:
    explicit PathfindingSimulator(QObject* parent = nullptr);

    Q_INVOKABLE bool start(double dt, double cubeSize, double cubeDensity, double angularVelocity, double axisOffset);

    Q_INVOKABLE bool togglePause();

    Q_INVOKABLE bool reset();

    Q_INVOKABLE QQuaternion fromToQuaternion(const QVector3D &from, const QVector3D &to) {
        return QQuaternion::rotationTo(from, to);
    }

    Q_INVOKABLE QVector3D rotateVector(const QQuaternion &quaternion, const QVector3D &vec) {
        return quaternion.rotatedVector(vec);
    }

    bool getRunning() const { return running; }

    bool getPaused() const { return paused; }

    double getTime() const { return currentState.time; }

    double getCubeSize() const { return cubeSize; }

    QQuaternion getRotation() const {
        return QQuaternion(static_cast<float>(currentState.rotation.W),
                           static_cast<float>(currentState.rotation.X),
                           static_cast<float>(currentState.rotation.Y),
                           static_cast<float>(currentState.rotation.Z));
    }

    QVector3D getGravityInCubeFrame() const {
        return QVector3D(
                static_cast<float>(gravityInCubeFrame.X()),
                static_cast<float>(gravityInCubeFrame.Y()),
                static_cast<float>(gravityInCubeFrame.Z())
        );
    }

    QVector3D getTorqueInCubeFrame() const {
        return QVector3D(
                static_cast<float>(torqueInCubeFrame.X()),
                static_cast<float>(torqueInCubeFrame.Y()),
                static_cast<float>(torqueInCubeFrame.Z())
        );
    }

    QVector3D getCubeCenterInCubeFrame() const {
        return QVector3D(
                static_cast<float>(cubeSize / 2),
                static_cast<float>(cubeSize / 2),
                static_cast<float>(cubeSize / 2)
        );
    }

    QVector3D getAngularVelocityInCubeFrame() const {
        return QVector3D(
                static_cast<float>(currentState.angularVelocity.X()),
                static_cast<float>(currentState.angularVelocity.Y()),
                static_cast<float>(currentState.angularVelocity.Z())
        );
    }

    bool getGravity() const { return gravity; }

    void setGravity(bool value) {
        if (gravity != value) {
            gravity = value;
            emit gravityChanged();
        }
    }

signals:
    void runningChanged();
    void cubeSizeChanged();
    void pausedChanged();

    void currentStateChanged();

    void gravityChanged();

    void step();

private slots:
    void tick();

private:

    struct state {
        double time;
        Math::Vector3d angularVelocity;
        Math::Quaternion rotation;
    } previousState, currentState;

    double dt, cubeSize, cubeDensity, angularVelocity, axisOffset;
    Math::Matrix3d tensor;
    Math::Matrix3d invertedTensor;
    Math::Vector3d gravityInCubeFrame;
    Math::Vector3d torqueInCubeFrame;

    bool gravity;

    bool running, paused;

    QTimer* timer;
    QElapsedTimer* elapsedTimer;
    qint64 leftover;

    void calculateTensor();

    Math::Vector4d getGravityForce();

    Math::Quaternion baseCubeRotation();

};


#endif //PSW_SIMULATOR_HPP
