#ifndef MATH_QUATERNION_HPP
#define MATH_QUATERNION_HPP

#include "Vector.hpp"
#include "Matrix.hpp"

namespace Math
{
    struct Quaternion
    {

        union {
            struct {
                double W;
                double X;
                double Y;
                double Z;
            };

            double Values[4];
        };

        explicit Quaternion();

        explicit Quaternion(double w, double x, double y, double z);

        Vector4d GetAxisAngle() const;

        Vector3d GetEulerAngles() const;

        Matrix4d GetRotationMatrix() const {
            double x2 = this->X * 2;
            double y2 = this->Y * 2;
            double z2 = this->Z * 2;
            double xx = this->X * x2;
            double yy = this->Y * y2;
            double zz = this->Z * z2;
            double xy = this->X * y2;
            double xz = this->X * z2;
            double yz = this->Y * z2;
            double wx = this->W * x2;
            double wy = this->W * y2;
            double wz = this->W * z2;

            return Matrix4d{
                    Vector4d{ 1 - (yy + zz), xy - wz, xz + wy, 0 },
                    Vector4d{ xy + wz, 1 - (xx + zz), yz - wx, 0 },
                    Vector4d{ xz - wy, yz + wx, 1 - (xx + yy), 0 },
                    Vector4d{ 0, 0, 0, 1 }
            };
        }

        double GetLengthSquared() const;

        double GetLength() const;

        Quaternion GetNormalized() const;

        void Normalize();

        static Quaternion CreateFromMatrix(const Matrix4d &matrix);

        static Quaternion CreateFromAxisAngle(const Vector3d &axis, double angle);

        static Quaternion CreateFromAxisAngle(double x, double y, double z, double angle);

        static Quaternion CreateRotationZ(double angle);

        static Quaternion CreateRotationY(double angle);

        static Quaternion CreateRotationX(double angle);

        static Quaternion CreateFromEuler(double x, double y, double z);

        static Quaternion CreateFromEuler(const Vector3d &angles);

		static const Quaternion Identity;
    };

    Quaternion operator-(const Quaternion &q);

    Quaternion operator+(const Quaternion &q);

    Quaternion operator+(const Quaternion &q1, const Quaternion &q2);

    Quaternion operator-(const Quaternion &q1, const Quaternion &q2);

    Quaternion operator*(const Quaternion &q1, const Quaternion &q2);

    Vector4d operator*(const Quaternion &q, const Vector4d &v);

    Quaternion operator*(const Quaternion &q, double value);

    Vector3d operator*(const Quaternion &q, const Vector3d &v);

    Quaternion operator/(const Quaternion &q, double v);

    bool operator==(const Quaternion &lhs, const Quaternion &rhs);

    bool operator!=(const Quaternion &lhs, const Quaternion &rhs);

    std::istream &operator>>(std::istream &stream, Quaternion &quaternion);

    std::istream &operator>>=(std::istream &stream, Quaternion &quaternion);

    std::ostream &operator<<(std::ostream &stream, const Quaternion &quaternion);

    std::ostream &operator<<=(std::ostream &stream, const Quaternion &quaternion);

}

#endif // !MATH_QUATERNION_HPP
