// Created by Paweł Sobótka on 18.08.17.
// Rail-Mil Computers Sp. z o.o.

#include <QApplication>
#include <QQmlApplicationEngine>

#include "Simulator.hpp"
#include "TipPath.hpp"

int main(int argc, char** argv) {

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<PathfindingSimulator>("PSW.Gyration", 1, 0, "Simulator");
    qmlRegisterType<TipPath>("PSW.Gyration", 1, 0, "TipPath");

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}