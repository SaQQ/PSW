// Created by Paweł Sobótka on 05.10.17.
// Rail-Mil Computers Sp. z o.o.

#include <QDebug>

#include "Simulator.hpp"

namespace {
    inline double integrate_forward(double currentValue, double currentDerivative, double dt) {
        return currentValue + currentDerivative * dt;
    }

    inline double integrate_central(double previousValue, double currentDerivative, double dt) {
        return previousValue + 2.0 *  currentDerivative * dt;
    }

    template<typename T>
    inline T integrate_runge_kutta(T previousValue, T k1, T k2, T k3, T k4, double dt) {
        return previousValue + (k1 + k2 * 2.0 + k3 * 2.0 + k4) * dt / 6.0;
    }

    using YType = std::pair<Math::Vector3d, Math::Quaternion>;

    inline YType F(const Math::Matrix3d &I, const Math::Matrix3d &IInv, const Math::Vector3d &N, const YType &Y) {
        Math::Vector3d W;
        Math::Quaternion Q;
        std::tie(W, Q) = Y;
        return {IInv * (N + Math::Vector3d::Cross(I * W, W)), Q.GetNormalized() * Math::Quaternion(0.0, W.X() / 2.0, W.Y() / 2.0, W.Z() / 2.0)};
    }

    inline Math::Vector3d F1(const Math::Matrix3d &I, const Math::Matrix3d &IInv, const Math::Vector3d &N, const Math::Vector3d &W) {
        return IInv * (N + Math::Vector3d::Cross(I * W, W));
    }

    inline Math::Quaternion F2(const Math::Vector3d &W, const Math::Quaternion &Q) {
        return Q.GetNormalized() * Math::Quaternion(0.0, W.X() / 2.0, W.Y() / 2.0, W.Z() / 2.0);
    }

    inline YType integrateCorrect(const Math::Matrix3d &I,
                                  const Math::Matrix3d &IInv,
                                  const Math::Vector3d &externalForce,
                                  double mass,
                                  double size,
                                  const YType &Y,
                                  double t,
                                  double dt) {

        Math::Vector3d W;
        Math::Quaternion Q;
        std::tie(W, Q) = Y;

        auto N = Math::Vector3d::Cross(Math::Vector3d(size / 2, size / 2, size / 2), (-Y.second) * (mass * externalForce));

        auto Yk1 = F(I, IInv, N, Y);
        YType k1 = {Y.first + Yk1.first * dt / 2.0, Y.second + Yk1.second * dt / 2.0};
        auto Yk2 = F(I, IInv, N, k1);
        YType k2 = {Y.first + Yk2.first * dt / 2.0, Y.second + Yk2.second * dt / 2.0};
        auto Yk3 = F(I, IInv, N, k2);
        YType k3 = {Y.first + Yk3.first * dt / 2.0, Y.second + Yk3.second * dt / 2.0};
        auto Yk4 = F(I, IInv, N, k3);

        auto integratedW = integrate_runge_kutta(Y.first, Yk1.first, Yk2.first, Yk3.first, Yk4.first, dt);
        auto integratedQ = integrate_runge_kutta(Y.second, Yk1.second, Yk2.second, Yk3.second, Yk4.second, dt);
        return {integratedW, integratedQ};
    }

    inline YType integrate(const Math::Matrix3d &I,
                           const Math::Matrix3d &IInv,
                           const Math::Vector3d &N,
                           const YType &Y,
                           double t,
                           double dt) {

        Math::Vector3d W;
        Math::Quaternion Q;
        std::tie(W, Q) = Y;

        auto Wk1 = F1(I, IInv, N, W);
        auto Wk2 = F1(I, IInv, N, W + Wk1 * dt / 2.0);
        auto Wk3 = F1(I, IInv, N, W + Wk2 * dt / 2.0);
        auto Wk4 = F1(I, IInv, N, W + Wk3 * dt);

        auto integratedAngularVelocity = integrate_runge_kutta<Math::Vector3d>(W, Wk1, Wk2, Wk3, Wk4, dt);

        auto Qk1 = F2(integratedAngularVelocity, Q);
        auto Qk2 = F2(integratedAngularVelocity, Q + Qk1 * dt / 2.0);
        auto Qk3 = F2(integratedAngularVelocity, Q + Qk2 * dt / 2.0);
        auto Qk4 = F2(integratedAngularVelocity, Q + Qk3 * dt);

        auto integratedRotation = integrate_runge_kutta<Math::Quaternion>(Q, Qk1, Qk2, Qk3, Qk4, dt).GetNormalized();

        return { integratedAngularVelocity, integratedRotation };
    }

    Math::Vector3d RK4_1F(const Math::Matrix3d &I,
                          const Math::Matrix3d &IInv,
                          const Math::Vector3d &angularVelocity,
                          const Math::Vector3d &torque)
    {
        return IInv * (torque + Math::Vector3d::Cross(I * angularVelocity, angularVelocity));
    }

    Math::Vector3d RK4_1(const Math::Matrix3d &I,
                         const Math::Matrix3d &IInv,
                         double dt,
                         const Math::Vector3d &angularVelocity,
                         const Math::Vector3d &torque)
    {
        auto dtHalf = dt * 0.5f;

        auto k1 = RK4_1F(I, IInv, angularVelocity, torque);
        auto k2 = RK4_1F(I, IInv, angularVelocity + dtHalf * k1, torque);
        auto k3 = RK4_1F(I, IInv, angularVelocity + dtHalf * k2, torque);
        auto k4 = RK4_1F(I, IInv, angularVelocity + dt * k3, torque);

        return angularVelocity + dt / 6.0 * (k1 + 2.0 * k2 + 2.0 * k3 + k4);
    }

    Math::Quaternion RK4_2F(const Math::Quaternion &rotation, const Math::Quaternion &angularVelocity)
    {
        // ???
        return rotation.GetNormalized() * Math::Quaternion(angularVelocity.W * 0.5,
                                                           angularVelocity.X * 0.5,
                                                           angularVelocity.Y * 0.5,
                                                           angularVelocity.Z * 0.5);
    }

    Math::Quaternion RK4_2(double dt, const Math::Quaternion &rotation, const Math::Vector3d &angularVelocity)
    {
        auto dtHalf = dt * 0.5f;

        auto velocityQuaternion = Math::Quaternion(0.0, angularVelocity.X(), angularVelocity.Y(), angularVelocity.Z());

        auto k1 = RK4_2F(rotation, velocityQuaternion);
        auto k2 = RK4_2F(rotation + k1 * dtHalf, velocityQuaternion);
        auto k3 = RK4_2F(rotation + k2 * dtHalf, velocityQuaternion);
        auto k4 = RK4_2F(rotation + k3 * dt, velocityQuaternion);

        return (rotation + (k1 + k2 * 2.0 + k3 * 2.0 + k4) * dt / 6.0).GetNormalized();
    }

}

PathfindingSimulator::PathfindingSimulator(QObject *parent) : QObject(parent),
                                                              dt{0.01}, cubeSize{2.0}, gravity{true},
                                                              running{false}, paused{false} {

    timer = new QTimer{this};
    elapsedTimer = new QElapsedTimer;
    leftover = 0;
    QObject::connect(timer, &QTimer::timeout, this, &PathfindingSimulator::tick);

    reset();
}

bool PathfindingSimulator::start(double dt, double cubeSize, double cubeDensity, double angularVelocity, double axisOffset)
{
    if(running)
        return false;

    this->dt = dt;
    this->cubeSize = cubeSize;
    this->cubeDensity = cubeDensity;
    this->angularVelocity = angularVelocity;
    this->axisOffset = axisOffset;

    emit cubeSizeChanged();

    calculateTensor();

    auto startRotation = Math::Quaternion::CreateFromAxisAngle(Math::Vector3d(1, 0, 0), Math::PI * axisOffset / 180.0) * baseCubeRotation();
    auto startAngularVelocity = Math::Vector3d(1.0, 1.0, 1.0).Normalized() * angularVelocity;

	currentState.time = 0.0;
	currentState.angularVelocity = startAngularVelocity;
	currentState.rotation = startRotation;

	previousState.time = -dt;
	previousState.angularVelocity = startAngularVelocity;
	previousState.rotation = startRotation;

    emit currentStateChanged();

    timer->setInterval(static_cast<int>(dt * 1000));
    timer->start();
    elapsedTimer->start();
    leftover = 0;

    running = true;
    emit runningChanged();
    return true;
}

bool PathfindingSimulator::togglePause() {
    if(!running)
        return false;
    paused = !paused;

    if(paused)
        timer->stop();
    else
    {
        timer->start();
        elapsedTimer->restart();
        leftover = 0;
    }

    emit pausedChanged();
    return true;
}

bool PathfindingSimulator::reset() {
    running = paused = false;
    emit runningChanged();
    emit pausedChanged();

    timer->stop();

    auto startRotation = baseCubeRotation();
    auto startAngularVelocity = Math::Vector3d::Zero;

	currentState.time = 0.0;
	currentState.angularVelocity = startAngularVelocity;
	currentState.rotation = startRotation;

	previousState.time = -dt;
	previousState.angularVelocity = startAngularVelocity;
	previousState.rotation = startRotation;

    emit currentStateChanged();

    return true;
}

void PathfindingSimulator::tick() {
    if(!running || paused)
        return;

    state nextState;

    auto elapsedMsec = elapsedTimer->elapsed() + leftover;

    elapsedTimer->restart();

    leftover = elapsedMsec % timer->interval();

    for(int i = 0; i < elapsedMsec / timer->interval(); ++i)
    {
        Math::Vector4d localForce = (-currentState.rotation) * (cubeSize * cubeSize * cubeSize * cubeDensity * getGravityForce());

        gravityInCubeFrame =  Math::Vector3d(localForce.X(), localForce.Y(), localForce.Z());

        torqueInCubeFrame = Math::Vector3d::Cross(Math::Vector3d(cubeSize / 2, cubeSize / 2, cubeSize / 2), gravityInCubeFrame);

        std::tie(nextState.angularVelocity, nextState.rotation) = integrate(tensor, invertedTensor, torqueInCubeFrame, {currentState.angularVelocity, currentState.rotation}, currentState.time, dt);
        nextState.time = currentState.time + dt;

//        nextState.angularVelocity = RK4_1(tensor, invertedTensor, dt, currentState.angularVelocity, torqueInCubeFrame);
//        nextState.rotation = RK4_2(dt, currentState.rotation, nextState.angularVelocity);

        previousState = currentState;
        currentState = nextState;

    }

    emit currentStateChanged();
    emit step();
}


void PathfindingSimulator::calculateTensor()
{
    double sizePowered = cubeSize * cubeSize * cubeSize * cubeSize * cubeSize;
    double diag = 2.0 * cubeDensity * sizePowered / 3.0;
    double rest = -cubeDensity * sizePowered / 4.0;

    tensor = Math::Matrix3d{{
                                    Math::Vector3d{diag, rest, rest},
                                    Math::Vector3d{rest, diag, rest},
                                    Math::Vector3d{rest, rest, diag}
                            }};

    invertedTensor = tensor.Inverse();
}

Math::Vector4d PathfindingSimulator::getGravityForce()
{
    return gravity ? Math::Vector4d(0, -9.81, 0, 0) : Math::Vector4d(0, 0, 0, 0);
}


Math::Quaternion PathfindingSimulator::baseCubeRotation()
{
    return Math::Quaternion::CreateFromAxisAngle(Math::Vector3d{1.0, 0.0, 0.0}, -std::atan(1 / std::sqrt(2)))
           * Math::Quaternion::CreateFromAxisAngle(Math::Vector3d{0.0, 0.0, 1.0},  Math::PI / 4);
}