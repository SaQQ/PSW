// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#include "TipPath.hpp"

#include <Qt3DRender/QGeometryRenderer>

#include <Qt3DExtras/QPerVertexColorMaterial>


TipPath::TipPath(Qt3DCore::QEntity *parent)
        : Qt3DCore::QEntity(parent),
          length{100} {

    auto geometry_renderer = new Qt3DRender::QGeometryRenderer(this);
    geometry_renderer->setPrimitiveType(Qt3DRender::QGeometryRenderer::LineStrip);
    auto geometry = new Qt3DRender::QGeometry(geometry_renderer);

    vbo = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::VertexBuffer, geometry);
    ibo = new Qt3DRender::QBuffer(Qt3DRender::QBuffer::IndexBuffer, geometry);

    positionAttr = new Qt3DRender::QAttribute(geometry);
    positionAttr->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    positionAttr->setBuffer(vbo);
    positionAttr->setByteOffset(0);
    positionAttr->setByteStride(sizeof(vertex));
    positionAttr->setVertexSize(3);
    positionAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
    positionAttr->setCount(0);
    positionAttr->setName(Qt3DRender::QAttribute::defaultPositionAttributeName());

    colorAttr = new Qt3DRender::QAttribute(geometry);
    colorAttr->setAttributeType(Qt3DRender::QAttribute::VertexAttribute);
    colorAttr->setBuffer(vbo);
    colorAttr->setByteOffset(sizeof(vertex::pos));
    colorAttr->setByteStride(sizeof(vertex));
    colorAttr->setVertexSize(3);
    colorAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::Float);
    colorAttr->setCount(0);
    colorAttr->setName(Qt3DRender::QAttribute::defaultColorAttributeName());

    indexAttr = new Qt3DRender::QAttribute(geometry);
    indexAttr->setAttributeType(Qt3DRender::QAttribute::IndexAttribute);
    indexAttr->setBuffer(ibo);
    indexAttr->setByteOffset(0);
    indexAttr->setByteStride(sizeof(unsigned int));
    indexAttr->setVertexSize(1);
    indexAttr->setVertexBaseType(Qt3DRender::QAttribute::VertexBaseType::UnsignedInt);
    indexAttr->setCount(0);

    geometry->addAttribute(positionAttr);
    geometry->addAttribute(colorAttr);
    geometry->addAttribute(indexAttr);

    geometry_renderer->setGeometry(geometry);

    auto material = new Qt3DExtras::QPerVertexColorMaterial(this);

    this->addComponent(geometry_renderer);
    this->addComponent(material);

    generateGeometry();
}

void TipPath::setLength(int value) {
    if (length != value) {
        length = value;
        emit lengthChanged();

        generateGeometry();
    }
}

void TipPath::generateGeometry() {

    if (vertexBuffer.size() > length)
    {
        auto diff = vertexBuffer.size() - length;
        vertexBuffer.erase(vertexBuffer.begin(), vertexBuffer.begin() + 1);
        indexBuffer.erase(indexBuffer.begin(), indexBuffer.begin() + 1);
        for (unsigned int i = 0; i < vertexBuffer.size(); ++i)
            indexBuffer[i] = i;
    }

    ibo->setData(QByteArray(reinterpret_cast<const char *>(indexBuffer.data()), static_cast<int>(indexBuffer.size() * sizeof(unsigned int))));
    vbo->setData(QByteArray(reinterpret_cast<const char *>(vertexBuffer.data()), static_cast<int>(vertexBuffer.size() * sizeof(vertex))));

    indexAttr->setCount(static_cast<unsigned int>(indexBuffer.size()));
    positionAttr->setCount(static_cast<unsigned int>(vertexBuffer.size()));
}

void TipPath::pushPoint(const QVector3D &point) {
	vertex newVertex;
	newVertex.pos = Math::Vector3f{ point.x(), point.y(), point.z() };
	newVertex.color = Math::Vector3f{ 1.0, 0.0, 0.0 };
    vertexBuffer.push_back(newVertex);
    indexBuffer.push_back(static_cast<unsigned int>(indexBuffer.size()));

    generateGeometry();
}

void TipPath::clear()
{
    vertexBuffer.clear();
    indexBuffer.clear();

    generateGeometry();
}