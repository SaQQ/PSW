#ifndef MATH_CONSTANTS_HPP
#define MATH_CONSTANTS_HPP

namespace Math {

    constexpr float Eps = 0.00001f;

	constexpr float PI = 3.14159265359f;

}

#endif //MATH_CONSTANTS_HPP
