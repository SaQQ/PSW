#ifndef MATH_MATRIX_HPP
#define MATH_MATRIX_HPP

#include "Vector.hpp"
#include "Utilities.hpp"

namespace Math {

	template<typename TType, std::size_t TRows, std::size_t TCols>
	class Matrix
	{
	public:
		using RowType = Vector<TType, TCols>;
		using DataType = std::array<RowType, TRows>;

		Matrix() :
			data{ Vector<TType, TCols>{ 0 } }
		{
		}

		Matrix(TType value) :
			data{ Vector<TType, TCols>{ value } }
		{
		}

		Matrix(const DataType& arr) :
			data{ arr }
		{
		}

		template<std::size_t r = TRows, typename = typename std::enable_if<r == 1>::type>
		Matrix(RowType row0) :
			data{ { row0 } }
		{
		}

		template<std::size_t r = TRows, typename = typename std::enable_if<r == 2>::type>
		Matrix(RowType row0, RowType row1) :
			data{ { row0, row1 } }
		{
		}

		template<std::size_t r = TRows, typename = typename std::enable_if<r == 3>::type>
		Matrix(RowType row0, RowType row1, RowType row2) :
			data{ { row0, row1, row2 } }
		{
		}

		template<std::size_t r = TRows, typename = typename std::enable_if<r == 4>::type>
		Matrix(RowType row0, RowType row1, RowType row2, RowType row3) :
			data{ { row0, row1, row2, row3 } }
		{
		}

		RowType &operator[](std::size_t row) { return data[row]; }
		const RowType &operator[](std::size_t row) const { return data[row]; }

		template<std::size_t other_cols>
		Matrix<TType, TRows, other_cols> operator*(const Matrix<TType, TCols, other_cols> &rhs) const
		{
			Matrix<TType, TRows, other_cols> ret;
			for (std::size_t r = 0; r < TRows; ++r) {
				for (std::size_t c = 0; c < other_cols; ++c) {
					ret.data[r][c] = data[r][0] * rhs.data[0][c];
					for (std::size_t i = 1; i < TCols; ++i)
						ret.data[r][c] += data[r][i] * rhs.data[i][c];
				}
			}

			return ret;
		};

		Vector<TType, TRows> operator*(const Vector<TType, TCols> &rhs) const {
			Vector<TType, TRows> ret;
			for (std::size_t r = 0; r < TRows; ++r) {
				ret[r] = 0;
				for (std::size_t c = 0; c < TCols; ++c)
					ret[r] += data[r][c] * rhs[c];
			}
			return ret;
		}

		Matrix<TType, TCols, TRows> Transpose() const {
			Matrix<TType, TCols, TRows> ret;
			for (std::size_t r = 0; r < TRows; ++r)
				for (std::size_t c = 0; c < TCols; ++c)
					ret.data[c][r] = data[r][c];
			return ret;
		}

		Matrix<TType, TCols, TRows>& operator+=(const Matrix<TType, TCols, TRows> &rhs) {
			for (std::size_t r = 0; r < TRows; ++r)
				data[r] += rhs.data[r];
			return *this;
		};

		Matrix<TType, TCols, TRows>& operator-=(const Matrix<TType, TCols, TRows> &rhs) {
			for (std::size_t r = 0; r < TRows; ++r)
				data[r] -= rhs.data[r];
			return *this;
		};

		Matrix<TType, TCols, TRows>& operator*=(const TType &rhs) {
			for (std::size_t r = 0; r < TRows; ++r)
				data[r] *= rhs;
			return *this;
		};

		Matrix<TType, TCols, TRows>& operator/=(const TType &rhs) {
			for (std::size_t r = 0; r < TRows; ++r)
				data[r] /= rhs;
			return *this;
		};

		Matrix<TType, TCols, TRows> operator+(const Matrix<TType, TCols, TRows> &rhs) const {
			Matrix<TType, TCols, TRows> ret = *this;
			ret += rhs;
			return ret;
		};

		Matrix<TType, TCols, TRows> operator-(const Matrix<TType, TCols, TRows> &rhs) const {
			Matrix<TType, TCols, TRows> ret = *this;
			ret -= rhs;
			return ret;
		};

		Matrix<TType, TCols, TRows> operator*(const TType &rhs) const {
			Matrix<TType, TCols, TRows> ret = *this;
			ret *= rhs;
			return ret;
		};

		Matrix<TType, TCols, TRows> operator/(const TType &rhs) const {
			Matrix<TType, TCols, TRows> ret = *this;
			ret /= rhs;
			return ret;
		};

		bool operator==(const Matrix<TType, TCols, TRows> &rhs) const {
            for (std::size_t r = 0; r < TRows; ++r)
                for (std::size_t c = 0; c < TCols; ++c)
                    if (!EpsEqual(data[r][c], rhs[r][c]))
                        return false;
            return true;
		};

        bool operator!=(const Matrix<TType, TCols, TRows> &rhs) const {
			for (std::size_t r = 0; r < TRows; ++r)
				for (std::size_t c = 0; c < TCols; ++c)
					if (EpsEqual(data[r][c], rhs[r][c]))
						return false;
			return true;
        }

		/* Inverse matrix functions for matrices 1x1, 2x2, 3x3 and 4x4 */

		template<std::size_t r = TRows, std::size_t c = TCols>
		typename std::enable_if<r == 1 && c == 1, Matrix<TType, r, c>>::type Inverse() const {
			return Matrix<TType, r, c>{ {Vector<TType, r>{ {1 / data[0][0]}}}};
		}

		template<std::size_t r = TRows, std::size_t c = TCols>
		typename std::enable_if<r == 2 && c == 2, Matrix<TType, r, c>>::type Inverse() const {
			auto det = data[0][0] * data[1][1] - data[0][1] * data[1][0];
			return Matrix<TType, r, c>{ {
					Vector<TType, r>{ {data[1][1] / det, -data[0][1] / det}},
						Vector<TType, r>{ {-data[1][0] / det, data[0][0] / det}}
				}};
		}

		template<std::size_t r = TRows, std::size_t c = TCols>
		typename std::enable_if<r == 3 && c == 3, Matrix<TType, r, c>>::type Inverse() const {
			auto det = data[0][0] * data[1][1] * data[2][2]
				+ data[1][0] * data[2][1] * data[0][2]
				+ data[2][0] * data[0][1] * data[1][2]
				- data[0][0] * data[2][1] * data[1][2]
				- data[2][0] * data[1][1] * data[0][2]
				- data[1][0] * data[0][1] * data[2][2];

			return Matrix<TType, r, c>{ {
					Vector<TType, r>{ {
							(data[1][1] * data[2][2] - data[1][2] * data[2][1]) / det,
								(data[0][2] * data[2][1] - data[0][1] * data[2][2]) / det,
								(data[0][1] * data[1][2] - data[0][2] * data[1][1]) / det
						}},
						Vector<TType, r>{ {
								(data[1][2] * data[2][0] - data[1][0] * data[2][2]) / det,
									(data[0][0] * data[2][2] - data[0][2] * data[2][0]) / det,
									(data[0][2] * data[1][0] - data[0][0] * data[1][2]) / det
							}},
								Vector<TType, r>{ {
										(data[1][0] * data[2][1] - data[1][1] * data[2][0]) / det,
											(data[0][1] * data[2][0] - data[0][0] * data[2][1]) / det,
											(data[0][0] * data[1][1] - data[0][1] * data[1][0]) / det
									}}
				}};
		}



		template<std::size_t r = TRows, std::size_t c = TCols>
		typename std::enable_if<r == 4 && c == 4, Matrix<TType, r, c>>::type Inverse() const {
			Matrix<TType, r, c> inv;
			inv.data[0][0] = data[1][1] * data[2][2] * data[3][3] -
				data[1][1] * data[3][2] * data[2][3] -
				data[1][2] * data[2][1] * data[3][3] +
				data[1][2] * data[3][1] * data[2][3] +
				data[1][3] * data[2][1] * data[3][2] -
				data[1][3] * data[3][1] * data[2][2];

			inv.data[0][1] = -data[0][1] * data[2][2] * data[3][3] +
				data[0][1] * data[3][2] * data[2][3] +
				data[0][2] * data[2][1] * data[3][3] -
				data[0][2] * data[3][1] * data[2][3] -
				data[0][3] * data[2][1] * data[3][2] +
				data[0][3] * data[3][1] * data[2][2];

			inv.data[0][2] = data[0][1] * data[1][2] * data[3][3] -
				data[0][1] * data[3][2] * data[1][3] -
				data[0][2] * data[1][1] * data[3][3] +
				data[0][2] * data[3][1] * data[1][3] +
				data[0][3] * data[1][1] * data[3][2] -
				data[0][3] * data[3][1] * data[1][2];

			inv.data[0][3] = -data[0][1] * data[1][2] * data[2][3] +
				data[0][1] * data[2][2] * data[1][3] +
				data[0][2] * data[1][1] * data[2][3] -
				data[0][2] * data[2][1] * data[1][3] -
				data[0][3] * data[1][1] * data[2][2] +
				data[0][3] * data[2][1] * data[1][2];

			inv.data[1][0] = -data[1][0] * data[2][2] * data[3][3] +
				data[1][0] * data[3][2] * data[2][3] +
				data[1][2] * data[2][0] * data[3][3] -
				data[1][2] * data[3][0] * data[2][3] -
				data[1][3] * data[2][0] * data[3][2] +
				data[1][3] * data[3][0] * data[2][2];

			inv.data[1][1] = data[0][0] * data[2][2] * data[3][3] -
				data[0][0] * data[3][2] * data[2][3] -
				data[0][2] * data[2][0] * data[3][3] +
				data[0][2] * data[3][0] * data[2][3] +
				data[0][3] * data[2][0] * data[3][2] -
				data[0][3] * data[3][0] * data[2][2];

			inv.data[1][2] = -data[0][0] * data[1][2] * data[3][3] +
				data[0][0] * data[3][2] * data[1][3] +
				data[0][2] * data[1][0] * data[3][3] -
				data[0][2] * data[3][0] * data[1][3] -
				data[0][3] * data[1][0] * data[3][2] +
				data[0][3] * data[3][0] * data[1][2];

			inv.data[1][3] = data[0][0] * data[1][2] * data[2][3] -
				data[0][0] * data[2][2] * data[1][3] -
				data[0][2] * data[1][0] * data[2][3] +
				data[0][2] * data[2][0] * data[1][3] +
				data[0][3] * data[1][0] * data[2][2] -
				data[0][3] * data[2][0] * data[1][2];

			inv.data[2][0] = data[1][0] * data[2][1] * data[3][3] -
				data[1][0] * data[3][1] * data[2][3] -
				data[1][1] * data[2][0] * data[3][3] +
				data[1][1] * data[3][0] * data[2][3] +
				data[1][3] * data[2][0] * data[3][1] -
				data[1][3] * data[3][0] * data[2][1];

			inv.data[2][1] = -data[0][0] * data[2][1] * data[3][3] +
				data[0][0] * data[3][1] * data[2][3] +
				data[0][1] * data[2][0] * data[3][3] -
				data[0][1] * data[3][0] * data[2][3] -
				data[0][3] * data[2][0] * data[3][1] +
				data[0][3] * data[3][0] * data[2][1];

			inv.data[2][2] = data[0][0] * data[1][1] * data[3][3] -
				data[0][0] * data[3][1] * data[1][3] -
				data[0][1] * data[1][0] * data[3][3] +
				data[0][1] * data[3][0] * data[1][3] +
				data[0][3] * data[1][0] * data[3][1] -
				data[0][3] * data[3][0] * data[1][1];

			inv.data[2][3] = -data[0][0] * data[1][1] * data[2][3] +
				data[0][0] * data[2][1] * data[1][3] +
				data[0][1] * data[1][0] * data[2][3] -
				data[0][1] * data[2][0] * data[1][3] -
				data[0][3] * data[1][0] * data[2][1] +
				data[0][3] * data[2][0] * data[1][1];

			inv.data[3][0] = -data[1][0] * data[2][1] * data[3][2] +
				data[1][0] * data[3][1] * data[2][2] +
				data[1][1] * data[2][0] * data[3][2] -
				data[1][1] * data[3][0] * data[2][2] -
				data[1][2] * data[2][0] * data[3][1] +
				data[1][2] * data[3][0] * data[2][1];

			inv.data[3][1] = data[0][0] * data[2][1] * data[3][2] -
				data[0][0] * data[3][1] * data[2][2] -
				data[0][1] * data[2][0] * data[3][2] +
				data[0][1] * data[3][0] * data[2][2] +
				data[0][2] * data[2][0] * data[3][1] -
				data[0][2] * data[3][0] * data[2][1];

			inv.data[3][2] = -data[0][0] * data[1][1] * data[3][2] +
				data[0][0] * data[3][1] * data[1][2] +
				data[0][1] * data[1][0] * data[3][2] -
				data[0][1] * data[3][0] * data[1][2] -
				data[0][2] * data[1][0] * data[3][1] +
				data[0][2] * data[3][0] * data[1][1];

			inv.data[3][3] = data[0][0] * data[1][1] * data[2][2] -
				data[0][0] * data[2][1] * data[1][2] -
				data[0][1] * data[1][0] * data[2][2] +
				data[0][1] * data[2][0] * data[1][2] +
				data[0][2] * data[1][0] * data[2][1] -
				data[0][2] * data[2][0] * data[1][1];

			auto det = data[0][0] * inv.data[0][0] + data[1][0] * inv.data[0][1] + data[2][0] * inv.data[0][2] + data[3][0] * inv.data[0][3];

			return inv / det;
		}

		/* Inverse matrix function for matrices bigger than 4x4 using LU decomposition */

		template<std::size_t r = TRows, std::size_t c = TCols>
		typename std::enable_if<r == c && r >= 5, Matrix<TType, r, c>>::type Inverse() const {
			throw std::runtime_error{ "Not implemented" };
		}

        /* Determinants */

        template<std::size_t r = TRows, std::size_t c = TCols>
        typename std::enable_if<r == 1 && c == 1, float>::type Determinant() const {
            return data[0][0];
        }

        template<std::size_t r = TRows, std::size_t c = TCols>
        typename std::enable_if<r == 2 && c == 2, float>::type Determinant() const {
            return data[0][0] * data[1][1] - data[0][1] * data[1][0];
        }

        template<std::size_t r = TRows, std::size_t c = TCols>
        typename std::enable_if<r == 3 && c == 3, float>::type Determinant() const {
            return data[0][0] * data[1][1] * data[2][2]
				   + data[1][0] * data[2][1] * data[0][2]
				   + data[2][0] * data[0][1] * data[1][2]
				   - data[0][0] * data[2][1] * data[1][2]
				   - data[2][0] * data[1][1] * data[0][2]
				   - data[1][0] * data[0][1] * data[2][2];
        }

		template<std::size_t r = TRows, std::size_t c = TCols>
		typename std::enable_if<r == 4 && c == 4, float>::type Determinant() const {
			return data[3][3] *
				   (data[0][0] * data[1][1] * data[2][2]
					+ data[1][0] * data[2][1] * data[0][2]
					+ data[2][0] * data[0][1] * data[1][2]
					- data[0][0] * data[2][1] * data[1][2]
					- data[2][0] * data[1][1] * data[0][2]
					- data[1][0] * data[0][1] * data[2][2]) +
					-data[3][2] *
				   (data[0][0] * data[1][1] * data[2][3]
					+ data[1][0] * data[2][1] * data[0][3]
					+ data[2][0] * data[0][1] * data[1][3]
					- data[0][0] * data[2][1] * data[1][3]
					- data[2][0] * data[1][1] * data[0][3]
					- data[1][0] * data[0][1] * data[2][3]) +
					data[3][1] *
				   (data[0][0] * data[1][2] * data[2][3]
					+ data[1][0] * data[2][2] * data[0][3]
					+ data[2][0] * data[0][2] * data[1][3]
					- data[0][0] * data[2][2] * data[1][3]
					- data[2][0] * data[1][2] * data[0][3]
					- data[1][0] * data[0][2] * data[2][3]) +
					-data[3][0] *
				   (data[0][1] * data[1][2] * data[2][3]
					+ data[1][1] * data[2][2] * data[0][3]
					+ data[2][1] * data[0][2] * data[1][3]
					- data[0][1] * data[2][2] * data[1][3]
					- data[2][1] * data[1][2] * data[0][3]
					- data[1][1] * data[0][2] * data[2][3]);
		}

        template<std::size_t r = TRows, std::size_t c = TCols>
        typename std::enable_if<r == c && r >= 5, float>::type Determinant() const {
            throw std::runtime_error{ "Not implemented" };
        }

        /* Random functions */

		template<std::size_t r = TRows, std::size_t c = TCols>
		static typename std::enable_if<(r == 4 && c == 4) || (r == 3 && c == 3), Vector<TType, 3>>::type TransformVector(const Matrix<TType, r, c>& matrix, const Vector<TType, 3>& vector)
		{
			return Vector<TType, 3>{
				matrix[0][0] * vector[0] + matrix[0][1] * vector[1] + matrix[0][2] * vector[2],
					matrix[1][0] * vector[0] + matrix[1][1] * vector[1] + matrix[1][2] * vector[2],
					matrix[2][0] * vector[0] + matrix[2][1] * vector[1] + matrix[2][2] * vector[2]
			};
		}

		template<std::size_t r = TRows, std::size_t c = TCols>
		static typename std::enable_if<r == 4 && c == 4, Vector<TType, 3>>::type TransformPosition(const Matrix<TType, r, c>& matrix, const Vector<TType, 3>& position)
		{
			return Vector<TType, 3>{
				matrix[0][0] * position[0] + matrix[0][1] * position[1] + matrix[0][2] * position[2] + matrix[0][3],
					matrix[1][0] * position[0] + matrix[1][1] * position[1] + matrix[1][2] * position[2] + matrix[1][3],
					matrix[2][0] * position[0] + matrix[2][1] * position[1] + matrix[2][2] * position[2] + matrix[2][3]
			};
		}

		template<std::size_t r = TRows, std::size_t c = TCols>
		static typename std::enable_if<(r == 4 && c == 4) || (r == 3 && c == 3), Vector<TType, 3>>::type TransformNormal(const Matrix<TType, r, c>& matrix, const Vector<TType, 3>& normal)
		{
			auto matInv = matrix.Inverse();

			return Vector<TType, 3>{
				matInv[0][0] * normal[0] + matInv[0][1] * normal[1] + matInv[0][2] * normal[2],
					matInv[1][0] * normal[0] + matInv[1][1] * normal[1] + matInv[1][2] * normal[2],
					matInv[2][0] * normal[0] + matInv[2][1] * normal[1] + matInv[2][2] * normal[2]
			};
		}


		template<std::size_t r = TRows, std::size_t c = TCols>
		static typename std::enable_if<r == 4 && c == 4, Vector<TType, 3>>::type TransformPerspective(const Matrix<TType, 4, 4>& matrix, const Vector<TType, 3>& position)
		{
			Vector<TType, 4> result{ position, 1 };
			result = matrix * result;
			return Vector<TType, 3>{result.X(), result.Y(), result.Z()} / result.W();
		}

		template<std::size_t r = TRows, std::size_t c = TCols>
		static typename std::enable_if<(r == 4 && c == 4), Matrix<TType, 4, 4>>::type CreateTranslation(const Vector<TType, 3>& translation)
		{
			return Matrix<TType, 4, 4>{
				Vector<TType, 4>{ 1, 0, 0, translation[0] },
					Vector<TType, 4>{ 0, 1, 0, translation[1] },
					Vector<TType, 4>{ 0, 0, 1, translation[2] },
					Vector<TType, 4>{ 0, 0, 0, 1 }
			};
		}

		template<std::size_t r = TRows, std::size_t c = TCols>
		static typename std::enable_if<(r == 4 && c == 4), Matrix<TType, 4, 4>>::type CreateScale(const Vector<TType, 3>& scale)
		{
			return Matrix<TType, 4, 4>{
				Vector<TType, 4>{ scale[0], 0, 0, 0 },
					Vector<TType, 4>{ 0, scale[1], 0, 0 },
					Vector<TType, 4>{ 0, 0, scale[2], 0 },
					Vector<TType, 4>{ 0, 0, 0, 1 }
			};
		}

		template<std::size_t r = TRows, std::size_t c = TCols>
		static typename std::enable_if<(r == 4 && c == 4), Matrix<TType, 4, 4>>::type CreateLookAt(const Vector<TType, 3>& eyePosition, const Vector<TType, 3>& targetPosition, const Vector<TType, 3>& upDirection)
		{
			auto tmp = targetPosition - eyePosition;
			auto zaxis = tmp.Normalized();
			auto xaxis = Vector<TType, 3>::Cross(upDirection, zaxis).Normalized();
			auto yaxis = Vector<TType, 3>::Cross(zaxis, xaxis).Normalized();

			return Matrix<TType, 4, 4>{
				Vector<TType, 4>{ xaxis[0], xaxis[1], xaxis[2], -Vector<TType, 3>::Dot(xaxis, eyePosition) },
				Vector<TType, 4>{ yaxis[0], yaxis[1], yaxis[2], -Vector<TType, 3>::Dot(yaxis, eyePosition) },
				Vector<TType, 4>{ zaxis[0], zaxis[1], zaxis[2], -Vector<TType, 3>::Dot(zaxis, eyePosition) },
				Vector<TType, 4>{ 0, 0, 0, 1 }
			};
		}

		template<std::size_t r = TRows, std::size_t c = TCols, typename Type = TType>
		static typename std::enable_if<(r == 4 && c == 4 && (std::is_same_v<Type, double> || std::is_same_v<Type, float>)), Matrix<Type, 4, 4>>::type CreatePerspective(Type fovY, Type aspectRatio, Type nearDistance, Type farDistance)
		{
			Type cot = static_cast<Type>(1) / std::tan(fovY / static_cast<Type>(2));
			Type diff = static_cast<Type>(1) / (farDistance - nearDistance);

			return Matrix<Type, 4, 4>{
				Vector<Type, 4>{ cot / aspectRatio, 0, 0, 0 },
				Vector<Type, 4>{ 0, cot, 0, 0 },
				Vector<Type, 4>{ 0, 0, (farDistance + nearDistance) * diff, -2.0f * farDistance * nearDistance * diff },
				Vector<Type, 4>{ 0, 0, 1, 0 }
			};
		}
		

	private:
		DataType data;

	public:
		static const Matrix Identity;
	};

	template<typename TType, std::size_t TRows, std::size_t TCols>
	Matrix<TType, TRows, TCols> operator*(TType lhs, const Matrix<TType, TRows, TCols> &rhs) {
		const Matrix<TType, TRows, TCols> ret = rhs;
		ret *= lhs;
		return ret;
	}

	template<typename TType, std::size_t TRows, std::size_t TCols>
	struct MatrixValueFactory;

	template<typename TType>
	struct MatrixValueFactory<TType, 4, 4>
	{
		static Matrix<TType, 4, 4> GetIdentity()
		{
			return Matrix<TType, 4, 4>{ {
					Vector<TType, 4>{ { 1, 0, 0, 0 } },
						Vector<TType, 4>{ { 0, 1, 0, 0 } },
						Vector<TType, 4>{ { 0, 0, 1, 0 } },
						Vector<TType, 4>{ { 0, 0, 0, 1 } },
				} };
		}
	};


	template<typename TType, std::size_t TRows, std::size_t TCols>
	const Matrix<TType, TRows, TCols> Matrix<TType, TRows, TCols>::Identity = MatrixValueFactory<TType, TRows, TCols>::GetIdentity();


	using Matrix2f = Matrix<float, 2, 2>;
	using Matrix3f = Matrix<float, 3, 3>;
	using Matrix4f = Matrix<float, 4, 4>;
	using Matrix3x4f = Matrix<float, 3, 4>;
	using Matrix4x3f = Matrix<float, 4, 3>;

	using Matrix2d = Matrix<double, 2, 2>;
	using Matrix3d = Matrix<double, 3, 3>;
	using Matrix4d = Matrix<double, 4, 4>;
	using Matrix3x4d = Matrix<double, 3, 4>;
	using Matrix4x3d = Matrix<double, 4, 3>;
}

#endif // MATH_MATRIX_HPP
