// Created by Paweł Sobótka on 27.10.17.
// Rail-Mil Computers Sp. z o.o.

#ifndef CNC_SIMULATOR_TOOLPATH_HPP
#define CNC_SIMULATOR_TOOLPATH_HPP

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QTransform>

#include <Qt3DRender/QBuffer>
#include <Qt3DRender/QAttribute>

#include "Vector.hpp"


class TipPath : public Qt3DCore::QEntity {
Q_OBJECT

    Q_PROPERTY(int length READ getLength WRITE setLength NOTIFY lengthChanged)

    struct vertex {
        Math::Vector3f pos;
        Math::Vector3f color;
    };
public:
    explicit TipPath(Qt3DCore::QEntity *parent = nullptr);

    int getLength() const { return length; }

    void setLength(int value);

    Q_INVOKABLE void pushPoint(const QVector3D &point);

    Q_INVOKABLE void clear();

signals:
    void lengthChanged();

private:
    int length;

    std::vector<vertex> vertexBuffer;
    std::vector<unsigned int> indexBuffer;

    Qt3DRender::QBuffer* vbo;
    Qt3DRender::QBuffer* ibo;

    Qt3DRender::QAttribute* positionAttr;
    Qt3DRender::QAttribute* colorAttr;
    Qt3DRender::QAttribute* indexAttr;

    void generateGeometry();
};

#endif //CNC_SIMULATOR_TOOLPATH_HPP
