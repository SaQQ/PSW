#ifndef MATH_VECTOR_HPP
#define MATH_VECTOR_HPP

#include <cmath>
#include <array>
#include <algorithm>
#include <numeric>

namespace Math {

	template<typename TType, std::size_t TSize>
	struct Vector
	{
		using DataType = std::array<TType, TSize>;

	private:
		DataType data;

	public:
		Vector<TType, TSize>() :
			data{ { 0 } }
		{
		}

		explicit Vector<TType, TSize>(TType value)
		{
			for (auto& val : data)
				val = value;
		}

		template<std::size_t l = TSize, typename = typename std::enable_if<l == 2>::type>
		explicit Vector<TType, TSize>(TType x, TType y) :
			data{ { x, y } }
		{
		}

		template<std::size_t l = TSize, typename = typename std::enable_if<l == 3>::type>
		explicit Vector<TType, TSize>(TType x, TType y, TType z) :
			data{ { x, y, z } }
		{
		}

		template<std::size_t l = TSize, typename = typename std::enable_if<l == 3>::type>
		explicit Vector<TType, TSize>(const Vector<TType, 2>& vector, TType z) :
			data{ { vector[0], vector[1], z } }
		{
		}
		
		template<std::size_t l = TSize, typename = typename std::enable_if<l == 4>::type>
		explicit Vector<TType, TSize>(TType x, TType y, TType z, TType w) :
			data{ { x, y, z, w } }
		{
		}

		template<std::size_t l = TSize, typename = typename std::enable_if<l == 4>::type>
		explicit Vector<TType, TSize>(const Vector<TType, 2>& vector, TType z, TType w) :
			data{ { vector[0], vector[1], z, w } }
		{
		}

		template<std::size_t l = TSize, typename = typename std::enable_if<l == 4>::type>
		explicit Vector<TType, TSize>(const Vector<TType, 3>& vector, TType w) :
			data{ { vector[0], vector[1], vector[2], w } }
		{
		}

		explicit Vector<TType, TSize>(const DataType& arr) :
			data{ arr }
		{
		}

		TType& operator[](std::size_t idx) { return data[idx]; }
		const TType& operator[](std::size_t idx) const { return data[idx]; }

		template<std::size_t l = TSize> typename std::enable_if<l >= 1, TType&>::type X() { return data[0]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 2, TType&>::type Y() { return data[1]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 3, TType&>::type Z() { return data[2]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 4, TType&>::type W() { return data[3]; };

		template<std::size_t l = TSize> typename std::enable_if<l >= 1, TType&>::type R() { return data[0]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 2, TType&>::type G() { return data[1]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 3, TType&>::type B() { return data[2]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 4, TType&>::type A() { return data[3]; };

		template<std::size_t l = TSize> typename std::enable_if<l >= 1, const TType&>::type X() const { return data[0]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 2, const TType&>::type Y() const { return data[1]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 3, const TType&>::type Z() const { return data[2]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 4, const TType&>::type W() const { return data[3]; };

		template<std::size_t l = TSize> typename std::enable_if<l >= 1, const TType&>::type R() const { return data[0]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 2, const TType&>::type G() const { return data[1]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 3, const TType&>::type B() const { return data[2]; };
		template<std::size_t l = TSize> typename std::enable_if<l >= 4, const TType&>::type A() const { return data[3]; };



		Vector<TType, TSize>& operator+=(const Vector<TType, TSize>& rhs)
		{
			for (std::size_t i = 0; i < TSize; ++i)
				data[i] += rhs[i];
			return *this;
		};

		Vector<TType, TSize>& operator-=(const Vector<TType, TSize>& rhs)
		{
			for (std::size_t i = 0; i < TSize; ++i)
				data[i] -= rhs[i];
			return *this;
		};

		Vector<TType, TSize>& operator*=(TType rhs)
		{
			for (std::size_t i = 0; i < TSize; ++i)
				data[i] *= rhs;
			return *this;
		};

		Vector<TType, TSize>& operator/=(TType rhs)
		{
			for (std::size_t i = 0; i < TSize; ++i)
				data[i] /= rhs;
			return *this;
		};


		static TType Dot(const Vector<TType, TSize>& lhs, const Vector<TType, TSize>& rhs)
		{
			TType acc = lhs[0] * rhs[0];
			for (std::size_t i = 1; i < TSize; ++i)
				acc += lhs[i] * rhs[i];
			return acc;
		}

		TType LengthSquared() const 
		{
			return std::accumulate(data.begin() + 1, data.end(), data[0] * data[0], [](TType acc, TType curr) { return acc + curr * curr; });
		}

		TType Length() const 
		{
			return std::sqrt(LengthSquared());
		}

		void Normalize() 
		{
			auto l = Length();
			for (std::size_t i = 0; i < TSize; ++i)
				data[i] /= l;
		}

		Vector<TType, TSize> Normalized() const
		{
			auto ret = *this;
			ret.Normalize();
			return ret;
		};

		template<typename VType>
		static Vector<VType, 3> Cross(const Vector<VType, 3>& u, const Vector<VType, 3>& v)
		{
			return Vector<VType, 3>
			{
				u[1] * v[2] - u[2] * v[1],
				u[2] * v[0] - u[0] * v[2],
				u[0] * v[1] - u[1] * v[0]
			};
		}


		static const Vector Zero;
		static const Vector One;
	};

	template<typename TType, std::size_t TSize>
	const Vector<TType, TSize> Vector<TType, TSize>::Zero = Vector<TType, TSize>{ 0 };

	template<typename TType, std::size_t TSize>
	const Vector<TType, TSize> Vector<TType, TSize>::One = Vector<TType, TSize>{ 1 };

	template<typename TType, std::size_t TSize>
	Vector<TType, TSize> operator+(const Vector<TType, TSize>& lhs, const Vector<TType, TSize>& rhs)
	{
		auto ret = lhs;
		ret += rhs;
		return ret;
	};

	template<typename TType, std::size_t TSize>
	Vector<TType, TSize> operator-(const Vector<TType, TSize>& lhs, const Vector<TType, TSize>& rhs)
	{
		auto ret = lhs;
		ret -= rhs;
		return ret;
	};

	template<typename TType, std::size_t TSize>
	Vector<TType, TSize> operator*(TType lhs, const Vector<TType, TSize> &rhs)
	{
		Vector<TType, TSize> ret = rhs;
		ret *= lhs;
		return ret;
	};

	template<typename TType, std::size_t TSize>
	Vector<TType, TSize> operator*(const Vector<TType, TSize>& lhs, const TType& rhs)
	{
		auto ret = lhs;
		ret *= rhs;
		return ret;
	};

	template<typename TType, std::size_t TSize>
	Vector<TType, TSize> operator/(const Vector<TType, TSize>& lhs, const TType& rhs)
	{
		auto ret = lhs;
		ret /= rhs;
		return ret;
	};


	using Vector2f = Vector<float, 2>;
	using Vector3f = Vector<float, 3>;
	using Vector4f = Vector<float, 4>;

	using Vector2d = Vector<double, 2>;
	using Vector3d = Vector<double, 3>;
	using Vector4d = Vector<double, 4>;

	using Vector2i = Vector<std::int32_t, 2>;
	using Vector3i = Vector<std::int32_t, 3>;
	using Vector4i = Vector<std::int32_t, 4>;
}


#endif // MATH_VECTOR_HPP
