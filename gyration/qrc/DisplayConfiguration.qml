import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GridLayout {
    id: root

    width: 220

    property bool enabled: true

    readonly property bool cubeVisible: cubeCheckBox.checked
    readonly property bool diagonalVisible: diagonalCheckBox.checked
    readonly property bool tipPathVisible: tipPathCheckBox.checked
    readonly property bool gravityVectorVisible: gravityVectorCheckBox.checked

    columnSpacing: 10

    columns: 2

    Text {
        text: "Cube"
        color: "#f5f5f5"
    }

    CheckBox {
        id: cubeCheckBox
        padding: 0
        checked: true
    }

    Text {
        text: "Diagonal"
        color: "#f5f5f5"
    }

    CheckBox {
        id: diagonalCheckBox
        padding: 0
        checked: true
    }

    Text {
        text: "Tip path"
        color: "#f5f5f5"
    }

    CheckBox {
        id: tipPathCheckBox
        padding: 0
        checked: true
    }

    Text {
        text: "Gravity vector"
        color: "#f5f5f5"
    }

    CheckBox {
        id: gravityVectorCheckBox
        padding: 0
        checked: true
    }

}
