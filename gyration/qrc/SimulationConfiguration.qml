import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GridLayout {
    id: root

    width: 220

    readonly property real integrationStep: Number(integrationStepTextField.text)
    readonly property real cubeSize: Number(sizeTextField.text)
    readonly property real cubeDensity: Number(densityTextField.text)
    readonly property real angularVelocity: Number(angularVelocityTextField.text)
    readonly property real axisOffset: Number(axisOffsetTextField.text)
    readonly property int pathLength: pathLengthSpinBox.value
    readonly property bool gravity: gravityCheckBox.checked

    property bool enabled: true

    columns: 2

    columnSpacing: 10

    property real integrationStepDecimals: 4
    property real integrationStepMin: 0.0001
    property real integrationStepMax: 1.0
    property real integrationStepDefault: 0.001

    Text {
        text: "Integration step"
        color: "#f5f5f5"
    }

    TextField {
        id: integrationStepTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.integrationStepDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.integrationStepMin
            top: root.integrationStepMax
            decimals: root.integrationStepDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    Text {
        text: "Cube size"
        color: "#f5f5f5"
    }

    property real sizeMin: 1.0
    property real sizeMax: 50.0
    property real sizeDefault: 2
    property real sizeDecimals: 1

    TextField {
        id: sizeTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.sizeDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.sizeMin
            top: root.sizeMax
            decimals: root.sizeDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    Text {
        text: "Cube density"
        color: "#f5f5f5"
    }

    property real densityMin: 1.0
    property real densityMax: 100.0
    property real densityDefault: 1
    property real densityDecimals: 1

    TextField {
        id: densityTextField
        selectByMouse: true
        Layout.fillWidth: true
        text: root.densityDefault
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        validator: DoubleValidator {
            bottom: root.densityMin
            top: root.densityMax
            decimals: root.densityDecimals
            notation: DoubleValidator.StandardNotation
        }
    }

    property real angularVelocityMin: 1.0
    property real angularVelocityMax: 10000.0
    property real angularVelocityDefault: 12.0
    property real angularVelocityDecimals: 1

    Text {
        color: "#f5f5f5"
        text: "Angular velocity"
    }

    TextField {
        id: angularVelocityTextField
        text: root.angularVelocityDefault
        validator: DoubleValidator {
            bottom: root.angularVelocityMin
            top: root.angularVelocityMax
            notation: DoubleValidator.StandardNotation
            decimals: root.angularVelocityDecimals
        }
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        selectByMouse: true
        Layout.fillWidth: true
    }

    property real axisOffsetMin: 0.0
    property real axisOffsetMax: 90.0
    property real axisOffsetDefault: 30.0
    property real axisOffsetDecimals: 5

    Text {
        color: "#f5f5f5"
        text: "Axis offset"
    }

    TextField {
        id: axisOffsetTextField
        text: root.axisOffsetDefault
        validator: DoubleValidator {
            bottom: root.axisOffsetMin
            top: root.axisOffsetMax
            notation: DoubleValidator.StandardNotation
            decimals: root.axisOffsetDecimals
        }
        enabled: root.enabled
        background: Rectangle {
            border.width: 1
        }
        Layout.fillWidth: true
        selectByMouse: true
    }

    Text {
        text: "Path length"
        color: "#f5f5f5"
    }

    SpinBox {
        id: pathLengthSpinBox
        Layout.fillWidth: true
        Layout.maximumHeight: 30
        from: 1000
        to: 200000
        stepSize: 1
        value: 50000
    }

    Text {
        text: "Gravity"
        color: "#f5f5f5"
    }

    CheckBox {
        id: gravityCheckBox
        padding: 0
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        checked: true
    }

}
