import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

RowLayout {
    id: root

    width: 220

    spacing: 5

    property bool startEnabled: true
    property bool pauseEnabled: true
    property bool stopEnabled: true

    signal start()
    signal pause()
    signal stop()

    Button {
        Layout.fillWidth: true
        Layout.fillHeight: true
        text: "Start"
        enabled: root.startEnabled
        onClicked: root.start()
    }

    Button {
        Layout.fillWidth: true
        Layout.fillHeight: true
        text: "Pause"
        enabled: root.pauseEnabled
        onClicked: root.pause()
    }

    Button {
        Layout.fillWidth: true
        Layout.fillHeight: true
        text: "Stop"
        enabled: root.stopEnabled
        onClicked: root.stop()
    }

}
