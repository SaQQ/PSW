import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import QtCharts 2.0

import "controls"

import PSW.Gyration 1.0 as PSW

ApplicationWindow {
    id: window
    objectName: "window"
    visible: true
    title: "PŚW - Gyration"

    width: 1200
    height: 800

    minimumWidth: 1000
    minimumHeight: 600

    PSW.Simulator {
        id: simulator

        property bool startEnabled: !running
        property bool pauseEnabled: running
        property bool resetEnabled: running

        gravity: simulationConfiguration.gravity
    }

    function start() {
        simulator.start(
            simulationConfiguration.integrationStep,
            simulationConfiguration.cubeSize,
            simulationConfiguration.cubeDensity,
            simulationConfiguration.angularVelocity,
            simulationConfiguration.axisOffset
        )
    }

    function pause() {
        simulator.togglePause()
    }

    function reset() {
        tipPath.clear()
        simulator.reset()
    }

    footer: ToolBar {
        height: 24
        Row {
            anchors.fill: parent
            anchors.margins: 4

            Label {
                text: simulator.running ? (simulator.paused ? qsTr("Running (Paused)") : qsTr("Running")) : qsTr("Idle")
            }
        }
    }

    Rectangle {
        id: background

        color: "#424242"

        anchors.fill: parent

        Item {
            id: leftPaneContainer

            width: 250

            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }

            ColumnLayout {

                anchors.fill: parent
                anchors.margins: 5

                spacing: 5

                SectionPlate {

                    Layout.fillWidth: true
                    Layout.minimumHeight: 70
                    Layout.maximumHeight: 70

                    header: "Simulation control"

                    SimulationControl {
                        anchors.fill: parent

                        onStart: window.start()
                        onPause: window.pause()
                        onStop: window.reset()

                        startEnabled: simulator.startEnabled
                        pauseEnabled: simulator.pauseEnabled
                        stopEnabled: simulator.resetEnabled
                    }

                }

                ScrollableSectionPlate {
                    Layout.fillWidth: true
                    Layout.minimumHeight: 260
                    Layout.maximumHeight: 260

                    header: "Simulation configuration"

                    SimulationConfiguration {
                        id: simulationConfiguration

                        enabled: !simulator.running
                    }
                }

                ScrollableSectionPlate {

                    Layout.fillWidth: true
                    Layout.minimumHeight: 160
                    Layout.maximumHeight: 160

                    header: "Display configuration"

                    DisplayConfiguration {
                        id: displayConfiguration
                    }

                }

                ScrollableSectionPlate {

                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    header: "Simulation information"

                    SimulationInformation {
                        time: simulator.time
                        gravity: simulator.gravity
                        angularVelocity: simulator.angularVelocityInCubeFrame
                    }

                }

            }

        }

        Item {
            id: centerPaneContainer

            anchors {
                top: parent.top
                bottom: parent.bottom
                left: leftPaneContainer.right
                right: parent.right
            }

            SectionPlate {
                header: "Visualization"

                anchors.fill: parent
                anchors.margins: 5
                anchors.leftMargin: 0

                Rectangle {
                    anchors.fill: parent

                    border {
                        width: 1
                        color: simulator.running ? (simulator.paused ? "#3F51B5" : "#4CAF50") : "#212121"
                    }

                    Scene3D {
                        id: scene3d
                        anchors.fill: parent
                        anchors.margins: 1
                        focus: true
                        aspects: ["input", "logic", "render"]
                        cameraAspectRatioMode: Scene3D.AutomaticAspectRatio

                        Entity {
                            id: sceneRoot

                            property real tableSize: 5.0

                            property real cubeSize: simulator.cubeSize

                            property real gravityVectorLength: 2.0

                            Camera {
                                id: camera
                                projectionType: CameraLens.PerspectiveProjection
                                fieldOfView: 45
                                nearPlane : 0.1
                                farPlane : 1000.0
                                position: Qt.vector3d( 0.0, 3.0, 12.0 )
                                upVector: Qt.vector3d( 0.0, 1.0, 0.0 )
                                viewCenter: Qt.vector3d( 0.0, 0.0, 0.0 )
                            }

                            OrbitCameraController {
                                camera: camera
                                linearSpeed: 100.0
                            }

                            components: [
                                RenderSettings {
                                    activeFrameGraph: ForwardRenderer {
                                        camera: camera
                                        clearColor: "gray"
                                    }
                                },
                                InputSettings { },
                                DirectionalLight {
                                    worldDirection: Qt.vector3d(1, -1, 1)
                                },
                                DirectionalLight {
                                    worldDirection: Qt.vector3d(-1, -1, -1)
                                }

                            ]

                            PhongAlphaMaterial {
                                id: cubeMaterial
                                ambient: "#BDBDBD"
                                alpha: displayConfiguration.diagonalVisible ? 0.7 : 1.0
                            }

                            PhongAlphaMaterial {
                                id: tableMaterial
                                diffuse: "#999999"
                                alpha: 0.5
                            }

                            CuboidMesh {
                                id: tableMesh
                                xExtent: sceneRoot.tableSize
                                yExtent: 0.1
                                zExtent: sceneRoot.tableSize
                            }

                            CuboidMesh {
                                id: cubeMesh
                                readonly property real size: sceneRoot.cubeSize
                                readonly property real diagonalLength: Math.sqrt(3) * size
                                xExtent: size
                                yExtent: size
                                zExtent: size
                            }

                            Transform {
                                id: frameTransform
                                rotation: simulator.rotation
                            }

                            Transform {
                                id: frame2Transform
                                rotation: simulator.rotation
                            }

                            Transform {
                                id: cubeTransform
                                translation: Qt.vector3d(cubeMesh.size / 2,cubeMesh.size / 2,cubeMesh.size / 2)
                            }

                            Vector {
                                id: gravityVector
                                enabled: displayConfiguration.gravityVectorVisible
                                simulator: simulator
                                coordinates: Qt.vector3d(0, -2, 0)
                            }

                            PSW.TipPath {
                                id: tipPath
                                enabled: displayConfiguration.tipPathVisible
                                length: simulationConfiguration.pathLength
                            }

                            Entity {
                                id: frame

                                Vector {
                                    id: diagonal
                                    simulator: simulator
                                    enabled: displayConfiguration.diagonalVisible
                                    coordinates: Qt.vector3d(cubeMesh.size, cubeMesh.size, cubeMesh.size)
                                    color: "#FD1425"
                                }

                                components: [ frameTransform ]
                            }

                            Entity {
                                id: table
                                components: [ tableMesh, tableMaterial ]
                            }

                            Entity {
                                id: frame2

                                Entity {
                                    id: cube
                                    enabled: displayConfiguration.cubeVisible
                                    components: [ cubeMesh, cubeMaterial, cubeTransform ]
                                }

                                components: [ frame2Transform ]
                            }

                        }

                    }

                }

            }

        }

    }

    Connections {
        target: simulator
        onStep: {
            tipPath.pushPoint(simulator.rotateVector(simulator.rotation, Qt.vector3d(simulator.cubeSize, simulator.cubeSize, simulator.cubeSize)))
        }
    }

}
