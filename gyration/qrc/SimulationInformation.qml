import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GridLayout {
    id: root

    width: 220

    columns: 2

    property real time: 0
    property bool gravity: true
    property vector3d angularVelocity: Qt.vector3d(0, 1, 0)

    property real valueWidth: 80

    Text {
        text: "Time"
        color: "#f5f5f5"
    }

    Text {
        text: root.time.toFixed(3) + " s"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
    }

    Text {
        text: "Gravity"
        color: "#f5f5f5"
    }

    Text {
        text: root.gravity ? "ON" : "OFF"
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
    }

    Text {
        text: "Angular velocity"
        color: "#f5f5f5"
    }

    Text {
        text: root.angularVelocity.length().toFixed(3)
        color: "#f5f5f5"
        Layout.minimumWidth: root.valueWidth
    }

}

